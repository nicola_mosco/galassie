//
//  PHYCore.cpp
//  Physics
//
//  Created by Nicola Mosco on 06/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#include "PHYCore.h"
#include "PHYBody.h"


using namespace phy;



Core::Core(const ReferenceFrame& aRefFrame,
           const std::string& name)
    : BodyCluster{aRefFrame, name},
      mass_{}
{
    bodyName() = "Black Hole";
}



const Real
Core::mass() const
{
    return mass_;
}



Real&
Core::mass()
{
    return mass_;
}



void
Core::buildStructure()
{
    Body::PtrType body{new Body{ReferenceFrame{}, bodyName()}};
    
    body->mass() = mass();
    
    bodies_.push_back(std::move(body));
}
