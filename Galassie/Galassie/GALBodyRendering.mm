//
//  GALBodyRendering.m
//  Galassie
//
//  Created by Nicola Mosco on 04/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import "GALBodyRendering.h"


using namespace gal;



BodyRendering::BodyRendering( void ) : radius_(0), slices_(0), stacks_(0),
                                       shininess_(0), colorMin_( 0.2 )
{
    colorAmbient_  = nil;
    colorDiffuse_  = nil;
    colorSpecular_ = nil;
    colorEmission_ = nil;
    
    displayList_   = glGenLists( 1 );
}



NSColor* BodyRendering::colorAmbient( void )
{
    return colorAmbient_;
}



void BodyRendering::setColorAmbient( NSColor *colorAmbient )
{
    colorAmbient_ = colorAmbient;
    
    return;
}



NSColor* BodyRendering::colorDiffuse( void )
{
    return colorDiffuse_;
}



void BodyRendering::setColorDiffuse( NSColor *colorDiffuse )
{
    colorDiffuse_ = colorDiffuse;
    
    return;
}



NSColor* BodyRendering::colorSpecular( void )
{
    return colorSpecular_;
}



void BodyRendering::setColorSpecular( NSColor *colorSpecular )
{
    colorSpecular = colorSpecular;
    
    return;
}



NSColor* BodyRendering::colorEmission( void )
{
    return colorEmission_;
}



void BodyRendering::setColorEmission( NSColor *colorEmission )
{
    colorEmission_ = colorEmission;
    
    return;
}



void BodyRendering::generateDisplayList( void )
{
    GLuint  i, j;
    CGFloat tmp_col[ 4 ][ 4 ];
    
    [colorAmbient_ getComponents: tmp_col[ 0 ]];
    [colorDiffuse_ getComponents: tmp_col[ 1 ]];
    [colorSpecular_ getComponents: tmp_col[ 2 ]];
    [colorEmission_ getComponents: tmp_col[ 3 ]];
    
    for ( i = 0; i < 4; i++ )
    {
        for ( j = 0; j < 4; j++ )
        {
            colors_[ i ][ j ] = tmp_col[ i ][ j ];
        }
    }
    
    
    glNewList( displayList_, GL_COMPILE );
    {
        /*glMaterialfv( GL_FRONT_AND_BACK, GL_AMBIENT, colors_[ 0 ] );
        glMaterialfv( GL_FRONT_AND_BACK, GL_DIFFUSE, colors_[ 1 ] );
        glMaterialfv( GL_FRONT_AND_BACK, GL_SPECULAR, colors_[ 2 ] );
        glMaterialfv( GL_FRONT_AND_BACK, GL_EMISSION, colors_[ 3 ] );
        glMaterialf( GL_FRONT_AND_BACK, GL_SHININESS, shininess_ );*/
        
        glBegin( GL_POINTS );
        {
            glVertex3d( 0.0, 0.0, 0.0 );
        }
        glEnd();
    }
    glEndList();
    
    return;
}
