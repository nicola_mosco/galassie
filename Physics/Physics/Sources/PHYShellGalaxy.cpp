//
//  PHYShellGalaxy.cpp
//  Physics
//
//  Created by Nicola Mosco on 06/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#include "PHYShellGalaxy.h"


using namespace phy;



ShellGalaxy::ShellGalaxy(const ReferenceFrame& aRefFrame,
                         const std::string& name)
    : Galaxy{aRefFrame, name}
{
    core_it_  = bodyClusters_.end();
    bulge_it_ = bodyClusters_.end();
    shell_it_ = bodyClusters_.end();
}



void
ShellGalaxy::setCore(Core::PtrType core)
{
    core_it_ = bodyClusters_.erase(core_it_);
    core_it_ = bodyClusters_.emplace(core_it_, std::move(core));
}



void
ShellGalaxy::setBulge(Shell::PtrType bulge)
{
    bulge_it_ = bodyClusters_.erase(bulge_it_);
    bulge_it_ = bodyClusters_.emplace(bulge_it_, std::move(bulge));
}



void
ShellGalaxy::setShell(Shell::PtrType shell)
{
    shell_it_ = bodyClusters_.erase(shell_it_);
    shell_it_ = bodyClusters_.emplace(shell_it_, std::move(shell));
}
