//
//  PHYShell.h
//  Physics
//
//  Created by Nicola Mosco on 06/01/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYOpenShell__
#define __Physics__PHYOpenShell__

#include "PHYBodyCluster.h"

namespace phy
{
    class Shell : public BodyCluster
    {
    public:
        Shell(const ReferenceFrame& refFrame = ReferenceFrame{},
              const std::string& name = "Shell");
        virtual ~Shell() = default;
        
    public:
        const Size  numBodies() const;
        Size&       numBodies();
        
        const Real  externalRadius() const;
        Real&       externalRadius();
        
        const Real  internalRadius() const;
        Real&       internalRadius();
        
        const Real  meanRadius() const;
        Real&       meanRadius();
        
        const Real  radialSpread() const;
        Real&       radialSpread();
        
        const Real  theta() const;
        Real&       theta();
        
        const Real  meanMass() const;
        Real&       meanMass();
        
        const Real  massSpread() const;
        Real&       massSpread();
        
        const Real  minMass() const;
        Real&       minMass();
        
        const Real  gravFactor() const;
        Real&       gravFactor();
        
        
        
    protected:
        virtual void buildStructure();
        
    protected:
        Size numBodies_;
        Real extRadius_;
        Real intRadius_;
        Real meanRadius_;
        Real radialSpread_;
        Real theta_;
        Real meanMass_;
        Real massSpread_;
        Real minMass_;
        Real gravFactor_;
    };
}

#endif /* defined(__Physics__PHYOpenShell__) */
