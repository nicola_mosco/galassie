//
//  GalassieAppDelegate.h
//  Galassie
//
//  Created by Nicola Mosco on 04/05/11.
//  Copyright 2011 Università degli Studi di Trieste. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface GalassieAppDelegate : NSObject <NSApplicationDelegate> {
@private
    NSWindow *window;
}

@property (assign) IBOutlet NSWindow *window;

@end
