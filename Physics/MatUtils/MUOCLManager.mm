//
//  MUOCLManager.mm
//  MatUtils
//
//  Created by Nicola Mosco on 19/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import "MUOCLManager.h"
#import <iostream>


using namespace cl;
using namespace mu;



OCLManager::OCLManager( const std::vector<std::string>& sourceCodeVec,
                        const std::string& kernelName,
                        const MUSize numWorkItems ) :
    kernelName_( kernelName ), numWorkItems_(numWorkItems)
{
    Platform::get( &platforms_ );
    
    for ( std::vector<std::string>::const_iterator it = sourceCodeVec.begin(); it != sourceCodeVec.end(); ++it )
    {
        sources_.push_back( std::make_pair( it->c_str(), it->length() + 1 ) );
    }
    
    global_ = NullRange;
    local_  = NullRange;
}



void OCLManager::setup( const cl_device_type devType )
{
    context_ = Context( devType );
    
    devices_ = context_.getInfo<CL_CONTEXT_DEVICES>();
    
    queue_   = CommandQueue( context_, devices_[0] );
    
    program_ = Program( context_, sources_ );
    program_.build( devices_ );
    
    kernel_  = Kernel( program_, kernelName_.c_str() );
    
    std::cout << kernel_.getWorkGroupInfo<CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE>(devices_[0]) << std::endl;
    std::cout << kernel_.getWorkGroupInfo<CL_KERNEL_WORK_GROUP_SIZE>(devices_[0]) << std::endl;
    std::cout << devices_[0].getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() << std::endl;
    
    ::size_t maxSize = kernel_.getWorkGroupInfo<CL_KERNEL_WORK_GROUP_SIZE>(devices_[0]);
    ::size_t global  = maxSize * (::size_t)ceil(numWorkItems_ / double(maxSize));
    
    global_ = NDRange( global );
    local_  = NDRange( maxSize );
    
    kernel_.setArg(0, sizeof(MUSize), &numWorkItems_);
}



void OCLManager::addBuffer( const MUSize length,
                            const MUSize kArg,
                            const cl_mem_flags flags )
{
    Buffer oclBuffer = Buffer( context_, flags, length );
    
    buffers_[ kArg ] = oclBuffer;
    
    kernel_.setArg( kArg, buffers_[ kArg ] );
}
