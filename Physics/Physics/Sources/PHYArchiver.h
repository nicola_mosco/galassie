//
//  PHYArchiver.h
//  Physics
//
//  Created by Nicola Mosco on 14/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYArchiver__
#define __Physics__PHYArchiver__

#include "PHYDevice.h"


namespace phy
{
    class Archiver : public Device
    {
    public:
        Archiver();
        virtual ~Archiver() = default;
        
        
        
    protected:
        virtual void task();
        
        
        
    private:
        Archiver(const Archiver& archiver) = delete;
        const Archiver& operator=(const Archiver& archiver) = delete;
    };
}

#endif /* defined(__Physics__PHYArchiver__) */
