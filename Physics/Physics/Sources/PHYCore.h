//
//  PHYCore.h
//  Physics
//
//  Created by Nicola Mosco on 06/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYCore__
#define __Physics__PHYCore__

#include "PHYBodyCluster.h"

namespace phy
{
    class Core : public BodyCluster
    {
    public:
        Core(const ReferenceFrame& aRefFrame = ReferenceFrame{},
             const std::string& name = "Core");
        virtual ~Core() = default;
        
    public:
        const Real mass() const;
        Real& mass();
        
        
        
    protected:
        virtual void buildStructure() final;
        
        
        
    private:
        Real mass_;
    };
}

#endif /* defined(__Physics__PHYCore__) */
