//
//  PHYBody.cpp
//  Physics
//
//  Created by Nicola Mosco on 06/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#include "PHYBody.h"


using namespace phy;



Body::Body(const ReferenceFrame& aRefFrame,
           const std::string& name)
    : Geometry{aRefFrame, name}, mass_{}
{}



const Real
Body::mass() const
{
    return mass_;
}



Real&
Body::mass()
{
    return mass_;
}



RVec3
Body::pos()
{
    return referenceFrame()(RVec3{});
}



RVec3
Body::vel()
{
    return referenceFrame().vel();
}
