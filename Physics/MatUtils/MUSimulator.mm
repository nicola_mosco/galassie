//
//  MUSimulator.mm
//  MatUtils
//
//  Created by Nicola Mosco on 23/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import "MUSimulator.h"


using namespace mu;



@implementation MUSimulator

@synthesize interaction = interaction_;

- (id)initWithSystem:(System*)aSystem
         interaction:(Interaction*)anInteraction
          integrator:(Integrator *)anIntegrator
      sampleInterval:(MUReal)sample
{
    self = [super init];
    
    if (self)
    {
        system_       = aSystem;
        interaction_  = anInteraction;
        integrator_   = anIntegrator;
        
        dataFileName_ = "dataFile.dat";
        
        sample_       = sample;
    }
    
    return self;
}



- (void)dealloc
{
    delete system_;
    delete interaction_;
    delete integrator_;
}



- (void)setup:(cl_device_type)devType
{
    integrator_->setup(devType);
}



- (System *)system
{
    return system_;
}



- (void)simulate:(id)sender
{
    (*integrator_)( sample_, dataFileName_ );
}

@end
