//
//  PHYDeviceData.cpp
//  Physics
//
//  Created by Nicola Mosco on 15/04/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#include "PHYDeviceDataImplementation.h"
#include "PHYDeviceData.h"


using namespace phy;



DeviceData::DeviceData(std::unique_ptr<DeviceDataImplementation> implementation)
    : implementation_{std::move(implementation)}
{}



DeviceData::DeviceData(DeviceData&& deviceData)
    : implementation_{std::move(deviceData.implementation_)}
{}
