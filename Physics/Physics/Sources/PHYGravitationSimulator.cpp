//
//  PHYGravitationSimulator.cpp
//  Physics
//
//  Created by Nicola Mosco on 14/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#include "PHYGravitationSimulator.h"
#include "PHYGravitation.cl.h"


using namespace phy;



GravitationSimulator::GravitationSimulator(const cl_device_type clDeviceType)
    : Simulator{clDeviceType, "GravitationSimulator"}
{}



void
GravitationSimulator::task()
{
    
}
