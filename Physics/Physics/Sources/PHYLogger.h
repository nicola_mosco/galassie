//
//  PHYLogger.h
//  Physics
//
//  Created by Nicola Mosco on 21/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYLogger__
#define __Physics__PHYLogger__

#include <memory>

#include "PHYSerialTaskDevice.h"
#include "PHYConsole.h"


namespace phy
{
    class Logger : public SerialTaskDevice // Logger is a pure interface.
                                           // This device is supposed to interface himself
                                           // with any logging facility, as a system log or application text view.
                                           // One has to subclass Logger and implement a platform specific
                                           // task, so that the user is informed on what others devices are doing.
                                           // One instance of Logger is attached to a specific logging system.
                                           // For example if you have a text view in which you want to display
                                           // device messages, than you attach a Logger subclass to this text view.
                                           // In this sense the text view "owns" the Logger which is unique.
                                           // A given Device is able to write to only one Logger.
    {
    public:
        Logger();
        virtual ~Logger();
        
    public: // Setting inputs parameters.
        void setLogInfo(const std::string& senderDescription,
                        const std::string& message);
        
    public: // Actions.
        void attachToConsole(std::shared_ptr<Console>& console);
        
        void log(const std::string& senderDescription,
                 const std::string& message);
        
        
        
    protected:
        virtual void task() final;
        
        
        
    private:
        std::string logInfo_;
        std::shared_ptr<Console> console_;
        
    private:
        Logger(const Logger& logger) = delete;
        const Logger& operator=(const Logger& logger) = delete;
    };
}

#endif /* defined(__Physics__PHYLogger__) */
