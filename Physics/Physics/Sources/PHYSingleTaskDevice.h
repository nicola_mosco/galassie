//
//  PHYSingleTaskDevice.h
//  Physics
//
//  Created by Nicola Mosco on 23/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYSingleTaskDevice__
#define __Physics__PHYSingleTaskDevice__

#include "PHYDevice.h"


namespace phy
{
    class SingleTaskDevice : public Device
    {
    public:
        virtual ~SingleTaskDevice() = 0;
        
    public:
        virtual void performTask(dispatch_block_t beginAction,
                                 dispatch_block_t endAction) final;
        
        
        
    protected:
        explicit SingleTaskDevice(const std::string& description);
        
    protected: // Task defined by subclasses.
        virtual void task() = 0;
        
        
        
    private:
        SingleTaskDevice() = delete;
        SingleTaskDevice(const SingleTaskDevice& device) = delete;
        const SingleTaskDevice& operator=(const SingleTaskDevice& device) = delete;
    };
}

#endif /* defined(__Physics__PHYSingleTaskDevice__) */
