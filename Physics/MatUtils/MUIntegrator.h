//
//  MUIntegrator.h
//  MatUtils
//
//  Created by Nicola Mosco on 08/05/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#import "MUTypes.h"
#import "MURungeKutta4.h"
#import "MUInteraction.h"
#import "MUOCLManager.h"
#import "MUOctree.h"
#import "MUSystem.h"
#import "MUSimState.h"
#import "MUSimStateManager.h"
#import <fstream>
#import <memory>
#import <dispatch/dispatch.h>


namespace mu
{
    class Integrator
    {
    public:
        Integrator(System* system,
                   Interaction* interaction,
                   const MUReal theta,
                   const MUReal iniTime,
                   const MUReal finTime,
                   const MUReal intStep);
        ~Integrator();
        
        void                    setup(const cl_device_type devType);
        
        void                    interaction(const MUReal t,
                                            const MUReal3 *pos,
                                            MUReal3 *accel);
        
        void                    operator()(const MUReal sampleIntv,
                                           const std::string& dataFileName);
        
        MUReal                  intStep() const { return rk4_->intStep(); }
        
        
        
    private:
        void                    postTimeUpdateNotification() const;
        
    private:
        RungeKutta4::RK4Ptr     rk4_;
        OCLManager*             openCL_;
        System*                 system_;
        Interaction*            interaction_;
        oct::Octree*            octree_;
        MUReal                  theta_;
        MUReal                  t_i_;
        MUReal                  t_;
        MUReal                  t_f_;
    };
}
