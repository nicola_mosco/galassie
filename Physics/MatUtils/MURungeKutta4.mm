//
//  MURungeKutta4.mm
//  MatUtils
//
//  Created by Nicola Mosco on 13/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import "MURungeKutta4.h"
#import "MUGeneralia.h"
#import "MUIntegrator.h"
#import <iostream>
#import <iomanip>


using namespace std;
using namespace mu;



RungeKutta4::RungeKutta4( const MUSize numBodies,
                          Integrator* integrator,
                          const MUReal intStep ) : Algorithm( "Runge Kutta 4", integrator ),
                                                   numBodies_( numBodies ), h_( intStep )
{
    posTmp_ = new MUReal3[ numBodies_ ];
    velTmp_ = new MUReal3[ numBodies_ ];
    
    for ( MUSize i = 0; i < 4; ++i )
    {
        accelTmp_[ i ] = new MUReal3[ numBodies_ ];
    }
}



RungeKutta4::~RungeKutta4()
{
    delete[] posTmp_;
    delete[] velTmp_;
    
    for ( MUSize i = 0; i < 4; ++i )
    {
        delete[] accelTmp_[ i ];
    }
}



void RungeKutta4::operator()( const MUReal t,
                              MUReal3 *pos,
                              MUReal3 *vel )
{
    register
    MUSize  i;
    MUReal  f;
    
    
    integrator_->interaction( t, pos, accelTmp_[0] );
    
    f = MUReal(0.5) * h_;
    
    for ( i = 0; i < numBodies_; ++i )
    {
        posTmp_[ i ] = muSum( pos[ i ], muScale( vel[ i ], f ) );
        velTmp_[ i ] = muSum( vel[ i ], muScale( accelTmp_[0][ i ], f ) );
    }
    
    
    integrator_->interaction( t + f, posTmp_, accelTmp_[1] );
    
    for ( i = 0; i < numBodies_; ++i )
    {
        posTmp_[ i ] = muSum( pos[ i ], muScale( velTmp_[ i ], f ) );
        velTmp_[ i ] = muSum( vel[ i ], muScale( accelTmp_[1][ i ], f ) );
    }
    
    
    integrator_->interaction( t + f, posTmp_, accelTmp_[2] );
    
    f = h_;
    
    for ( i = 0; i < numBodies_; ++i )
    {
        posTmp_[ i ] = muSum( pos[ i ], muScale( velTmp_[ i ], f ) );
        velTmp_[ i ] = muSum( vel[ i ], muScale( accelTmp_[2][ i ], f ) );
    }
    
    
    integrator_->interaction( t + f, posTmp_, accelTmp_[3] );
    
    /*cout << setprecision(7) << accelTmp_[0][1].x << ", " << accelTmp_[0][1].y << ", " << accelTmp_[0][1].z << endl;
    cout << setprecision(7) << accelTmp_[1][1].x << ", " << accelTmp_[1][1].y << ", " << accelTmp_[1][1].z << endl;
    cout << setprecision(7) << accelTmp_[2][1].x << ", " << accelTmp_[2][1].y << ", " << accelTmp_[2][1].z << endl;
    cout << setprecision(7) << accelTmp_[3][1].x << ", " << accelTmp_[3][1].y << ", " << accelTmp_[3][1].z << endl;
    cout << endl;*/
    
    f = h_ / MU_REAL(6);
    
    for ( i = 0; i < numBodies_; ++i )
    {
        MUReal3 tmp = muScale( muSum( 3, accelTmp_[0][ i ],
                                         accelTmp_[1][ i ],
                                         accelTmp_[2][ i ] ), h_ * f );
        
        pos[ i ] = muSum( 3, pos[ i ], muScale( vel[ i ], h_ ), tmp );
        
        tmp = muSum( 4, accelTmp_[0][ i ],
                        muScale( accelTmp_[1][ i ], 2.0 ),
                        muScale( accelTmp_[2][ i ], 2.0 ),
                        accelTmp_[3][ i ] );
        vel[ i ] = muSum( vel[ i ], muScale( tmp, f ) );
    }
}
