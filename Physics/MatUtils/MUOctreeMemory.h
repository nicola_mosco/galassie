//
//  MUOctreeMemory.h
//  MatUtils
//
//  Created by Nicola Mosco on 04/09/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MUOctreeDefs.h"


namespace mu
{
    namespace oct
    {
        class OctreeMemory
        {
        public:
            OctreeMemory( const MUSize numBodies );
            ~OctreeMemory( void );
            
            
        public:
            MUSize         bufferSize( void ) { return _bufSize; }
            MUSize         maxNodes( void ) { return _maxNodes; }
            MUSize         maxDepth( void ) { return _maxDepth; }
            MUSize         numBodies( void ) { return _numBodies; }
            
            MUBuffer       buffer( void ) { return _buffer; }
            
            
            
        protected:
            MUSize         depthForNumBodies( const MUSize numBodies )
            {
                return (MUSize)ceilf( logf( (MUReal)numBodies ) / logf( 8.0f ) );
            }
            
            MUSize         numNodesForNumBodies( const MUSize numBodies,
                                                 const MUSize depth )
            {
                MUSize exp      = OCTSIZE(3) * ( depth + OCTSIZE(1) );
                MUSize powerOf2 = OCTSIZE(1) << exp;
                
                return ( powerOf2 - OCTSIZE(1) ) / OCTSIZE(7);
            }
            
            MUSize         bufferSizeForNumNodes( const MUSize numNodes )
            {
                return OCT_SIZE_HEADER + numNodes * OCT_SIZE_NODE;
            }
            
            MURef          offsetForNodeIndex( const MURef nodeIndex )
            {
                return OCT_SIZE_HEADER + nodeIndex * OCT_SIZE_NODE;
            }
            
            
            MURef          current( void ) { return _current; }
            MURef          next( void ) { return _next; }
            
            
            OctNode         allocNewNode( MURef *const nodeOffsetPtr );
            
            
            void            reset( void );
            
            
        protected:
            MUBuffer       _buffer;
            MUSize         _maxNodes;
            MUSize         _maxDepth;
            MUSize         _numBodies;
            
            
            
        private:
            void            allocBuffer( const MUSize numNodes );
            void            freeBuffer( void );
            void            reallocBuffer( const MUSize numNodes );
            
            void            addNodes( const MUSize numAddedNodes );
            
            
        private:
            NSMutableData*  _bufferData;
            MUSize          _bufSize;
            MUSize          _freeBytes;
            MURef           _current;
            MURef           _next;
            OctNode         _currentNode;
        };
    }
}
