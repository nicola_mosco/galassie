//
//  PHYDevice.cpp
//  Physics
//
//  Created by Nicola Mosco on 14/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#include "PHYDevice.h"
#include "PHYLogger.h"


namespace phy
{
    const std::array<std::string, Device::numStateValues_> Device::stateNames_ = { "ONLINE", "OFFLINE", "READY", "BUSY", "IDLE", "SUSPENDED" };
}


using namespace phy;



Device::Device(const std::string description)
    : description_{std::move(description)}, state_{State::ONLINE}
{
    auto label = "com.device." + description;
    
    deviceGroup_ = dispatch_group_create();
    deviceQueue_ = dispatch_queue_create(label.c_str(), nullptr);
    
    state_ = State::READY;
}



Device::~Device()
{
    dispatch_release(deviceQueue_);
    dispatch_release(deviceGroup_);
}



const std::string
Device::description() const
{
    return description_;
}



const dispatch_group_t
Device::deviceGroup() const
{
    return deviceGroup_;
}



const dispatch_queue_t
Device::deviceQueue() const
{
    return deviceQueue_;
}



bool
Device::isBusy() const
{
    return state_ == State::BUSY;
}



bool
Device::isReady() const
{
    return state_ == State::READY;
}



bool
Device::isIdle() const
{
    return state_ == State::IDLE;
}



bool
Device::isOnline() const
{
    return state_ == State::ONLINE;
}



bool
Device::isOffline() const
{
    return state_ == State::OFFLINE;
}



const Device::State
Device::state() const
{
    return state_;
}



void
Device::setState(const phy::Device::State state)
{
    state_ = state;
}



const std::string
Device::stateName() const
{
    return stateNames_[state_];
}



const std::string
Device::stateName(const State state) const
{
    return stateNames_[state];
}



void
Device::registerLogger(std::unique_ptr<Logger> logger)
{
    logger_ = std::move(logger);
    
    writeMessage("registered logger: " + logger_->description());
    writeMessage("device is " + stateName());
}



void
Device::unregisterLogger()
{
    writeMessage("unregistering logger: " + logger_->description());
    
    logger_ = nullptr;
}



void
Device::writeMessage(const std::string& message) const
{
    if (logger_)
    {
        logger_->log(description(), message);
    }
}



void
Device::performTask(dispatch_block_t beginAction,
                    dispatch_block_t endAction)
{
    dispatch_group_async(deviceGroup(), deviceQueue(),
                         ^{
                             setState(State::BUSY);
                             
                             beginAction();
                             
                             writeMessage("device begins task execution...");
                             
                             task();
                             
                             endAction();
                             
                             setState(State::READY);
                             
                             writeMessage("device is READY!");
                         });
    
    return;
}
