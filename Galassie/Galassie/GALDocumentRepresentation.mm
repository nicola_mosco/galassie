//
//  GALDocumentRepresentation.mm
//  Galassie
//
//  Created by Nicola Mosco on 05/01/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import "GALDocumentRepresentation.h"
#import "GALDocument.h"

@implementation GALDocumentRepresentation
{
    NSString*   simulationDataDir;
    NSString*   stateDataDir;
    NSString*   energyDataDir;
    NSString*   systemDataDir;
    
    NSString*   initialPosDataPath;
    NSString*   initialVelDataPath;
    NSString*   massDataPath;
    NSString*   systemGeometryPath;
    NSString*   simulationParametersPath;
    
    NSArray*    directories;
}



- (id)initWithDocument:(GALDocument *)doc
{
    self = [super init];
    
    if (self)
    {
        document = doc;
        
        simulationDataDir           = @"Simulation data";
        stateDataDir                = @"Simulation data/State data";
        energyDataDir               = @"Simulation data/Energy data";
        systemDataDir               = @"System data";
        
        initialPosDataPath          = @"System data/Initial pos data.dat";
        initialVelDataPath          = @"System data/Initial vel data.dat";
        massDataPath                = @"System data/mass data.dat";
        systemGeometryPath          = @"System geometry.dat";
        simulationParametersPath    = @"Simulation parameters.dat";
        
        directories                 = @[simulationDataDir, stateDataDir, energyDataDir, systemDataDir];
    }
    
    return self;
}



- (BOOL)buildStructureAtURL:(NSURL*)fileURL
                  withError:(NSError *__autoreleasing *)error
{
    BOOL            flag = NO;
    NSFileManager*  fileManager = [NSFileManager defaultManager];
    
    flag = [fileManager createDirectoryAtURL:fileURL
                 withIntermediateDirectories:NO
                                  attributes:nil
                                       error:error];
    
    if (!flag)
    {
        return flag;
    }
    
    for (NSString* dirPath in directories)
    {
        @autoreleasepool
        {
            flag = [fileManager createDirectoryAtURL:[fileURL URLByAppendingPathComponent:dirPath
                                                                              isDirectory:YES]
                         withIntermediateDirectories:NO
                                          attributes:nil
                                               error:error];
        }
        
        if (!flag)
        {
            break;
        }
    }
    
    return flag;
}

@end
