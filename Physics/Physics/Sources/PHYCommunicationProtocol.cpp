//
//  PHYCommunicationProtocol.cpp
//  Physics
//
//  Created by Nicola Mosco on 15/04/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#include "PHYNotification.h"
#include "PHYRequest.h"
#include "PHYCommunicationProtocol.h"


using namespace phy;



CommunicationProtocol::~CommunicationProtocol() = default;



void
CommunicationProtocol::sendNotification(std::unique_ptr<Notification> notification,
                                        CommunicationProtocol& observer)
{
    observer.notify(std::move(notification));
}
