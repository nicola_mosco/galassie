//
//  GALSpaceView.mm
//  Galassie
//
//  Created by Nicola Mosco on 03/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import "MatUtils/MUGeneralia.h"
#import "GALSpaceView.h"
#import "GALViewState.h"
#import "GALAxises.h"
#import "GALBoundingBox.h"
#import "GALBodyRendering.h"
#import "GALSimulationParameters.h"


using namespace gal;



@implementation GALSpaceView
{
    MUSize                      numBodies;
    MUSize                      numGal;
    MUSize                      numBodGal;
    
    __strong
    NSData*                     posData;
    __strong
    NSData*                     velData;
    
    const
    MUReal3*                    pos;
    const
    MUReal3*                    vel;
    
    MUReal                      minSpeed;
    MUReal                      maxSpeed;
    
    __strong
    GALSimulationParameters*    simParams;
}



@synthesize rotationSens, translationSens, scaleSens, zoomSens;
@synthesize visAngle, visCooL, visCooR, visCooB, visCooT, visWidth, visHeight, visFrameW, visFrameH;
@synthesize vPx, vPy, vPz, scale, zoom, bodyScale;
@synthesize constAttenuation, linearAttenuation, quadraticAttenuation;
@synthesize scaleMax, scaleMin, zoomMin, zoomMax, bodyScaleMax, bodyScaleMin;
@synthesize showFrame, showBoundingBox;
@synthesize lightAmbient, lightDiffuse, lightSpecular, lightPosition;
@synthesize bodiesPosition;
@synthesize viewState;
@synthesize mouseX, mouseY;
@synthesize extension;



- (void)dealloc
{
    delete body;
    
    return;
}



- (void)awakeFromNib
{
    self.visAngle             = 60.0;
    self.visWidth             = 1.0;
    self.visHeight            = 1.0;
    self.visFrameW            = 1.0;
    self.visFrameH            = 1.0;
    self.visCooL              = -visWidth  * 0.5;
    self.visCooR              =  visWidth  * 0.5;
    self.visCooB              = -visHeight * 0.5;
    self.visCooT              =  visHeight * 0.5;
    self.vPx                  = 0.0;
    self.vPy                  = 0.0;
    self.vPz                  = 3.0;
    self.scale                = 1.0;
    self.zoom                 = 1.0;
    self.bodyScale            = 0.01;
    self.extension            = 1.0;
    
    self.constAttenuation     = 1.0;
    self.linearAttenuation    = 0.001;
    self.quadraticAttenuation = 0.0001;
    
    angleX                    = 0.0;
    angleY                    = 0.0;
    deltaX                    = 0.0;
    deltaY                    = 0.0;
    
    self.rotationSens         = 0.25;
    self.translationSens      = 0.004;
    self.scaleSens            = 0.008;
    self.zoomSens             = 0.002;
    self.scaleMin             = 0.0;
    self.scaleMax             = 1.0;
    self.zoomMin              = 0.9;
    self.zoomMax              = 1.1;
    self.bodyScaleMin         = 0.001;
    self.bodyScaleMax         = 20.0;
    
    mouseEvent                = NO;
    
    self.showFrame            = YES;
    self.showBoundingBox      = YES;
    
    self.lightAmbient         = [NSColor whiteColor];
    self.lightDiffuse         = [NSColor whiteColor];
    self.lightSpecular        = [NSColor whiteColor];
    self.lightPosition        = nil;
    
    mouse.x                   = 0.0;
    mouse.y                   = 0.0;
    zoomPoint.x               = 0.0;
    zoomPoint.y               = 0.0;
    
    self.mouseX               = 0.0;
    self.mouseY               = 0.0;
    
    numBodies                 = 0;
    numGal                    = 0;
    numBodGal                 = 0;
    bodiesPosition            = nil;
    pos                       = NULL;
    vel                       = NULL;
    
    simParams                 = nil;
}



- (void)prepareOpenGL
{
    openGLContext = [NSOpenGLContext currentContext];
    
    axises      = [[GALAxises alloc] init];
    boundingBox = [[GALBoundingBox alloc] init];
    body        = new BodyRendering;
    
    
    glEnable( GL_DEPTH_TEST );
    //glEnable( GL_LIGHTING );
    //glEnable( GL_LIGHT0 );
    //glEnable( GL_POINT_SMOOTH );
    glEnable( GL_LINE_SMOOTH );
    glEnable( GL_NORMALIZE );
    glEnable( GL_COLOR_MATERIAL );
    
    GLfloat lux_amb[] = { 0.8, 0.8, 0.7, 1.0 };
    GLfloat lux_dif[] = { 0.8, 0.8, 0.7, 1.0 };
    GLfloat lux_spe[] = { 0.9, 0.9, 0.9, 1.0 };
    GLfloat lux_pos[] = { 0.0, 0.0, 0.0, 1.0 };
    
    self.lightAmbient  = [NSColor colorWithCalibratedRed: lux_amb[ 0 ]
                                                   green: lux_amb[ 1 ]
                                                    blue: lux_amb[ 2 ]
                                                   alpha: lux_amb[ 3 ]];
    self.lightDiffuse  = [NSColor colorWithCalibratedRed: lux_dif[ 0 ]
                                                   green: lux_dif[ 1 ]
                                                    blue: lux_dif[ 2 ]
                                                   alpha: lux_dif[ 3 ]];
    self.lightSpecular = [NSColor colorWithCalibratedRed: lux_spe[ 0 ]
                                                   green: lux_spe[ 1 ]
                                                    blue: lux_spe[ 2 ]
                                                   alpha: lux_spe[ 3 ]];
    self.lightPosition = [NSData dataWithBytes: lux_pos
                                        length: 4 * sizeof( GLfloat )];
    
    
    /*glLightfv( GL_LIGHT0, GL_POSITION, lux_pos );
    glLightfv( GL_LIGHT0, GL_AMBIENT, lux_amb );
    glLightfv( GL_LIGHT0, GL_DIFFUSE, lux_dif );
    glLightfv( GL_LIGHT0, GL_SPECULAR, lux_spe );
    glLightf( GL_LIGHT0, GL_CONSTANT_ATTENUATION, self.constAttenuation );
    glLightf( GL_LIGHT0, GL_LINEAR_ATTENUATION, self.linearAttenuation );
    glLightf( GL_LIGHT0, GL_QUADRATIC_ATTENUATION, self.quadraticAttenuation );*/
    
    glShadeModel( GL_SMOOTH );
    
    glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
    glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
    glHint( GL_POINT_SMOOTH_HINT, GL_NICEST );
    
    
    body->setColorAmbient( [NSColor colorWithCalibratedRed: 0.6
                                                     green: 0.5
                                                      blue: 0.2
                                                     alpha: 1.0] );
    body->setColorDiffuse( [NSColor colorWithCalibratedRed: 0.9
                                                     green: 0.6
                                                      blue: 0.3
                                                     alpha: 1.0] );
    body->setColorSpecular( [NSColor colorWithCalibratedRed: 0.9
                                                      green: 0.6
                                                       blue: 0.3
                                                      alpha: 1.0] );
    body->setColorEmission( [NSColor colorWithCalibratedRed: 0.0
                                                      green: 0.0
                                                       blue: 0.0
                                                      alpha: 1.0] );
    body->shininess() = 20.0;
    
    body->generateDisplayList();
    
    
    axises.color = [NSColor colorWithCalibratedRed: 0.2
                                             green: 0.4
                                              blue: 0.6
                                             alpha: 1.0];
    boundingBox.color = [NSColor colorWithCalibratedRed: 0.2
                                                  green: 0.4
                                                   blue: 0.6
                                                  alpha: 1.0];
}



- (void)drawRect:(NSRect)dirtyRect
{
    glClearColor( 0.0, 0.0, 0.0, 1.0 );
    glClearDepth( 1.0 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    
    glLoadIdentity();
    
    glPushMatrix();
    {
        [self moveFrameCamera];
        [self rotateFrame];
        [self drawFrame];
    }
    glPopMatrix();
    
    glPushMatrix();
    {
        [self moveBodiesCamera];
        [self rotateFrame];
        [self drawBodies];
    }
    glPopMatrix();
    
    
    [openGLContext flushBuffer];
}



- (void)reshape
{
    GLfloat    w = self.bounds.size.width;
    GLfloat    h = self.bounds.size.height;
    
    glViewport( 0.0, 0.0, (GLsizei) w, (GLsizei) h );
    
    if ( w < h )
    {
        visHeight = visWidth  * h / w;
        visFrameH = visFrameW * h / w;
    }
    else
    {
        visWidth  = visHeight * w / h;
        visFrameW = visFrameH * w / h;
    }
    
    visCooL    = -visWidth  * 0.5;
    visCooR    =  visWidth  * 0.5;
    visCooB    = -visHeight * 0.5;
    visCooT    =  visHeight * 0.5;
}



- (void)rotateFrame
{
    glRotatef( -90.0, 0.0, 0.0, 1.0 );
    glRotatef( -90.0, 0.0, 1.0, 0.0 );
    
    //gluLookAt(cos(angleX), sin(angleX), 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    
    glTranslatef( 0.0, deltaX, deltaY );
    glRotatef( angleY, 0.0, 1.0, 0.0 );
    glRotatef( angleX, 0.0, 0.0, 1.0 );
}



- (void)drawFrame
{
    glPushMatrix();
    {
        if ( showFrame )
        {
            [axises draw];
        }
        
        if ( showBoundingBox )
        {
            [boundingBox draw];
        }
    }
    glPopMatrix();
}



- (void)drawBodies
{
    if ( pos == NULL )
    {
        return;
    }
    
    glPushMatrix();
    {
        glScalef( scale, scale, scale );
        
        MUSize  offset;
        
        for ( MUSize i = 0; i < numGal; i++ )
        {
            offset = i * numBodGal;
            
            glPointSize( 10.0 );
            body->draw( pos[ offset ], vel[ offset ] );
            
            glPointSize( 1.0 );
            
            for ( MUSize j = 1; j < numBodGal; j++ )
            {
                body->draw( pos[ offset + j ], vel[ offset + j ] );
            }
        }
        
        /*for ( MUSize j = 1; j < numBodies; j++ )
        {
            body->draw( pos[ j ], vel[ j ] );
        }*/
    }
    glPopMatrix();
}



- (void)moveBodiesCamera
{
    glMatrixMode( GL_PROJECTION );
    
    glLoadIdentity();
    
    glFrustum( visCooL, visCooR, visCooB, visCooT, 1.0, 100.0 );
    
    
    glMatrixMode( GL_MODELVIEW );
    
    glLoadIdentity();
    
    gluLookAt( vPx, vPy, vPz, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0 );
}



- (void)moveFrameCamera
{
    glMatrixMode( GL_PROJECTION );
    
    glLoadIdentity();
    
    glFrustum( -visFrameW * 0.5, visFrameW * 0.5, -visFrameH * 0.5, visFrameH * 0.5, 1.0, 100.0 );
    
    
    glMatrixMode( GL_MODELVIEW );
    
    glLoadIdentity();
    
    gluLookAt( vPx, vPy, vPz, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0 );
}



- (MUPoint3)convertFromWindowCoord:(MUPoint3)winPoint
{
    GLdouble    winZ = 0.0;
    GLdouble    glPointX;
    GLdouble    glPointY;
    GLdouble    glPointZ;
    GLint       viewport[4]    = { static_cast<GLint>(0.0) };
    GLdouble    modelView[16]  = { 0.0 };
    GLdouble    projection[16] = { 0.0 };
    
    glGetIntegerv( GL_VIEWPORT, viewport );
    glGetDoublev( GL_MODELVIEW_MATRIX, modelView );
    glGetDoublev( GL_PROJECTION_MATRIX, projection );
    
    glReadPixels((GLint)winPoint.x, (GLint)winPoint.y, 1, 1, GL_DEPTH_COMPONENT, GL_DOUBLE, &winZ);
    
    gluUnProject( (GLdouble)winPoint.x, (GLdouble)winPoint.y, (GLdouble)winZ,
                  modelView, projection, viewport,
                  &glPointX, &glPointY, &glPointZ );
    
    return muPoint3( glPointX, glPointY, glPointZ );
}



- (void)updateSceneWithParameters:(GALSimulationParameters*)someParams
                    positionsData:(NSData*)positionsData
                   velocitiesData:(NSData*)velocitiesData
{
    simParams        = someParams;
    numBodies        = simParams.numBodies;
    numGal           = simParams.numGalaxies;
    numBodGal        = simParams.numBodiesGal;
    posData          = positionsData;
    velData          = velocitiesData;
    pos              = (const MUReal3*)posData.bytes;
    vel              = (const MUReal3*)velData.bytes;
    
    scale            = 1.0 / simParams.uniMaxRadius;
    self.extension   = 2.0 / ( zoom * scale );
    
    maxSpeed         = 0.0;
    minSpeed         = muNorm2( vel[0] );
    
    for ( MUSize i = 0; i < numBodies; ++i )
    {
        MUReal speed = muNorm2( vel[ i ] );
        
        if ( speed > maxSpeed )
        {
            maxSpeed = speed;
        }
        
        if ( speed < minSpeed )
        {
            minSpeed = speed;
        }
    }
    
    body->minSpeed() = minSpeed;
    body->maxSpeed() = maxSpeed;
    
    body->computeDeltaSpeed();
    
    [self setNeedsDisplay: YES];
}



- (void)centerFrameOrigin
{
    deltaX =  0.0;
    deltaY =  0.0;
    
    zoom =  1.0;
    
    mouse.x     = 0.0;
    mouse.y     = 0.0;
    zoomPoint.x = 0.0;
    zoomPoint.y = 0.0;
    
    mouseEvent = NO;
    
    visWidth  =  1.0;
    visHeight =  1.0;
    visCooL   = -visWidth  * 0.5;
    visCooR   =  visWidth  * 0.5;
    visCooB   = -visHeight * 0.5;
    visCooT   =  visHeight * 0.5;
    
    self.rotationSens = 0.25;
    self.zoomSens     = 0.005;
    
    [self reshape];
    
    [self setNeedsDisplay: YES];
}



- (void) zoom:(GLfloat)zf
          min:(GLfloat)minZf
          max:(GLfloat)maxZf
     property:(GLfloat *)sc
{
    *sc += zf;
    
    if ( *sc < minZf )
    {
        *sc = minZf;
    }
    else if ( *sc > maxZf )
    {
        *sc = maxZf;
    }
}



- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    [self setNeedsDisplay: YES];
}

@end
