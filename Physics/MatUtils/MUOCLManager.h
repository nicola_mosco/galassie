//
//  MUOCLManager.h
//  MatUtils
//
//  Created by Nicola Mosco on 19/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#define __CL_ENABLE_EXCEPTIONS

#import <map>
#import "MUOCLInterface.hpp"
#import "MUTypes.h"


namespace mu
{
    class OCLManager
    {
    public:
        OCLManager( const std::vector<std::string>& sourceCodeVec,
                    const std::string& clKernelName,
                    const MUSize numWorkItems );
        
        const
        std::vector<cl::Platform>&      platforms() const { return  platforms_; }
        const
        cl::Context&                    context() const { return context_; }
        const
        std::vector<cl::Device>&        devices() const { return devices_; }
        const
        cl::CommandQueue&               commandQueue() const { return queue_; }
        const
        cl::Program&                    program() const { return program_; }
        const
        cl::Kernel&                     kernel() const { return kernel_; }
        const
        std::string&                    kernelName() const { return kernelName_; }
        
        void                            setup( const cl_device_type devType = CL_DEVICE_TYPE_GPU );
        
        void                            addBuffer( const MUSize length,
                                                   const MUSize kArg,
                                                   const cl_mem_flags flags );
        
        void                            writeBuffer( const MUSize kArg,
                                                     const MUSize length,
                                                     const MUBuffer buffer )
        {
            queue_.enqueueWriteBuffer( buffers_.find( kArg )->second,
                                       CL_TRUE,
                                       0,
                                       length,
                                       buffer );
        }
        
        void                            readBuffer( const MUSize kArg,
                                                    const MUSize length,
                                                    MUBuffer buffer )
        {
            queue_.enqueueReadBuffer( buffers_.find( kArg )->second,
                                      CL_TRUE,
                                      0,
                                      length,
                                      buffer );
        }
        
        void                            operator()()
        {
            queue_.enqueueNDRangeKernel( kernel_,
                                         cl::NullRange,
                                         global_,
                                         local_ );
        }
        
        
        
    private:
        std::string                     kernelName_;
        cl::Program::Sources            sources_;
        MUSize                          numWorkItems_;
        
        std::vector<cl::Platform>       platforms_;
        cl::Context                     context_;
        std::vector<cl::Device>         devices_;
        cl::CommandQueue                queue_;
        cl::Program                     program_;
        cl::Kernel                      kernel_;
        std::map<MUSize, cl::Buffer>    buffers_;
        cl::NDRange                     global_;
        cl::NDRange                     local_;
    };
}
