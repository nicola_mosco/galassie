//
//  PHYSimulator.h
//  Physics
//
//  Created by Nicola Mosco on 13/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYSimulator__
#define __Physics__PHYSimulator__

#include <OpenCL/OpenCL.h>

#include "PHYSingleTaskDevice.h"
#include "PHYSystem.h"


namespace phy
{
    class Simulator : public SingleTaskDevice // Simulator class is a pure interface.
    {
    public:
        virtual ~Simulator() = 0;
        
    public: // Device specific actions and settings.
        void acquireSystem(System::SPtrType system); // A Simulator simulates a physical system.
        void setNotificationInterval(const Real notificationInterval); // Time step to retrieve data
                                                                       // from simulation.
        
        
        
    protected: // Protected c-tor: only for subclasses.
        explicit Simulator(const cl_device_type clDeviceType, // On which physical device are we going to do the simulation.
                           const std::string& description);   // Parameter defined in subclasses.
        
    protected: // Task cannot yet be implemented. Subclassing is needed.
        virtual void task() = 0;
        
    protected:
        Real notificationInterval_;
        
    protected:
        System::SPtrType system_;
        
        
        
    private:
        cl_device_type physDevType_;
        
    private:  // We do not want one to be able to use default c-tor.
        Simulator() = delete;
        Simulator(const Simulator& simulator) = delete;
        const Simulator& operator=(const Simulator& simulator) = delete;
    };
}

#endif /* defined(__Physics__PHYSimulator__) */
