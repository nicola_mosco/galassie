//
//  MUSystem.h
//  MatUtils
//
//  Created by Nicola Mosco on 08/05/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MUGeneralia.h"


namespace mu
{
    class System
    {
    public:
        System( const MUSize numBodies,
                NSMutableData* posData,
                NSMutableData* velData,
                NSMutableData* massesData );
        ~System() {}
        
        MUSize              numBodies() const { return numBodies_; };
        MUReal3*            pos() { return pos_; }
        MUReal3*            vel() { return vel_; }
        MUReal*             masses() { return masses_; }
        
        MUReal3             cmPos() const;
        MUReal3             cmVel() const;
        
        MUReal              mass() const { return mass_; }
        MUReal&             energy() { return energy_; }
        MUReal              energy() const { return energy_; }
        
        NSMutableData*      posData() { return posData_; }
        NSMutableData*      velData() { return velData_; }
        NSMutableData*      massesData() { return massesData_; }
        
        
        
    private:
        void                computeTotalMass();
        
    private:
        MUSize              numBodies_;
        MUReal3*            pos_;
        MUReal3*            vel_;
        MUReal*             masses_;
        MUReal3*            accel_;
        
    private:
        MUReal              mass_;
        MUReal              energy_;
        
    private:
        NSMutableData*      posData_;
        NSMutableData*      velData_;
        NSMutableData*      massesData_;
    };
}
