//
//  PHYBody.h
//  Physics
//
//  Created by Nicola Mosco on 06/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYBody__
#define __Physics__PHYBody__

#include <list>

#include "PHYGeometry.h"


namespace phy
{
    class Body : public Geometry
    {
    public:
        typedef std::unique_ptr<Body> PtrType;
        typedef std::list<PtrType> ListType;
        
    public:
        Body(const ReferenceFrame& aRefFrame = ReferenceFrame{},
             const std::string& name = "Body");
        virtual ~Body() = default;
        
    public:
        const Real mass() const;
        Real& mass();
        
    public:
        RVec3 pos();
        RVec3 vel();
        
        
        
    private:
        Real mass_;
    };
}

#endif /* defined(__Physics__PHYBody__) */
