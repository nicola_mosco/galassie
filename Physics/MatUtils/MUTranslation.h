//
//  MUTranslation.h
//  MatUtils
//
//  Created by Nicola Mosco on 10/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import "MUAffineTransformation.h"
#import "MUTypes.h"

@interface MUTranslation : MUAffineTransformation
{
    MUReal3      vector;
}

@property (assign) MUReal3 vector;


+ (MUTranslation*)translationWithVector:(MUReal3)aVector;


- (id)initWithVector:(MUReal3)aVector;

- (void)applyToData:(NSMutableData *)pointsData
          withRange:(NSRange)range;

@end
