//
//  PHYModule.h
//  Physics
//
//  Created by Nicola Mosco on 06/04/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYModule__
#define __Physics__PHYModule__

#include <memory>
#include <list>


namespace phy
{
    class Module
    {
    public:
        virtual ~Module() = default;
        
    public:
        void setParent(std::weak_ptr<Module> module);
        virtual void addSubModule(std::shared_ptr<Module> module);
        virtual void removeSubModule(std::shared_ptr<Module> module);
        
        
        
    protected:
        Module() = default;
        
        
        
    private:
        std::weak_ptr<Module> parent_;
        std::list<std::shared_ptr<Module>> sub_modules_;
    };
}

#endif /* defined(__Physics__PHYModule__) */
