//
//  PHYNotification.h
//  Physics
//
//  Created by Nicola Mosco on 15/04/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYNotification__
#define __Physics__PHYNotification__

#include <string>


namespace phy
{
    class Module;
    
    
    class Notification
    {
    public:
        Notification(const std::string name, Module& sender);
        virtual ~Notification() = default;
        
    public:
        std::string name() const;
        Module& sender();
        
        
        
    private:
        std::string name_;
        Module& sender_;
    };
}

#endif /* defined(__Physics__PHYNotification__) */
