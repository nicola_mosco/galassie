//
//  GALViewState.m
//  Galassie
//
//  Created by Nicola Mosco on 03/04/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import "GALViewState.h"


NSString* GALBodiesStyle    = @"GALBodiesStyle";
NSString* GALBodiesSpheres  = @"GALBodiesSpheres";
NSString* GALBodiesVertices = @"GALBodiesVertices";



@implementation GALViewState

@synthesize state;



- (id)init
{
    self = [super init];
    
    if ( self != nil )
    {
        [state setValue: GALBodiesSpheres
                 forKey: GALBodiesStyle];
        
        properties = [NSArray arrayWithObjects: GALBodiesStyle, nil];
    }
    
    return self;
}

@end
