//
//  NSNumber+GALNumberExtension.mm
//  Galassie
//
//  Created by Nicola Mosco on 17/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import "NSNumber+GALNumberExtension.h"

@implementation NSNumber (GALNumberExtension)

- (phy::Real)value
{
    if (PHY_FLOATING == PHY_FLOAT)
    {
        return self.floatValue;
    }
    else if (PHY_FLOATING == PHY_DOUBLE)
    {
        return self.doubleValue;
    }
    else
    {
        @throw @"Unknown floating type";
    }
}



- (phy::Size)sizeValue
{
    return (phy::Size)self.longLongValue;
}

@end
