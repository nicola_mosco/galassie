//
//  MURotation.mm
//  MatUtils
//
//  Created by Nicola Mosco on 10/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import "MURotation.h"


//using namespace cryph;


@implementation MURotation

@synthesize axis, angle;


#if 0
+ (MURotation *)rotationWithAxis:(MUReal3)anAxis
                           angle:(MUReal)anAngle
{
    return [[MURotation alloc] initWithAxis: anAxis
                                      angle: anAngle];
}



- (id)initWithAxis:(MUReal3)anAxis
             angle:(MUReal)anAngle
{
    self = [super init];
    
    if (self)
    {
        axis  = anAxis;
        angle = anAngle;
    }
    
    return self;
}



/*- (Matrix3x3)rotationMatrix
{
    return Matrix3x3( AffVector( axis.x, axis.y, axis.z ), angle );
}*/



- (void)applyToData:(NSMutableData *)pointsData
          withRange:(NSRange)range
{
    MUReal3*    points      = ((MUReal3*)pointsData.mutableBytes) + range.location;
    Matrix3x3   rotationM   = Matrix3x3( AffVector( axis.x, axis.y, axis.z ), angle );
    AffVector   vector;
    AffVector   newVector;
    
    for ( MUSize i = 0; i < range.length; i++ )
    {
        vector.x()      = points[ i ].x;
        vector.y()      = points[ i ].y;
        vector.z()      = points[ i ].z;
        
        newVector       = rotationM * vector;
        
        points[ i ].x   = (MUReal)newVector.x();
        points[ i ].y   = (MUReal)newVector.y();
        points[ i ].z   = (MUReal)newVector.z();
    }
    
    return;
}
#endif
@end
