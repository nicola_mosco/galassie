//
//  PHYConsole.cpp
//  Physics
//
//  Created by Nicola Mosco on 29/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#include "PHYConsole.h"


using namespace phy;



Console::Console(const std::string& description)
    : SerialTaskDevice(description)
{}



Console::~Console() = default;



void
Console::setLogInfo(const std::string& logInfo)
{
    line_ = logInfo;
}



void
Console::writeLine(const std::string& line)
{
    performTask(^{ setLogInfo(line); }, ^{});
}
