//
//  MUInteraction.h
//  MatUtils
//
//  Created by Nicola Mosco on 09/05/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#import <string>
#import <vector>
#import <map>
#import "MUTypes.h"
#import "MUOctree.h"
#import "MUSystem.h"
#import "MUOCLManager.h"


namespace mu
{
    class Interaction
    {
    public:
        typedef std::map<std::string, MUReal> Params;
        
    public:
        Interaction( const std::string& name,
                     const std::string& oclKernelName,
                     const Params& params ) : name_( name ), oclKernelName_( oclKernelName ), params_( params )
        {}
        virtual ~Interaction() {}
        
        const
        std::string&                name() const { return name_; }
        const
        std::string&                oclKernelName() const { return oclKernelName_; }
        const
        MUReal                      paramForName( const std::string& pName ) const { return params_.find( pName )->second; }
        
        virtual const
        std::vector<std::string>&   oclKernelSources() const = 0;
        
        virtual
        void                        addBuffers( System* system, oct::Octree* octree, OCLManager* openCL ) = 0;
        virtual
        void                        writeBuffers( const MUReal3* pos, oct::Octree* octree, OCLManager* openCL ) = 0;
        virtual
        void                        readBuffers( MUReal3* accel, oct::Octree* octree, OCLManager* openCL ) = 0;
        
        virtual
        MUReal                      energy( System* system ) = 0;
        virtual
        MUReal                      energy( const MUSize numBodies,
                                            const MUReal3* pos,
                                            const MUReal3* vel,
                                            const MUReal* masses ) = 0;
        
        
        
    protected:
        Params          params_;
        
        
        
    private:
        std::string     name_;
        std::string     oclKernelName_;
    };
}
