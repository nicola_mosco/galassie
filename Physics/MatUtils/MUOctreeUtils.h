//
//  MUOctreeUtils.h
//  MatUtils
//
//  Created by Nicola Mosco on 07/08/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#import "MUOctreeDefs.h"


/******************* General octree management functions... ******************/

inline void octInsertLeaf( OctNode node,
                           MUReal3 pos,
                           MUReal mass )
{
    node->M      = mass;
    node->cm     = pos;
    node->isLeaf = CL_TRUE;
    
    return;
}

void octOctreeExtension( const MUReal3* pos, const cl_ulong n_bodies,
                         const MUReal3 center,
                         MUReal *const LPtr );

void octComputeAccelForBody( MUReal3 *const accPtr,
                             const MUReal3 pos,
                             const OctNode node,
                             const MUBuffer octree );

inline MUReal3 octComputeAccelFor2Bodies( const MUReal3 pos1,
                                          const MUReal3 pos2,
                                          const MUReal M2,
                                          const MUReal G,
                                          const MUReal eps )
{
    MUReal        a    = 0.0;
    MUReal        d    = 0.0;
    MUReal3       dpos = { 0.0 };
    
    dpos.x  = pos2.x - pos1.x;
    dpos.y  = pos2.y - pos1.y;
    dpos.z  = pos2.z - pos1.z;
    
    d       = sqrtf(dpos.x * dpos.x +
                    dpos.y * dpos.y +
                    dpos.z * dpos.z +
                    eps);
    
    a       = G * M2 / (d * d * d);
    
    dpos.x *= a;
    dpos.y *= a;
    dpos.z *= a;
    
    return dpos;
}



cl_int3 octCellSign( cl_uint c );



inline MUBool octNodeIsLeaf( const OctNode node )
{
    return node->isLeaf;
}



inline void octNodeSetIsLeaf( OctNode node,
                             const MUBool isLeaf )
{
    node->isLeaf = isLeaf;
    
    return;
}



inline MUBool octNodeIsEmpty( const MURef nodeOffset )
{
    return ( nodeOffset == OCTREF(0) );
}



inline MUBool octBranchIsEmpty( const OctNode node,
                               const MUCell cell )
{
    if ( cell >= OCT_NUM_BRANCHES )
    {
        return CL_FALSE;
    }
    
    return ( node->branches[ cell ] == OCTREF(0) );
}



inline MUBool octNodeBHTest( const OctNode node,
                            const MUReal d,
                            const MUReal theta )
{
    return ( node->L / d < theta );
}



inline OctHeader octGetHeader( MUBuffer octree )
{
    return (OctHeader)octree;
}



inline OctNode octGetNode( MUBuffer octree,
                          const MURef nodeOffset )
{
    if ( nodeOffset == 0 )
    {
        return NULL;
    }
    
    return (OctNode) ( octree + nodeOffset );
}



inline OctNode octGetBranch( const OctNode node,
                            const MUCell c,
                            MUBuffer octree )
{
    MURef      branchOffset = node->branches[ c ];
    
    if ( branchOffset == 0 )
    {
        return NULL;
    }
    
    return (OctNode) ( octree + branchOffset );
}



inline OctNode octGetParent( const OctNode node,
                            MUBuffer octree )
{
    if ( node->parent == 0 )
    {
        return NULL;
    }
    
    return (OctNode) ( octree + node->parent );
}



inline OctNode octGetRoot( MUBuffer octree )
{
    return octGetNode( octree, ((OctHeader)octree)->root );
}



inline MUCell octNodeCell( const OctNode node )
{
    return node->c;
}



inline void octSetNodeCell( OctNode node,
                           const MUCell cell )
{
    node->c = cell;
    
    return;
}



inline void octSetBranchCell( OctNode node,
                             const MUCell cell,
                             MUBuffer octree )
{
    octSetNodeCell( octGetBranch( node, cell, octree ), cell );
    
    return;
}

/*****************************************************************************/
