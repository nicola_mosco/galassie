//
//  GALVector3.mm
//  Galassie
//
//  Created by Nicola Mosco on 17/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import "GALVector3.h"
#import "NSNumber+GALNumberExtension.h"


@implementation GALVector3

- (id)initWithX:(NSNumber *)x
              y:(NSNumber *)y
              z:(NSNumber *)z
{
    self = [super init];
    
    if (self)
    {
        self.x = x;
        self.y = y;
        self.z = z;
    }
    
    return self;
}



- (phy::RVec3)realVector3
{
    return {self.x.value, self.y.value, self.z.value};
}



- (phy::Vector3)vector3
{
    return {self.x.value, self.y.value, self.z.value};
}

@end
