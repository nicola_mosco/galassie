//
//  GALFrame.m
//  Galassie
//
//  Created by Nicola Mosco on 04/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import "GALFrame.h"


@implementation GALFrame
{
    GLfloat colors_[ 4 ];
}

@synthesize color;


- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.color = [NSColor colorWithCalibratedRed: 0.2
                                               green: 0.6
                                                blue: 0.8
                                               alpha: 1.0];
        
        GLuint  i;
        
        CGFloat tmpColor[ 4 ];
        
        [color getComponents: tmpColor];
        
        for ( i = 0; i < 4; i++ )
        {
            colors_[ i ] = tmpColor[ i ];
        }
    }
    return self;
}



- (void)draw
{
    /*glMaterialfv( GL_FRONT_AND_BACK, GL_AMBIENT, colors_ );
    glMaterialfv( GL_FRONT_AND_BACK, GL_DIFFUSE, colors_ );
    glMaterialfv( GL_FRONT_AND_BACK, GL_SPECULAR, colors_ );*/
    
    glColor3f( colors_[0], colors_[1], colors_[2] );
}

@end
