//
//  MUSimStateManager.mm
//  MatUtils
//
//  Created by Nicola Mosco on 26/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#include "MUSimStateManager.h"
#import <stdexcept>


using namespace std;
using namespace mu;



SimStateManager::SimStateManager(const string& dataFileName) : dataFileName_(dataFileName)
{
    writeQueue_ = dispatch_queue_create("com.nicolamosco.Galassie.writeQueue", 0);
    writeGroup_ = dispatch_group_create();
}



SimStateManager::~SimStateManager()
{
    dispatch_release(writeQueue_);
    dispatch_release(writeGroup_);
}



void SimStateManager::operator()(const MUReal t,
                                 System* system)
{
    SimState*   simState = new SimState(system->numBodies(),
                                        t,
                                        system->pos(),
                                        system->vel());
    SimStatePtr statePtr(simState);
    
    dispatch_group_async(writeGroup_, writeQueue_,
                         ^{
                             writeToFile(statePtr);
                         } );
}



inline
void SimStateManager::writeToFile(SimStatePtr statePtr)
{
    FILE* dataFile = fopen(dataFileName_.c_str(), "ab");
    
    if (!dataFile)
    {
        throw runtime_error(strerror(errno));
    }
    
    statePtr->writeToFile(dataFile);
    
    fclose(dataFile);
}



void SimStateManager::wait()
{
    dispatch_group_wait(writeGroup_, DISPATCH_TIME_FOREVER);
}
