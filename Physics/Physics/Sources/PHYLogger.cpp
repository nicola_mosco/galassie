//
//  PHYLogger.cpp
//  Physics
//
//  Created by Nicola Mosco on 21/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#include <ctime>
#include <stdexcept>

#include "PHYLogger.h"


using namespace phy;



Logger::Logger()
    : SerialTaskDevice{"Logger"}
{}



Logger::~Logger() = default;



void
Logger::setLogInfo(const std::string& senderDescription,
                   const std::string& message)
{
    auto now = time(nullptr);
    
    char timestamp[30]{};
    
    if (strftime(timestamp, 30, "%F %T", localtime(&now)) == 0)
    {
        throw std::logic_error("*** ERROR: string too short for date representation ***");
    }
    
    logInfo_ = "[" + std::string(timestamp) + "] " + senderDescription + ": " + message;
}



void
Logger::attachToConsole(std::shared_ptr<Console>& console)
{
    console_ = console;
}



void
Logger::log(const std::string& senderDescription,
            const std::string& message)
{
    setLogInfo(senderDescription, message);
    
    performTask(^{}, ^{});
}



void
Logger::task()
{
    console_->writeLine(logInfo_);
}
