//
//  GALSystemBuilder.mm
//  Galassie
//
//  Created by Nicola Mosco on 08/12/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import "GALSystemBuilder.h"
#import <MatUtils/MUMathFunc.h>
#import <MatUtils/MUGeneralia.h>
#import <MatUtils/MUDistributions.h>
#import <MatUtils/MUTranslation.h>
#import <MatUtils/MURotation.h>
#import <MatUtils/MUSystem.h>
#import "GALGalaxyBuilder.h"
#import "GALSimulationParameters.h"


using namespace std;
using namespace mu;
using namespace dist;



@implementation GALSystemBuilder
{
    GALSimulationParameters*    simParams;
    
    NSMutableData*              stateData;
    NSMutableData*              massData;
    
    GALGalaxyBuilder*           galBuilder;
    
    
    NSMutableArray*             stateOpArray;
    NSMutableArray*             massOpArray;
    
    NSMutableArray*             stateArray;
    NSMutableArray*             massArray;
    NSMutableData*              galPosData;
    NSMutableData*              galVelData;
    
    MUReal3                     cmp;
    MUReal3                     cmv;
}



- (id)initWithSimulationParameters:(GALSimulationParameters *)someParams
{
    self = [super init];
    
    if (self)
    {
        simParams    = someParams;
        
        stateData    = [[NSMutableData alloc] initWithLength: 2 * simParams.numBodies * MU_SIZE_REAL3];
        massData     = [[NSMutableData alloc] initWithLength: simParams.numBodies * MU_SIZE_REAL];
        
        galBuilder   = [[GALGalaxyBuilder alloc] initWithSimulationParameters: someParams];
        
        stateOpArray = [[NSMutableArray alloc] initWithCapacity: simParams.numGalaxies];
        massOpArray  = [[NSMutableArray alloc] initWithCapacity: simParams.numGalaxies];
        stateArray   = [[NSMutableArray alloc] initWithCapacity: simParams.numGalaxies];
        massArray    = [[NSMutableArray alloc] initWithCapacity: simParams.numGalaxies];
        
        galPosData   = [[NSMutableData alloc] initWithLength: simParams.numGalaxies * MU_SIZE_REAL3];
        galVelData   = [[NSMutableData alloc] initWithLength: simParams.numGalaxies * MU_SIZE_REAL3];
        
        cmp          = muAssign( 0.0 );
        cmv          = muAssign( 0.0 );
    }
    
    return self;
}



- (void)generateGalaxyPosVelInQueue:(NSOperationQueue*)galQueue
{
    [galQueue addOperationWithBlock:
     ^{
         MUSize      numGal = simParams.numGalaxies;
         MUReal3*    pos    = (MUReal3*)galPosData.mutableBytes;
         MUReal3*    vel    = (MUReal3*)galVelData.mutableBytes;
         MUReal      r;
         MUReal      v;
         MUReal      theta;
         MUReal      phi;
         MUReal      rst;
         MUReal      d      = simParams.d;
         Uniform     rDist( simParams.uniMinRadius, simParams.uniMaxRadius );
         Uniform     thetaDist( 0.0, M_PI );
         Uniform     phiDist( 0.0, 2.0 * M_PI );
         Uniform     velDist( simParams.minSpeed, simParams.maxSpeed );
         
         r      = rDist.get();
         theta  = thetaDist.get();
         phi    = phiDist.get();
         rst    = r * Sin( theta );
         v      = velDist.get();
         
         pos[0] = muReal3( rst * Cos( phi ),
                           rst * Sin( phi ),
                           r * Cos( theta ) );
         vel[0] = muScale( pos[0], v / r );
         
         for ( MUSize i = 1; i < numGal; i++ )
         {
             for ( MUSize j = 0; j < i; j++ )
             {
                 do
                 {
                     r        = rDist.get();
                     theta    = thetaDist.get();
                     phi      = phiDist.get();
                     rst      = r * Sin( theta );
                     
                     pos[ i ] = muReal3( rst * Cos( phi ),
                                         rst * Sin( phi ),
                                         r * Cos( theta ) );
                 }
                 while ( muDist2( pos[ i ], pos[ j ] ) < d );
             }
             
             v        = velDist.get();
             vel[ i ] = muScale( pos[ i ], v / r );
         }
     }];
    
    return;
}



- (void)scheduleBodyOperationsInQueue:(NSOperationQueue*)galQueue
{
    MUSize             numGal    = simParams.numGalaxies;
    MUSize             numBodGal = simParams.numBodiesGal;
    
    for ( MUSize i = 0; i < numGal; i++ )
    {
        NSMutableData*          galStateData = [[NSMutableData alloc] initWithLength: 2 * numBodGal * MU_SIZE_REAL3];
        NSMutableData*          galMassData  = [[NSMutableData alloc] initWithLength: numBodGal * MU_SIZE_REAL];
        NSInvocationOperation*  stateOp      = [[NSInvocationOperation alloc] initWithTarget: galBuilder
                                                                                    selector: @selector( generateStateData: )
                                                                                      object: galStateData];
        NSInvocationOperation*  massOp       = [[NSInvocationOperation alloc] initWithTarget: galBuilder
                                                                                    selector: @selector( generateMassData: )
                                                                                      object: galMassData];
        
        [stateOpArray addObject: stateOp];
        [massOpArray addObject: massOp];
        
        [stateArray addObject: galStateData];
        [massArray addObject: galMassData];
    }
    
    [galQueue addOperations: stateOpArray
          waitUntilFinished: NO];
    [galQueue addOperations: massOpArray
          waitUntilFinished: NO];
    
    return;
}



- (void)scheduleGalaxiesOperations
{
    NSOperationQueue*   galQueue = [[NSOperationQueue alloc] init];
    
    if ( simParams.numGalaxies > 1 )
    {
        [self generateGalaxyPosVelInQueue: galQueue];
    }
    
    [self scheduleBodyOperationsInQueue: galQueue];
    
    [galQueue waitUntilAllOperationsAreFinished];
    
    return;
}



- (MURotation*)randomRotation
{
    MUReal3     axis;
    MUReal      angle;
    MUReal      theta;
    MUReal      phi;
    Uniform     phiDist( 0.0, 2.0 * M_PI );
    Uniform     thetaDist( 0.0, M_PI );
    
    angle = phiDist.get();
    theta = thetaDist.get();
    phi   = phiDist.get();
    
    axis  = muReal3( Sin( theta ) * Cos( phi ), Sin( theta ) * Sin( phi ), Cos( theta ) );
    
    return [MURotation rotationWithAxis: axis
                                  angle: angle];
}



- (void)deployGalaxies
{
    MUSize              numGal      = simParams.numGalaxies;
    MUSize              numBodGal   = simParams.numBodiesGal;
    MUReal3*            pos         = (MUReal3*)galPosData.mutableBytes;
    MUReal3*            vel         = (MUReal3*)galVelData.mutableBytes;
    NSOperationQueue*   deployQueue = [[NSOperationQueue alloc] init];
    
    @autoreleasepool
    {
        for ( MUSize i = 0; i < numGal; i++ )
        {
            [deployQueue addOperationWithBlock:
             ^{
                 MUTranslation*  posTrl  = [MUTranslation translationWithVector: pos[ i ]];
                 MUTranslation*  velTrl  = [MUTranslation translationWithVector: vel[ i ]];
                 MURotation*     rot     = [self randomRotation];
                 NSMutableData*  galData = [stateArray objectAtIndex: i];
                 
                 [rot applyToData: galData
                        withRange: NSMakeRange( 0, 2 * numBodGal )];
                 [posTrl applyToData: galData
                           withRange: NSMakeRange( 0, numBodGal )];
                 [velTrl applyToData: galData
                           withRange: NSMakeRange( numBodGal, numBodGal )];
             }];
        }
        
        [deployQueue waitUntilAllOperationsAreFinished];
    }
    
    return;
}



- (void)writeSystemStateDataInQueue:(NSOperationQueue*)writeQueue
{
    MUSize              numGal      = simParams.numGalaxies;
    MUSize              numBod      = simParams.numBodies;
    MUSize              numBodGal   = simParams.numBodiesGal;
    MUReal3*            pos         = (MUReal3*)stateData.mutableBytes;
    MUReal3*            vel         = pos + numBod;
    
    for ( MUSize i = 0; i < numGal; i++ )
    {
        MUReal3*    galPos = (MUReal3*)[[stateArray objectAtIndex: i] mutableBytes];
        MUReal3*    galVel = galPos + numBodGal;
        
        [writeQueue addOperationWithBlock:
         ^{
             for ( MUSize j = 0; j < numBodGal; j++ )
             {
                 pos[ i * numBodGal + j ] = galPos[ j ];
             }
         }];
        
        [writeQueue addOperationWithBlock:
         ^{
             for ( MUSize j = 0; j < numBodGal; j++ )
             {
                 vel[ i * numBodGal + j ] = galVel[ j ];
             }
        }];
    }
    
    return;
}



- (void)writeSystemMassDataInQueue:(NSOperationQueue*)writeQueue
{
    MUSize      numGal      = simParams.numGalaxies;
    MUSize      numBodGal   = simParams.numBodiesGal;
    MUReal*     masses      = (MUReal*)massData.mutableBytes;
    
    for ( MUSize i = 0; i < numGal; i++ )
    {
        MUSize  offset    = i * numBodGal;
        MUReal* galMasses = (MUReal*)[[massArray objectAtIndex: i] mutableBytes];
        
        [writeQueue addOperationWithBlock:
         ^{
             for ( MUSize j = 0; j < numBodGal; j++ )
             {
                 masses[ offset + j ] = galMasses[ j ];
             }
        }];
    }
    
    return;
}



- (void)writeSystemDataBuffers
{
    NSOperationQueue*   writeQueue = [[NSOperationQueue alloc] init];
    
    [self writeSystemStateDataInQueue: writeQueue];
    [self writeSystemMassDataInQueue: writeQueue];
    
    [writeQueue waitUntilAllOperationsAreFinished];
    
    return;
}



- (void)centerOfMassPos:(id)object
{
    MUReal      M      = 0.0;
    MUSize      numBod = simParams.numBodies;
    MUReal3*    pos    = (MUReal3*)stateData.mutableBytes;
    MUReal*     m      = (MUReal*)massData.mutableBytes;
    
    cmp = muReal3( 0.0, 0.0, 0.0 );
    
    for ( MUSize i = 0; i < numBod; i++ )
    {
        cmp.s[0] += m[ i ] * pos[ i ].s[0];
        cmp.s[1] += m[ i ] * pos[ i ].s[1];
        cmp.s[2] += m[ i ] * pos[ i ].s[2];
        
        M     += m[ i ];
    }
    
    cmp = muInvScale( cmp, M );
    
    return;
}



- (void)centerOfMassVel:(id)object
{
    MUReal      M      = 0.0;
    MUSize     numBod = simParams.numBodies;
    MUReal3*    vel    = ((MUReal3*)stateData.mutableBytes) + numBod;
    MUReal*     m      = (MUReal*)massData.mutableBytes;
    
    cmv = muReal3( 0.0, 0.0, 0.0 );
    
    for ( MUSize i = 0; i < numBod; i++ )
    {
        cmv.s[0] += m[ i ] * vel[ i ].s[0];
        cmv.s[1] += m[ i ] * vel[ i ].s[1];
        cmv.s[2] += m[ i ] * vel[ i ].s[2];
        
        M     += m[ i ];
    }
    
    cmv = muInvScale( cmv, M );
    
    return;
}



- (void)writeSystemStateInCenterOfMassFrame
{
    MUSize                  numBod  = simParams.numBodies;
    MUReal3*                pos     = (MUReal3*)stateData.mutableBytes;
    MUReal3*                vel     = pos + numBod;
    NSOperationQueue*       cmQueue = [[NSOperationQueue alloc] init];
    NSInvocationOperation*  cmPosOp = [[NSInvocationOperation alloc] initWithTarget: self
                                                                           selector: @selector( centerOfMassPos: )
                                                                             object: nil];
    NSInvocationOperation*  cmVelOp = [[NSInvocationOperation alloc] initWithTarget: self
                                                                           selector: @selector( centerOfMassVel: )
                                                                             object: nil];
    
    [cmQueue addOperation: cmPosOp];
    [cmQueue addOperation: cmVelOp];
    
    [cmQueue waitUntilAllOperationsAreFinished];
    
    
    for ( MUSize i = 0; i < numBod; i++ )
    {
        pos[ i ] = muDiff( pos[ i ], cmp );
        vel[ i ] = muDiff( vel[ i ], cmv );
    }
    
    return;
}



- (void)generateSystemData
{
    [self scheduleGalaxiesOperations];
    
    if ( simParams.numGalaxies > 1 )
    {
        [self deployGalaxies];
    }
    
    [self writeSystemDataBuffers];
    [self writeSystemStateInCenterOfMassFrame];
    
    return;
}



- (System *)buildSystem
{
    MUSize          numBodSize = simParams.numBodies * MU_SIZE_REAL3;
    NSMutableData*  posData    = [[NSMutableData alloc] initWithData: stateData];
    NSData*         subData    = [stateData subdataWithRange: NSMakeRange( numBodSize, numBodSize )];
    NSMutableData*  velData    = [[NSMutableData alloc] initWithData: subData];
    
    return new System( simParams.numBodies, posData, velData, massData );
}

@end
