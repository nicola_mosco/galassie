//
//  PHYConsole.h
//  Physics
//
//  Created by Nicola Mosco on 29/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//


/*
 * Console is a Device intended to be attached (via specialization) to some
 * platform specific logging facility, such as a text view in an application
 * window or a system console.
 * Concurrent access of this device from other devices is guaranteed to be
 * safe.
 *
 */


#ifndef __Physics__PHYConsole__
#define __Physics__PHYConsole__

#include "PHYSerialTaskDevice.h"


namespace phy
{
    class Console : public SerialTaskDevice
    {
    public:
        virtual ~Console() = 0;
        
    public:
        void setLogInfo(const std::string& logInfo);
        
    public: // Actions
        void writeLine(const std::string& line); // This method prints to the
                                                 // console the specified
                                                 // string.
        
        
        
    protected:
        explicit Console(const std::string& description);
        
    protected:
        virtual void task() = 0;
        
        
        
    private:
        std::string line_;
        
    private:
        Console() = delete;
        Console(const Console& console) = delete;
        const Console& operator=(const Console& console) = delete;
    };
}

#endif /* defined(__Physics__PHYConsole__) */
