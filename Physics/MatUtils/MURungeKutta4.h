//
//  MURungeKutta4.h
//  MatUtils
//
//  Created by Nicola Mosco on 13/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import <array>
#import "MUAlgorithm.h"
#import "MUTypes.h"


namespace mu
{   
    class RungeKutta4 : public Algorithm
    {
    public:
        typedef std::shared_ptr<RungeKutta4> RK4Ptr;
        
    public:
        RungeKutta4( const MUSize numBodies,
                     Integrator* integrator,
                     const MUReal intStep );
        ~RungeKutta4();
        
        void                operator()( const MUReal t,
                                        MUReal3* pos,
                                        MUReal3* vel );
        
        MUSize              numBodies() const { return numBodies_; }
        MUReal&             intStep() { return h_; }
        
        
        
    private:
        MUSize              numBodies_;
        MUReal              h_;
        
    private:
        typedef std::array<MUReal3*, 4> AuxArray;
        
        MUReal3*            posTmp_;
        MUReal3*            velTmp_;
        AuxArray            accelTmp_;
    };
}
