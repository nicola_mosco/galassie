//
//  GALParameter.mm
//  Galassie
//
//  Created by Nicola Mosco on 05/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import "GALParameter.h"

@implementation GALParameter

+ (GALParameter *)parameterWithName:(NSString *)name
                              value:(NSNumber *)value
                         identifier:(NSString*)identifier
                          formatter:(NSNumberFormatter *)formatter
{
    return [[GALParameter alloc] initWithName: name
                                        value: value
                                   identifier: identifier
                                    formatter: formatter];
}



- (id)initWithName:(NSString *)name
             value:(NSNumber *)value
        identifier:(NSString*)identifier
         formatter:(NSNumberFormatter *)formatter
{
    self = [super init];
    
    if (self)
    {
        self.name       = name;
        self.value      = value;
        self.identifier = identifier;
        self.formatter  = formatter;
    }
    
    return self;
}



- (NSString *)description
{
    return [self.formatter stringFromNumber: self.value];
}



- (id)copyWithZone:(NSZone *)zone
{
    GALParameter* newPar = [GALParameter parameterWithName: self.name
                                                     value: self.value
                                                identifier: self.identifier
                                                 formatter: self.formatter];
    
    return newPar;
}

@end
