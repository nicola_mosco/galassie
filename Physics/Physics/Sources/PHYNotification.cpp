//
//  PHYNotification.cpp
//  Physics
//
//  Created by Nicola Mosco on 15/04/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#include "PHYModule.h"
#include "PHYNotification.h"


using namespace phy;



Notification::Notification(const std::string name,
                           Module& sender)
    : name_{std::move(name)}, sender_{sender}
{}



std::string
Notification::name() const
{
    return name_;
}



Module&
Notification::sender()
{
    return sender_;
}
