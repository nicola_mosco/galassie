//
//  MUTranslation.mm
//  MatUtils
//
//  Created by Nicola Mosco on 10/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import "MUTranslation.h"
#import "MUGeneralia.h"


@implementation MUTranslation

+ (MUTranslation *)translationWithVector:(MUReal3)aVector
{
    return [[MUTranslation alloc] initWithVector: aVector];
}



- (id)initWithVector:(MUReal3)aVector
{
    self = [super init];
    
    if (self)
    {
        vector = aVector;
    }
    
    return self;
}



- (void)applyToData:(NSMutableData *)pointsData
          withRange:(NSRange)range
{
    MUReal3*    points = ((MUReal3*)pointsData.mutableBytes) + range.location;
    
    for ( MUSize i = 0; i < range.length; i++ )
    {
        points[ i ] = muSum( points[ i ], vector );
    }
    
    return;
}

@end
