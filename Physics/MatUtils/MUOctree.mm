//
//  MUOctree.mm
//  MatUtils
//
//  Created by Nicola Mosco on 03/09/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#import "MUOctree.h"
#import <iostream>


using namespace std;
using namespace mu;
using namespace oct;



Octree::Octree( const MUSize numBodies,
                const MUReal L ) : OctreeMemory( numBodies )
{
    _pos        = NULL;
    _masses     = NULL;
    _accel      = NULL;
    
    reset();
    
    MUReal3 center  = { MU_REAL(0) };
    
    _header->center = center;
    _header->L      = L;
    _header->G      = 0.0;
    _header->eps    = 0.0;
    _header->theta  = 0.0;
    
    queue_ = dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0 );
}



Octree::Octree( const MUSize numBodies,
                const MUReal3* pos,
                const MUReal* masses,
                MUReal3* accel,
                const MUReal3 center,
                const MUReal G,
                const MUReal eps,
                const MUReal theta ) : OctreeMemory( numBodies )
{
    _pos        = NULL;
    _masses     = NULL;
    _accel      = NULL;
    
    _header = getHeader();
    
    _header->center = center;
    _header->G      = G;
    _header->eps    = eps;
    _header->theta  = theta;
    
    update( pos, masses, accel );
    
    queue_ = dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0 );
}



Octree::~Octree() {}



OctHeader
Octree::header()
{
    return _header;
}



void
Octree::setupHeader(const MUReal G,
                    const MUReal eps,
                    const MUReal theta)
{
    _header->G     = G;
    _header->eps   = eps;
    _header->theta = theta;
    
    return;
}



OctNode
Octree::rootNode()
{
    return _root;
}



OctNode
Octree::nodeForIndex(const MURef nodeIndex)
{
    OctNode node       = NULL;
    MURef   nodeOffset = 0;
    
    nodeOffset = offsetForNodeIndex(nodeIndex);
    
    if (nodeOffset > current())
    {
        node = NULL;
    }
    else
    {
        node = OctNode(_buffer + nodeOffset);
    }
    
    return node;
}



OctNode
Octree::nodeForOffset(const MURef nodeOffset)
{
    OctNode node;
    
    if (nodeOffset > current() || nodeOffset == 0)
    {
        node = NULL;
    }
    else
    {
        node = OctNode( _buffer + nodeOffset);
    }
    
    return node;
}



OctNode
Octree::branchForCell(OctNode node,
                      const MUCell cell)
{
    return nodeForOffset(node->branches[cell]);
}



MURef
Octree::nodeOffsetForIndex(const MURef nodeIndex)
{
    MURef nodeOffset = 0;
    
    nodeOffset = offsetForNodeIndex(nodeIndex);
    
    if (nodeOffset > current())
    {
        nodeOffset = 0;
    }
    
    return nodeOffset;
}



MUReal&
Octree::extension()
{
    return _header->L;
}



const MUReal3*
Octree::pos() const
{
    return _pos;
}



const MUReal*
Octree::masses() const
{
    return _masses;
}




MUReal3*
Octree::accel()
{
    return _accel;
}



void
Octree::update(const MUReal3* pos,
               const MUReal* masses,
               MUReal3* accel)
{
    if (pos != NULL)
    {
        _pos = pos;
    }
    
    if (masses != NULL)
    {
        _masses = masses;
    }
    
    if (accel != NULL)
    {
        _accel = accel;
    }
    
    
    update();
}



MUBool
Octree::insertBody(const MUReal3 pos,
                   const MUReal mass,
                   const MUReal lambda)
{
    MUBool ins;
    
    ins = insertNode(&_header->root,
                     0,
                     _header->L,
                     _header->center,
                     pos,
                     mass,
                     lambda);
    
    if (ins)
    {
        _header->n_bodies++;
        _header->cm = _root->cm;
        _header->M  = _root->M;
    }
    
    return ins;
}



MUBool
Octree::insertNode(MURef* nodeOffsetPtr,
                   const MURef parentOffset,
                   const MUReal L,
                   const MUReal3 center,
                   const MUReal3 pos,
                   const MUReal mass,
                   const MUReal lambda)
{
    MUBool      ins  = false;
    OctNode     node = NULL;
    
    if (nodeIsEmpty(*nodeOffsetPtr)) /* Il node è vuoto e *
                                      * posso inserire    *
                                      * una foglia.       */
    {
        node = allocNewNode(nodeOffsetPtr);
        
        node->parent   = parentOffset;
        node->L        = L;
        node->center   = center;
        
        octInsertLeaf(node, pos, mass);
        
        ins = true;
    }
    else if (!nodeIsLeaf(*nodeOffsetPtr)) /* Il node è già    *
                                           * occupato, ma non *
                                           * è una foglia.    *
                                           * Bisogna          *
                                           * continuare       *
                                           * finché non       *
                                           * si trova         *
                                           * un node libero.  */
    {
        ins  = insertNodeFindingCell(nodeOffsetPtr, pos, mass, lambda);
        
        if (ins)
        {
            node = nodeForOffset(*nodeOffsetPtr);
            
            nodeCenterOfMass(node);
        }
    }
    else /* Il node è una foglia.                                           *
          * Bisogna prima spostare la foglia e poi inserire il nuovo corpo. */
    {
        node = nodeForOffset(*nodeOffsetPtr);
        
        /* Vedo se le posizioni dei due corpi coincidono: 
           se pos ≡ cm, allora si rigetta quel corpo.     */
        
        if (muEqual(pos, node->cm) || node->L < lambda)
        {
            return false;
        }
        
        
        ins = insertNodeFindingCell(nodeOffsetPtr, node->cm, node->M, lambda) && // Ora devo spostare la foglia un livello più in basso...
              insertNodeFindingCell(nodeOffsetPtr, pos, mass, lambda); // Provo ad inserire il nuovo node...
        
        if (ins)
        {
            nodeCenterOfMass(node);
        }
    }
    
    return ins;
}



MUBool Octree::insertNodeFindingCell(MURef *nodeOffsetPtr,
                                     const MUReal3 pos,
                                     const MUReal mass,
                                     const MUReal lambda)
{
    MUBool          ins    = false;
    cl_uint         c      = 0;
    MUReal          L      = 0.0;
    MUReal3         center = {0.0};
    OctNode         node   = NULL;
    
    node = nodeForOffset(*nodeOffsetPtr);
    
    c    = findCell(node, pos, &L, &center);
    
    ins  = insertNode(&node->branches[c],
                      *nodeOffsetPtr,
                      L,
                      center,
                      pos,
                      mass,
                      lambda);
    
    if (ins)
    {
        octNodeSetIsLeaf(node, false);
        
        setBranchCell(node, c);
    }
    
    return ins;
}



MUCell Octree::findCell(const OctNode node,
                        const MUReal3 pos,
                        MUReal *const LPtr,
                        MUReal3 *const centerPtr)
{
    cl_uint         i;
    MUCell          c = OCT_NUM_BRANCHES;
    MUReal3         diff;
    cl_int3         sgnCell;
    cl_int3         sgnBody;
    
    diff = muDiff(pos, node->center);
    
    sgnBody = muSign(diff);
    
    *LPtr = MUReal(0.5) * node->L;
    
    for (i = 0; i < OCT_NUM_BRANCHES; i++)
    {
        sgnCell = octCellSign(i);
        
        if (muEqualSign(sgnBody, sgnCell) == true)
        {
            c = i;
            
            break;
        }
    }
    
    *centerPtr = muSum( node->center, muScaleInt(sgnCell, *LPtr) );
    
    return c;
}



void Octree::nodeCenterOfMass(OctNode node)
{
    MUCell i;
    
    if (nodeIsLeaf(node)) /* Se il node è una foglia  *
                           * non bisogna fare niente! */
    {
        return;
    }
    
    
    node->cm = muAssign(0.0);
    
    node->M  = 0.0;
    
    for (i = 0; i < OCT_NUM_BRANCHES; i++)
    {
        OctNode branch = NULL;
        
        if (nodeIsEmpty(node->branches[i])) /* Se la cella    *
                                             * non è occupata *
                                             * saltala...     */
        {
            continue;
        }
        
        branch   = nodeForOffset(node->branches[i]);
        
        node->cm = muSum( node->cm, muScale(branch->cm, branch->M) );
        
        node->M += branch->M;
    }
    
    node->cm = muInvScale(node->cm, node->M);
    
    return;
}



void
Octree::computeAccel()
{
    size_t numG    = 8;
    size_t numBodG = _numBodies / numG;
    
    dispatch_apply(numG,
                   queue_,
                   ^(size_t g)
                   {
                       size_t ini = g * numBodG;
                       size_t fin = (g == (numG - 1) ? _numBodies : (g + 1) * numBodG);
                       
                       for (size_t i = ini; i < fin; ++i)
                       {
                           octComputeAccelForBody(&_accel[i],
                                                  _pos[i],
                                                  _root,
                                                  _buffer);
                       }
                   });
    
    return;
}



void
Octree::operator()()
{
    computeAccel();
}



void
Octree::operator()(const MUReal3* pos,
                   MUReal3* accel)
{
    update(pos, NULL, accel);
    computeAccel();
}



OctHeader
Octree::getHeader()
{
    return OctHeader(_buffer);
}



OctNode
Octree::getRoot()
{
    return OctNode(_buffer + offsetForNodeIndex(0));
}



void
Octree::update()
{
    reset();
    build();
    setup();
    
    return;
}



void
Octree::reset()
{
    OctreeMemory::reset();
    
    computeExtension();
    
    setup();
    
    _header->n_bodies = MUSize(0);
    _header->cm       = muAssign(0.0f);
    _header->M        = 0.0f;
    _header->root     = MUSize(0);
    
    _root             = NULL;
    
    return;
}



void
Octree::setup()
{
    _header = getHeader();
    _root   = getRoot();
    
    return;
}



void
Octree::computeExtension()
{
    if (_pos == NULL)
    {
        return;
    }
    
    octOctreeExtension(_pos,
                       _numBodies,
                       _header->center,
                       &_header->L);
    
    return;
}



OctNode
Octree::allocNewNode(MURef *const nodeOffsetPtr)
{
    OctNode newNode = OctreeMemory::allocNewNode(nodeOffsetPtr);
    
    setup();
    
    return newNode;
}



void
Octree::build()
{
    MUSize i;
    
    for (i = 0; i < _numBodies; i++)
    {
        if ( !insertBody(_pos[i], _masses[i]) )
        {
            throw std::runtime_error("Cannot build octree!");
        }
    }
    
    return;
}



MUBool
Octree::nodeIsEmpty(const MURef nodeOffset)
{
    return MUBool(nodeOffset == 0);
}



MUBool
Octree::nodeIsLeaf(const MURef nodeOffset)
{
    return MUBool(nodeForOffset(nodeOffset)->isLeaf == true);
}



MUBool
Octree::nodeIsLeaf(const OctNode node)
{
    return MUBool(node->isLeaf == true);
}



void
Octree::setBranchCell(OctNode node,
                      const MUCell cell)
{
    octSetNodeCell( branchForCell(node, cell), cell );
    
    return;
}
