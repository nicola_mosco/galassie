//
//  GALBoundingBox.m
//  Galassie
//
//  Created by Nicola Mosco on 04/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import "GALBoundingBox.h"

@implementation GALBoundingBox

- (void)draw
{
    [super draw];
    
    
    glBegin( GL_LINE_LOOP );
    {
        glVertex3f( -1.0, -1.0, -1.0 );
        glVertex3f(  1.0, -1.0, -1.0 );
        glVertex3f(  1.0,  1.0, -1.0 );
        glVertex3f( -1.0,  1.0, -1.0 );
        glVertex3f( -1.0, -1.0, -1.0 );
        
        glVertex3f( -1.0, -1.0,  1.0 );
        glVertex3f(  1.0, -1.0,  1.0 );
        glVertex3f(  1.0,  1.0,  1.0 );
        glVertex3f( -1.0,  1.0,  1.0 );
        glVertex3f( -1.0, -1.0,  1.0 );
    }
    glEnd();
    
    glBegin( GL_LINES );
    {
        glVertex3f( 1.0, -1.0, -1.0 );
        glVertex3f( 1.0, -1.0,  1.0 );
        
        glVertex3f( 1.0,  1.0, -1.0 );
        glVertex3f( 1.0,  1.0,  1.0 );
        
        glVertex3f( -1.0,  1.0, -1.0 );
        glVertex3f( -1.0,  1.0,  1.0 );
    }
    glEnd();
}

@end
