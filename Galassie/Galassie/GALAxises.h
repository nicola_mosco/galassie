//
//  GALAxises.h
//  Galassie
//
//  Created by Nicola Mosco on 04/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GALFrame.h"


@interface GALAxises : GALFrame
{
    GLfloat             height;
    GLfloat             base;
    GLint               slices;
    GLint               stacks;
    
    GLUquadricObj*      arrow;
}

@property (assign) GLfloat height;
@property (assign) GLfloat base;
@property (assign) GLint   slices;
@property (assign) GLint   stacks;

@end
