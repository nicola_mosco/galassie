//
//  GALUniverse.h
//  Galassie
//
//  Created by Nicola Mosco on 17/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import "GALGeometry.h"


namespace phy
{
    class Universe;
}


@interface GALUniverse : GALGeometry

- (phy::Universe::PtrType)generate;

@end
