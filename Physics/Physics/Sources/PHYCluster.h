//
//  PHYCluster.h
//  Physics
//
//  Created by Nicola Mosco on 06/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYCluster__
#define __Physics__PHYCluster__

#include "PHYGeometry.h"
#include "PHYBody.h"


namespace phy
{
    class Cluster : public Geometry
    {
    public:
        typedef std::unique_ptr<Cluster> PtrType;
        typedef std::list<PtrType> ListType;
        
    public:
        Cluster(const ReferenceFrame& aRefFrame = ReferenceFrame{},
                const std::string& name = "Cluster");
        virtual ~Cluster() = default;
        
    public:
        virtual void build(Body::ListType& bodies) = 0;
    };
}

#endif /* defined(__Physics__PHYCluster__) */
