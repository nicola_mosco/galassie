//
//  PHYAlgebra.h
//  Physics
//
//  Created by Nicola Mosco on 09/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYAlgebra__
#define __Physics__PHYAlgebra__

#include <Eigen/Geometry>

#include "PHYTypes.h"

namespace phy
{
    typedef Eigen::Matrix<Real, 3, 1> Vector3;
    typedef Eigen::Matrix<Real, 3, 3> Matrix3;
    typedef Eigen::Transform<Real, 3, Eigen::Affine> Affine3;
    typedef Eigen::AngleAxis<Real> Rotation3;
    typedef Eigen::Translation<Real, 3> Translation3;
}

#endif /* defined(__Physics__PHYAlgebra__) */
