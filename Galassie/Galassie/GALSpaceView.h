//
//  GALSpaceView.h
//  Galassie
//
//  Created by Nicola Mosco on 03/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import <AppKit/AppKit.h>
#import <OpenGL/OpenGL.h>
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>
#import <OpenCL/OpenCL.h>
#import "GALBodyRendering.h"


@class GALAxises, GALBoundingBox, GALViewState, GALSimulationParameters;


@interface GALSpaceView : NSOpenGLView
{
    GLdouble                visAngle; // Visual/Perspective angle.
    GLdouble                visCooL;
    GLdouble                visCooR;
    GLdouble                visCooB;
    GLdouble                visCooT;
    GLdouble                visWidth;
    GLdouble                visHeight;
    GLdouble                visFrameW;
    GLdouble                visFrameH;
    GLfloat                 vPx, vPy, vPz;
    GLfloat                 scale;
    GLfloat                 zoom;
    GLfloat                 bodyScale;
    GLfloat                 angleX;
    GLfloat                 angleY;
    GLfloat                 deltaX;
    GLfloat                 deltaY;
    GLfloat                 rotationSens;
    GLfloat                 translationSens;
    GLfloat                 scaleSens;
    GLfloat                 scaleMin;
    GLfloat                 scaleMax;
    GLfloat                 zoomSens;
    GLfloat                 zoomMin;
    GLfloat                 zoomMax;
    GLfloat                 bodyScaleMin;
    GLfloat                 bodyScaleMax;
    GLfloat                 extension;
    
    NSPoint                 zoomPoint;
    NSPoint                 mouse;
    CGFloat                 mouseX;
    CGFloat                 mouseY;
    
    BOOL                    mouseEvent;
    
    BOOL                    showFrame;
    BOOL                    showBoundingBox;
    
    NSColor*                lightAmbient;
    NSColor*                lightDiffuse;
    NSColor*                lightSpecular;
    
    NSData*                 lightPosition;
    GLfloat                 constAttenuation;
    GLfloat                 linearAttenuation;
    GLfloat                 quadraticAttenuation;
    
    GALViewState*           viewState;
    
    GALAxises*              axises;
    GALBoundingBox*         boundingBox;
    
    gal::BodyRendering*     body;
    
    NSOpenGLContext*        openGLContext;
}

@property (assign)      GLdouble        visAngle;
@property (assign)      GLdouble        visCooL;
@property (assign)      GLdouble        visCooR;
@property (assign)      GLdouble        visCooB;
@property (assign)      GLdouble        visCooT;
@property (assign)      GLdouble        visWidth;
@property (assign)      GLdouble        visHeight;
@property (assign)      GLdouble        visFrameW;
@property (assign)      GLdouble        visFrameH;
@property (assign)      GLfloat         vPx;
@property (assign)      GLfloat         vPy;
@property (assign)      GLfloat         vPz;
@property (assign)      GLfloat         scale;
@property (assign)      GLfloat         zoom;
@property (assign)      GLfloat         bodyScale;
@property (assign)      GLfloat         constAttenuation;
@property (assign)      GLfloat         linearAttenuation;
@property (assign)      GLfloat         quadraticAttenuation;
@property (assign)      GLfloat         rotationSens;
@property (assign)      GLfloat         translationSens;
@property (assign)      GLfloat         scaleSens;
@property (assign)      GLfloat         zoomSens;
@property (assign)      GLfloat         scaleMin;
@property (assign)      GLfloat         scaleMax;
@property (assign)      GLfloat         zoomMin;
@property (assign)      GLfloat         zoomMax;
@property (assign)      GLfloat         bodyScaleMin;
@property (assign)      GLfloat         bodyScaleMax;
@property (assign)      GLfloat         extension;
@property (assign)      CGFloat         mouseX;
@property (assign)      CGFloat         mouseY;
@property (assign)      BOOL            showFrame;
@property (assign)      BOOL            showBoundingBox;
@property (strong)      NSColor*        lightAmbient;
@property (strong)      NSColor*        lightDiffuse;
@property (strong)      NSColor*        lightSpecular;
@property (strong)      NSData*         lightPosition;
@property (strong)      NSData*         bodiesPosition;
@property (strong)      GALViewState*   viewState;


- (void)centerFrameOrigin;

- (void) zoom: (GLfloat)zf
          min: (GLfloat)minZf
          max: (GLfloat)maxZf
     property: (GLfloat*)sc;

- (MUPoint3)convertFromWindowCoord:(MUPoint3)winPoint;

- (void)updateSceneWithParameters:(GALSimulationParameters*)someParams
                    positionsData:(NSData*)posData
                   velocitiesData:(NSData*)velData;

@end
