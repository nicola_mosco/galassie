//
//  GALFrame.h
//  Galassie
//
//  Created by Nicola Mosco on 04/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGL/OpenGL.h>
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>


@interface GALFrame : NSObject
{
    NSColor*        color;
}

@property (strong) NSColor* color;


- (void)draw;


@end
