//
//  NSNumber+GALNumberExtension.h
//  Galassie
//
//  Created by Nicola Mosco on 17/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Physics/PHYTypes.h>


@interface NSNumber (GALNumberExtension)

@property (readonly) phy::Real value;
@property (readonly) phy::Size sizeValue;

@end
