//
//  MUOctreeUtils.h
//  MatUtils
//
//  Created by Nicola Mosco on 07/08/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#ifndef _MU_OCTREE_UTILS_H_
#define _MU_OCTREE_UTILS_H_

#include "MUOctreeDefs.h"


__BEGIN_DECLS

/******************* General octree management functions... ******************/

__INLINE__ void octInsertLeaf( OctNode node,
                               Reale3 pos,
                               Reale mass );

Reale3 octPairsCenterOfMass( const cl_uint n, ... ); /* n indica il      *
                                                         * numero di coppie *
                                                         * ( pos, massa )   *
                                                         * per cui si vuole *
                                                         * calcolare il     *
                                                         * centro di massa. */

void octOctreeExtension( const Reale3* pos, const cl_ulong n_bodies,
                         const Reale3 center,
                         Reale *const LPtr );

void octComputeAccelForBody( Reale3 *const accPtr,
                             const Reale3 pos,
                             const OctNode node,
                             const OctBuffer octree );

#ifdef DEBUG
__INLINE__ Reale3 octComputeAccelFor2Bodies( const Reale3 pos1,
                                                const Reale3 pos2,
                                                const Reale M2,
                                                const Reale G,
                                                const Reale eps );
#else
inline Reale3 octComputeAccelFor2Bodies( const Reale3 pos1,
                                            const Reale3 pos2,
                                            const Reale M2,
                                            const Reale G,
                                            const Reale eps )
{
    Reale        a    = 0.0;
    Reale        d    = 0.0;
    Reale3       dpos = { 0.0 };
    
    dpos.x  = pos2.x - pos1.x;
    dpos.y  = pos2.y - pos1.y;
    dpos.z  = pos2.z - pos1.z;
    
    d       = sqrtf( dpos.x * dpos.x +
                     dpos.y * dpos.y +
                     dpos.z * dpos.z +
                     eps );
    
    a       = G * M2 / ( d * d * d );
    
    dpos.x *= a;
    dpos.y *= a;
    dpos.z *= a;
    
    return dpos;
}
#endif

cl_int3 octCellSign( cl_uint c );


#ifdef DEBUG
__INLINE__ cl_bool octNodeIsLeaf( const OctNode node );
__INLINE__ void octNodeSetIsLeaf( OctNode node,
                                  const cl_bool isLeaf );
__INLINE__ cl_bool octNodeIsEmpty( const OctRef nodeOffset );
__INLINE__ cl_bool octBranchIsEmpty( const OctNode node,
                                     const OctCell cell );
__INLINE__ cl_bool octNodeBHTest( const OctNode node,
                                  const Reale d,
                                  const Reale theta );

__INLINE__ OctHeader octGetHeader( OctBuffer octree );
__INLINE__ OctNode octGetNode( OctBuffer octree,
                               const OctRef nodeOffset );
__INLINE__ OctNode octGetBranch( const OctNode node,
                                 const OctCell c,
                                 OctBuffer octree );
__INLINE__ OctNode octGetParent( const OctNode node,
                                 OctBuffer octree );
__INLINE__ OctNode octGetRoot( OctBuffer octree );

__INLINE__ OctCell octNodeCell( const OctNode node );
__INLINE__ void octSetNodeCell( OctNode node,
                                const OctCell cell );
__INLINE__ void octSetBranchCell( OctNode node,
                                  const OctCell cell,
                                  OctBuffer octree );
#else
inline cl_bool octNodeIsLeaf( const OctNode node )
{
    return node->isLeaf;
}



inline void octNodeSetIsLeaf( OctNode node,
                              const cl_bool isLeaf )
{
    node->isLeaf = isLeaf;
    
    return;
}



inline cl_bool octNodeIsEmpty( const OctRef nodeOffset )
{
    return ( nodeOffset == OCTREF(0) );
}



inline cl_bool octBranchIsEmpty( const OctNode node,
                                 const OctCell cell )
{
    if ( cell >= OCT_NUM_BRANCHES )
    {
        return CL_FALSE;
    }
    
    return ( node->branches[ cell ] == OCTREF(0) );
}



inline cl_bool octNodeBHTest( const OctNode node,
                             const Reale d,
                             const Reale theta )
{
    return ( node->L / d < theta );
}



inline OctHeader octGetHeader( OctBuffer octree )
{
    return (OctHeader)octree;
}



inline OctNode octGetNode( OctBuffer octree,
                          const OctRef nodeOffset )
{
    if ( nodeOffset == 0 )
    {
        return NULL;
    }
    
    return (OctNode) ( octree + nodeOffset );
}



inline OctNode octGetBranch( const OctNode node,
                             const OctCell c,
                             OctBuffer octree )
{
    OctRef      branchOffset = node->branches[ c ];
    
    if ( branchOffset == 0 )
    {
        return NULL;
    }
    
    return (OctNode) ( octree + branchOffset );
}



inline OctNode octGetParent( const OctNode node,
                             OctBuffer octree )
{
    if ( node->parent == 0 )
    {
        return NULL;
    }
    
    return (OctNode) ( octree + node->parent );
}



inline OctNode octGetRoot( OctBuffer octree )
{
    return octGetNode( octree, ((OctHeader)octree)->root );
}



inline OctCell octNodeCell( const OctNode node )
{
    return node->c;
}



inline void octSetNodeCell( OctNode node,
                            const OctCell cell )
{
    node->c = cell;
    
    return;
}



inline void octSetBranchCell( OctNode node,
                              const OctCell cell,
                              OctBuffer octree )
{
    octSetNodeCell( octGetBranch( node, cell, octree ), cell );
    
    return;
}
#endif

/*****************************************************************************/

__END_DECLS

#endif //_OCTREE_DATA_H_
