//
//  MUOctreeDefs.h
//  MatUtils
//
//  Created by Nicola Mosco on 03/09/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#import <stdio.h>
#import <stdlib.h>
#import <stdarg.h>
#import <math.h>

#ifdef __APPLE__
#   include <OpenCL/OpenCL.h>
#else
#   include <CL/cl.h>
#endif

#import "MUTypes.h"



#ifndef MU_SIZE_PTR
#   define MU_SIZE_PTR  sizeof( MUPtr )
#endif // MU_SIZE_PTR



#define OCT_SIZE_PTR    MU_SIZE_PTR
#define OCT_SIZE_BYTE   sizeof( MUByte )
#define OCT_SIZE_BUFFER sizeof( MUBuffer )
#define OCT_SIZE_HEADER sizeof( struct _oct_header )
#define OCT_SIZE_NODE   sizeof( struct _oct_node )


#define OCT_NUM_CELLS       8
#define OCT_NUM_BRANCHES    OCT_NUM_CELLS


#define OCTSIZE( n ) ((MUSize)(n))
#define OCTREF( i )  ((MURef)(i))



__BEGIN_DECLS

#pragma pack(push)
#pragma pack(16)
struct _oct_node
{
    MUCell                 c;          // Cell number relative to parent.
    MUBool                 isLeaf;
    MUReal                 M;
    MUReal3                cm;
    MUReal                 L;
    MUReal3                center;
    MURef                  branches[ OCT_NUM_BRANCHES ];
    MURef                  parent;
};
#pragma pack(pop)

typedef struct _oct_node* OctNode;
typedef OctNode* OctPNode;
typedef OctPNode OctBranch;


#pragma pack(push)
#pragma pack(16)
struct _oct_header
{
    MUReal                 L;
    MUReal3                center;
    MUSize                 n_bodies;
    MUReal3                cm;
    MUReal                 M;
    MUReal                 theta;
    MUReal                 eps;
    MUReal                 G;
    MURef                  root;
};
#pragma pack(pop)

typedef struct _oct_header* OctHeader;

__END_DECLS
