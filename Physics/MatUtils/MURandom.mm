//
//  MURandom.mm
//  MatUtils
//
//  Created by Nicola Mosco on 29/07/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import "MURandom.h"
#import <time.h>


using namespace mu;



Random::Random( const gsl_rng_type* type )
{    
    type_ = type;
    
    generator_ = gsl_rng_alloc( type_ );
    
    gsl_rng_set( generator_, arc4random() );
}



Random::~Random( void )
{
    gsl_rng_free( generator_ );
}
