//
//  MUNGravitation.mm
//  MatUtil
//
//  Created by Nicola Mosco on 12/05/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#import "MUNGravitation.h"
#import "MUConstants.h"
#import "MUGeneralia.h"
#import <dispatch/dispatch.h>


#define MU_OCL_KERNEL_SOURCE "#define USE_OCTREE \n \
\n \
\n \
#ifndef NULL \n \
#   define NULL ((MUPtr)0) \n \
#endif // NULL \n \
\n \
\n \
#ifndef MU_SIZE_PTR \n \
#   define MU_SIZE_PTR  sizeof( MUPtr ) \n \
#endif // MU_SIZE_PTR \n \
\n \
\n \
\n \
#define CL_SUCCESS  0 \n \
\n \
#define CL_TRUE     1 \n \
#define CL_FALSE    0 \n \
\n \
\n \
\n \
#define OCT_SIZE_PTR        MU_SIZE_PTR \n \
#define OCT_SIZE_BYTE       sizeof( MUByte ) \n \
#define OCT_SIZE_BUFFER     sizeof( MUBuffer ) \n \
#define OCT_SIZE_HEADER     sizeof( struct _oct_header ) \n \
#define OCT_SIZE_NODE       sizeof( struct _oct_node ) \n \
\n \
\n \
#define OCT_NUM_CELLS       8 \n \
#define OCT_NUM_BRANCHES    OCT_NUM_CELLS \n \
\n \
\n \
#define OCTSIZE( n ) ((MUSize)(n)) \n \
#define OCTREF( i )  ((MURef)(i)) \n \
\n \
\n \
\n \
typedef uchar           cl_uchar; \n \
typedef uint            MUBool; \n \
typedef uint            cl_uint; \n \
typedef int             cl_int; \n \
typedef ulong           cl_ulong; \n \
typedef int3            cl_int3; \n \
typedef uint3           cl_uint3; \n \
typedef float           cl_float; \n \
typedef float3          cl_float3; \n \
typedef cl_uchar        MUByte; \n \
typedef cl_uint         MURef; \n \
typedef cl_int          MUCell; \n \
typedef cl_uint         MUSize; \n \
\n \
typedef __global \n \
MUByte     *   MUBuffer; \n \
typedef __global \n \
void        *   MUPtr; \n \
\n \
typedef cl_float        MUReal; \n \
typedef cl_float3       MUReal3; \n \
\n \
\n \
\n \
struct _oct_node \n \
{ \n \
MUCell                 c;          // Cell number relative to parent. \n \
MUBool                 isLeaf; \n \
MUReal                 M; \n \
MUReal3                cm; \n \
MUReal                 L; \n \
MUReal3                center; \n \
MURef                  branches[ OCT_NUM_BRANCHES ]; \n \
MURef                  parent; \n \
} __attribute__(( aligned(16) )); \n \
\n \
typedef __global struct \n \
_oct_node       *   OctNode; \n \
typedef OctNode         *   OctPNode; \n \
typedef OctPNode            OctBranch; \n \
\n \
\n \
struct _oct_header \n \
{ \n \
MUReal                   L; \n \
MUReal3                  center; \n \
MUSize                   n_bodies; \n \
MUReal3                  cm; \n \
MUReal                   M; \n \
MUReal                   theta; \n \
MUReal                   eps; \n \
MUReal                   G; \n \
MURef                  root; \n \
} __attribute__(( aligned(16) )); \n \
\n \
typedef __global struct \n \
_oct_header     *   OctHeader; \n \
\n \
\n \
\n \
MUReal muSign( const MUReal x ) \n \
{ \n \
    return ( (signbit( x ) == 0) ? +1 : -1 ); \n \
} \n \
\n \
void octComputeAccelForBody( MUReal3 *const accPtr, \n \
                             const MUReal3 pos, \n \
                             MUBuffer octree ); \n \
MUReal3 octComputeAccelFor2Bodies( const MUReal3 pos1, \n \
                                   const MUReal3 pos2, \n \
                                   const MUReal M2, \n \
                                   const MUReal G, \n \
                                   const MUReal eps ); \n \
\n \
MUBool octNodeIsLeaf( const OctNode node ); \n \
MUBool octNodeIsEmpty( const MURef nodeOffset ); \n \
MUBool octBranchIsEmpty( const OctNode node, \n \
                         const MUCell cell ); \n \
MUBool octNodeBHTest( const OctNode node, \n \
                      const MUReal d, \n \
                      const MUReal theta ); \n \
\n \
OctHeader octGetHeader( MUBuffer octree ); \n \
OctNode octGetNode( MUBuffer octree, \n \
                   const MURef nodeOffset ); \n \
OctNode octGetBranch( const OctNode node, \n \
                     const MUCell c, \n \
                     MUBuffer octree ); \n \
OctNode octGetParent( const OctNode node, \n \
                     MUBuffer octree ); \n \
OctNode octGetRoot( MUBuffer octree ); \n \
\n \
MUCell octNodeCell( const OctNode node ); \n \
\n \
MUBool octFindNotEmptyBranch( OctPNode curNodePtr, \n \
                              MUCell* cPtr, \n \
                              MUBuffer octree ); \n \
\n \
MUBool octVisitNode( OctPNode curNodePtr, \n \
                     MUReal3 *const accPtr, \n \
                     const MUReal3 pos, \n \
                     MUBuffer octree ); \n \
MUBool octDescendNode( OctPNode curNodePtr, \n \
                       MUCell* cPtr, \n \
                       MUBuffer octree ); \n \
void octAscendNode( OctPNode curNodePtr, \n \
                   MUCell* cPtr, \n \
                   MUBuffer octree ); \n \
\n \
\n \
\n \
__kernel void ngravkernel( MUSize numBodies, \n \
                           __global MUReal3* pos, \n \
                           __global MUByte* octree, \n \
                           __global MUReal3* accel ) \n \
{ \n \
    MUSize          i      = get_global_id( 0 ); \n \
    MUReal3         acc_v  = (MUReal3)( 0.0, 0.0, 0.0 ); \n \
    \n \
    if ( i >= numBodies ) \n \
    { \n \
        return; \n \
    } \n \
    \n \
    octComputeAccelForBody( &acc_v, pos[ i ], octree ); \n \
    \n \
    accel[ i ] = acc_v; \n \
    \n \
    return; \n \
} \n \
\n \
\n \
\n \
inline MUReal3 octComputeAccelFor2Bodies( const MUReal3 pos1, \n \
                                          const MUReal3 pos2, \n \
                                          const MUReal M2, \n \
                                          const MUReal G, \n \
                                          const MUReal eps ) \n \
{ \n \
    MUReal        a    = 0.0; \n \
    MUReal        d    = 0.0; \n \
    MUReal3       dpos = (MUReal3)( 0.0, 0.0, 0.0 ); \n \
    \n \
    dpos.x  = pos2.x - pos1.x; \n \
    dpos.y  = pos2.y - pos1.y; \n \
    dpos.z  = pos2.z - pos1.z; \n \
    \n \
    d       = sqrt( dpos.x * dpos.x + \n \
                    dpos.y * dpos.y + \n \
                    dpos.z * dpos.z + \n \
                    eps ); \n \
    \n \
    a       = G * M2 / ( d * d * d ); \n \
    \n \
    dpos.x *= a; \n \
    dpos.y *= a; \n \
    dpos.z *= a; \n \
    \n \
    return dpos; \n \
} \n \
\n \
\n \
\n \
void octComputeAccelForBody( MUReal3 *const accPtr, \n \
                             const MUReal3 pos, \n \
                             MUBuffer octree ) \n \
{ \n \
    MUBool         processed = CL_FALSE; \n \
    MUBool         descended = CL_FALSE; \n \
    MUBool         visited   = CL_FALSE; \n \
    MUCell         c         = 0; \n \
    OctNode         curNode   = octGetRoot( octree ); \n \
    \n \
    while ( curNode != NULL ) \n \
    { \n \
        if ( !visited ) \n \
        { \n \
            processed = octVisitNode( &curNode, \n \
                                     accPtr, \n \
                                     pos, \n \
                                     octree ); \n \
        } \n \
        \n \
        if ( !processed ) \n \
        { \n \
            descended = octDescendNode( &curNode, &c, octree ); \n \
            \n \
            if ( descended ) \n \
            { \n \
                visited = CL_FALSE; \n \
            } \n \
        } \n \
        \n \
        if ( !descended || processed ) \n \
        { \n \
            visited   = CL_TRUE; \n \
            processed = CL_FALSE; \n \
            \n \
            octAscendNode( &curNode, &c, octree ); \n \
        } \n \
    } \n \
    \n \
    return; \n \
} \n \
\n \
\n \
\n \
MUBool octVisitNode( OctPNode curNodePtr, \n \
                     MUReal3 *const accPtr, \n \
                     const MUReal3 pos, \n \
                     MUBuffer octree ) \n \
{ \n \
    MUBool         processed = CL_FALSE; \n \
    MUReal3          cm        = (*curNodePtr)->cm; \n \
    MUReal3          acc_v     = (MUReal3)( 0.0, 0.0, 0.0 ); \n \
    OctHeader       header; \n \
    \n \
    if ( cm.x == pos.x && cm.y == pos.y && cm.z == pos.z ) \n \
    { \n \
        return CL_FALSE; \n \
    } \n \
    \n \
    \n \
    header = octGetHeader( octree ); \n \
    \n \
    if ( octNodeIsLeaf( *curNodePtr ) ) \n \
    { \n \
        processed = CL_TRUE; \n \
    } \n \
    else \n \
    { \n \
        MUReal d; \n \
        \n \
        d = sqrt( ( pos.x - cm.x ) * ( pos.x - cm.x ) + \n \
                 ( pos.y - cm.y ) * ( pos.y - cm.y ) + \n \
                 ( pos.z - cm.z ) * ( pos.z - cm.z ) ); \n \
        \n \
        processed = octNodeBHTest( *curNodePtr, d, header->theta ); \n \
    } \n \
    \n \
    if ( processed == CL_TRUE ) \n \
    { \n \
        acc_v = octComputeAccelFor2Bodies( pos, \n \
                                          cm, \n \
                                          (*curNodePtr)->M, \n \
                                          header->G, \n \
                                          header->eps ); \n \
        \n \
        *accPtr += acc_v; // Guai ad accedere alle componenti di accPtr, va tutto a... \n \
    } \n \
    \n \
    return processed; \n \
} \n \
\n \
\n \
\n \
inline \n \
MUBool octDescendNode( OctPNode curNodePtr, \n \
                       MUCell* cPtr, \n \
                       MUBuffer octree ) \n \
{ \n \
    MUBool     descended = CL_FALSE; \n \
    OctNode     prevNode  = NULL; \n \
    \n \
    prevNode  = *curNodePtr; \n \
    \n \
    descended = octFindNotEmptyBranch( curNodePtr, cPtr, octree ); \n \
    \n \
    return descended; \n \
} \n \
\n \
\n \
\n \
inline \n \
void octAscendNode( OctPNode curNodePtr, \n \
                   MUCell* cPtr, \n \
                   MUBuffer octree ) \n \
{ \n \
    *cPtr = octNodeCell( *curNodePtr ) + 1; \n \
    \n \
    *curNodePtr = octGetParent( *curNodePtr, octree ); \n \
    \n \
    return; \n \
} \n \
\n \
\n \
\n \
inline \n \
MUBool octFindNotEmptyBranch( OctPNode curNodePtr, \n \
                              MUCell* cPtr, \n \
                              MUBuffer octree ) \n \
{ \n \
    MUBool     found = CL_FALSE; \n \
    \n \
    while ( *cPtr < OCT_NUM_BRANCHES && \n \
           octBranchIsEmpty( *curNodePtr, *cPtr ) ) \n \
    { \n \
        (*cPtr)++; \n \
    } \n \
    \n \
    if ( *cPtr < OCT_NUM_BRANCHES ) \n \
    { \n \
        *curNodePtr = octGetBranch( *curNodePtr, *cPtr, octree ); \n \
        \n \
        found       = CL_TRUE; \n \
        \n \
        *cPtr       = 0; \n \
    } \n \
    \n \
    return found; \n \
} \n \
\n \
\n \
\n \
inline MUBool octNodeIsLeaf( const OctNode node ) \n \
{ \n \
    return node->isLeaf; \n \
} \n \
\n \
\n \
\n \
inline MUBool octNodeIsEmpty( const MURef nodeOffset ) \n \
{ \n \
    return ( nodeOffset == OCTREF(0) ); \n \
} \n \
\n \
\n \
\n \
inline MUBool octBranchIsEmpty( const OctNode node, \n \
                                const MUCell cell ) \n \
{ \n \
    if ( cell >= OCT_NUM_BRANCHES ) \n \
    { \n \
        return CL_FALSE; \n \
    } \n \
    \n \
    return ( node->branches[ cell ] == OCTREF(0) ); \n \
} \n \
\n \
\n \
\n \
inline MUBool octNodeBHTest( const OctNode node, \n \
                             const MUReal d, \n \
                             const MUReal theta ) \n \
{ \n \
    return ( node->L / d < theta ); \n \
} \n \
\n \
\n \
\n \
inline OctHeader octGetHeader( MUBuffer octree ) \n \
{ \n \
    return (OctHeader)octree; \n \
} \n \
\n \
\n \
\n \
inline OctNode octGetNode( MUBuffer octree, \n \
                          const MURef nodeOffset ) \n \
{ \n \
    if ( nodeOffset == 0 ) \n \
    { \n \
        return NULL; \n \
    } \n \
    \n \
    return (OctNode) ( octree + nodeOffset ); \n \
} \n \
\n \
\n \
\n \
inline OctNode octGetBranch( const OctNode node, \n \
                            const MUCell c, \n \
                            MUBuffer octree ) \n \
{ \n \
    MURef      branchOffset = node->branches[ c ]; \n \
    \n \
    if ( branchOffset == 0 ) \n \
    { \n \
        return NULL; \n \
    } \n \
    \n \
    return (OctNode) ( octree + branchOffset ); \n \
} \n \
\n \
\n \
\n \
inline OctNode octGetParent( const OctNode node, \n \
                            MUBuffer octree ) \n \
{ \n \
    if ( node->parent == 0 ) \n \
    { \n \
        return NULL; \n \
    } \n \
    \n \
    return (OctNode) ( octree + node->parent ); \n \
} \n \
\n \
\n \
\n \
inline OctNode octGetRoot( MUBuffer octree ) \n \
{ \n \
    return octGetNode( octree, ((OctHeader)octree)->root ); \n \
} \n \
\n \
\n \
\n \
inline MUCell octNodeCell( const OctNode node ) \n \
{ \n \
    return node->c; \n \
}"



using namespace std;
using namespace mu;



static
const string oclKernelData = MU_OCL_KERNEL_SOURCE;



NGravitation::NGravitation( const Params& params ) : Interaction( "Newtonian Graviation", "ngravkernel", params )
{
    oclKernelSources_ = vector<string>( 1, MU_OCL_KERNEL_SOURCE );
}



MUReal NGravitation::energy(const MUSize numBodies,
                            const MUReal3 *pos,
                            const MUReal3 *vel,
                            const MUReal* masses)
{
    MUReal              energy    = 0.0;
    MUReal              G         = params_[ "G" ];
    dispatch_queue_t    queue     = dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0 );
    size_t              numG      = 32;
    size_t              numBodG   = numBodies / numG;
    MUReal*             kinETmp   = new MUReal[ numG ];
    MUReal*             potETmp   = new MUReal[ numG ];
    
    dispatch_apply( numG, queue,
                   ^( size_t g )
                   {
                       MUReal gamma;
                       size_t ini = g * numBodG;
                       size_t fin = ( g == ( numG - 1 ) ? ( numBodies - 1 ) : ( g + 1 ) * numBodG);
                       
                       kinETmp[ g ] = 0.0;
                       potETmp[ g ] = 0.0;
                       
                       for ( size_t i = ini; i < fin; ++i )
                       {
                           gamma = G * masses[ i ];
                           
                           kinETmp[ g ] += 0.5 * masses[ i ] * muScalProd( vel[ i ], vel[ i ] );
                           
                           for ( size_t j = i + 1; j < numBodies; ++j )
                           {
                               potETmp[ g ] -= gamma * masses[ j ] / muDist2( pos[ i ], pos[ j ] );
                           }
                       }
                   } );
    
    for ( size_t g = 0; g < numG; ++g )
    {
        energy += kinETmp[ g ] + potETmp[ g ];
    }
    
    delete kinETmp;
    delete potETmp;
    
    return energy;
}
