//
//  PHYGeometry.h
//  Physics
//
//  Created by Nicola Mosco on 06/01/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYGeometry__
#define __Physics__PHYGeometry__

#include <string>

#include "PHYReferenceFrame.h"


namespace phy
{
    class Geometry
    {
    public:
        Geometry(const ReferenceFrame& refFrame = ReferenceFrame{},
                 const std::string& name = "Geometry");
        virtual ~Geometry() = 0;
        
    public:
        const std::string& name() const;
        const ReferenceFrame& referenceFrame() const;
        
        
        
    private:
        std::string name_;
        ReferenceFrame refFrame_;
    };
}

#endif /* defined(__Physics__PHYGeometry__) */
