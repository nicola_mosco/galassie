//
//  PHYBodyCluster.h
//  Physics
//
//  Created by Nicola Mosco on 07/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYBodyCluster__
#define __Physics__PHYBodyCluster__

#include "PHYCluster.h"
#include "PHYBody.h"

namespace phy
{
    class BodyCluster : public Cluster
    {
    public:
        BodyCluster(const ReferenceFrame& aRefFrame = ReferenceFrame{},
                    const std::string& name = "BodyCluster");
        virtual ~BodyCluster() = 0;
        
    public:
        const std::string& bodyName() const;
        std::string& bodyName();
        
    public:
        virtual void build(Body::ListType& bodies) final;
        
        
        
    protected:
        virtual void buildStructure() = 0;
        
    protected:
        Body::ListType bodies_;
        
        
        
    private:
        std::string bodyName_;
    };
}

#endif /* defined(__Physics__PHYBodyCluster__) */
