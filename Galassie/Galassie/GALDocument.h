//
//  GALDocument.h
//  Galassie
//
//  Created by Nicola Mosco on 03/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <MatUtils/MUTypes.h>
#import <CorePlot/CorePlot.h>


@class GALSpaceView, GALSimulationParameters, MUSimulator;


@interface GALDocument : NSDocument <CPTPlotDataSource>
{
    CGFloat                     defInspWidth;
    CGFloat                     maxInspWidth;
    CGFloat                     minInspWidth;
    CGFloat                     lastWidth;
    
    NSInteger                   inspDividerIndex;
    
    MUSimulator*                simulator;
}

@property (weak)    IBOutlet GALSpaceView*              spaceView;
@property (weak)    IBOutlet CPTGraphHostingView*       hostingView;
@property (weak)    IBOutlet NSProgressIndicator*       indicator;
@property (weak)    IBOutlet NSTextField*               timeLeftField;
@property (strong)  IBOutlet GALSimulationParameters*   simParams;
@property (assign)  MUReal                              visTime;
@property (assign)  NSTimeInterval                      simTimeLeft;
@property (strong)  NSString*                           timeLeftRep;
@property (assign)  BOOL                                simRunning;


- (IBAction)centerFrameOrigin:(id)sender;
- (IBAction)buildSimulation:(id)sender;
- (IBAction)runSimulation:(id)sender;
- (IBAction)startVisualization:(id)sender;
- (IBAction)stopVisualization:(id)sender;

- (void)prepareVisualization;
- (void)updateScene:(NSTimer*)theTimer;
- (void)resetVisualization;

@end
