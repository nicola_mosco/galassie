//
//  PHYSingleTaskDevice.cpp
//  Physics
//
//  Created by Nicola Mosco on 23/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#include "PHYSingleTaskDevice.h"


using namespace phy;



SingleTaskDevice::SingleTaskDevice(const std::string& description)
    : Device{description}
{}



SingleTaskDevice::~SingleTaskDevice() = default;



void
SingleTaskDevice::performTask(dispatch_block_t beginAction,
                              dispatch_block_t endAction)
{
    if (!state())
    {
        writeMessage("device is " + stateName() + "...");
        
        return;
    }
    
    Device::performTask(beginAction,
                        ^{
                            endAction();
                            
                            setState(State::OFFLINE);
                            
                            writeMessage("device is " + stateName() + ".");
                        });
}
