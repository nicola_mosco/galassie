//
//  GALSystemBuilder.h
//  Galassie
//
//  Created by Nicola Mosco on 08/12/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MatUtils/MUTypes.h>
#import <MatUtils/MUSystem.h>


@class GALSimulationParameters;


@interface GALSystemBuilder : NSObject

- (id)initWithSimulationParameters:(GALSimulationParameters*)someParams;

- (void)generateSystemData;

- (mu::System*)buildSystem;

@end
