//
//  MUSimulator.h
//  MatUtils
//
//  Created by Nicola Mosco on 23/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MUSystem.h"
#import "MUIntegrator.h"
#import "MUInteraction.h"
#import "MURungeKutta4.h"


@interface MUSimulator : NSObject
{
    mu::System*         system_;
    mu::Interaction*    interaction_;
    
    mu::Integrator*     integrator_;
    
    std::string         dataFileName_;
    
    MUReal              sample_;
}

@property (readonly, assign) mu::System* system;
@property (readonly, assign) mu::Interaction* interaction;


- (id)initWithSystem:(mu::System*)aSystem
         interaction:(mu::Interaction*)anInteraction
          integrator:(mu::Integrator*)anIntegrator
      sampleInterval:(MUReal)sample;

- (void)setup:(cl_device_type)devType;

- (void)simulate:(id)sender;

@end
