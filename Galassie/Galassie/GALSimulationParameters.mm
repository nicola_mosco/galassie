//
//  GALSimulationParameters.mm
//  Galassie
//
//  Created by Nicola Mosco on 12/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import <MatUtils/MUConstants.h>
#import "GALSimulationParameters.h"
#import "GALParameter.h"



@implementation GALSimulationParameters
{
    NSNumberFormatter*  formatter;
}



- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.numGalaxies    = 1;
        self.numBodiesGal   = 100000;
        self.G              = CF_G_SOLM_LY_YEAR; // G expressed in solar-masses, light-years, years.
        self.coreMass       = 1.0e4; // SOL_M.
        self.meanMass       = 10.0; // SOL_M.
        self.massSpread     = 0.8 * self.meanMass; // SOL_M.
        self.bulge          = 5.0e1; // ly.
        self.extent         = 1.0e2; // ly.
        self.d              = 2.0 * 2.0 * self.extent; // ly.
        self.uniMinRadius   = self.d * 0.5; // ly.
        self.uniMaxRadius   = self.d; // ly.
        self.minSpeed       = umConvLY(3.0e5) / umConvYEAR(1); // ly/y.
        self.maxSpeed       = umConvLY(6.0e5) / umConvYEAR(1); // ly/y.
        self.lambda         = 2.0e1 * umConvLY(1.4e9); // ly.
        self.coreRadius     = 10.0; // ly.
        self.eps            = 0.5; // ly^2.
        self.theta          = 0.4;
        self.t_i            = 0.0; // y.
        self.t_f            = 1.0e6; // y.
        self.h              = 20; // y.
        self.sampleIntv     = 100; // y.
        self.usingGPU       = YES;
        
        self.parameters          = [[NSMutableArray alloc] initWithCapacity: 23];
        
        
        formatter                   = [[NSNumberFormatter alloc] init];
        formatter.formatterBehavior = NSNumberFormatterBehavior10_4;
        formatter.numberStyle       = NSNumberFormatterDecimalStyle;
        
        
        [self.parameters addObject: [GALParameter parameterWithName: @"Number of galaxies"
                                                              value: [NSNumber numberWithLong: self.numGalaxies]
                                                         identifier: @"numGalaxies"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Number of bodies per galaxy"
                                                              value: [NSNumber numberWithLong: self.numBodiesGal]
                                                         identifier: @"numBodiesGal"
                                                          formatter: formatter]];
        
        
        formatter                               = [[NSNumberFormatter alloc] init];
        formatter.formatterBehavior             = NSNumberFormatterBehavior10_4;
        formatter.numberStyle                   = NSNumberFormatterScientificStyle;
        formatter.alwaysShowsDecimalSeparator   = YES;
        formatter.minimumIntegerDigits          = 1;
        formatter.maximumIntegerDigits          = 1;
        formatter.minimumFractionDigits         = 6;
        formatter.maximumFractionDigits         = 6;
        
        
        [self.parameters addObject: [GALParameter parameterWithName: @"Gravitational constant"
                                                              value: [NSNumber numberWithDouble: self.G]
                                                         identifier: @"G"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Galaxy core mass"
                                                              value: [NSNumber numberWithDouble: self.coreMass]
                                                         identifier: @"coreMass"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Stars mean mass"
                                                              value: [NSNumber numberWithDouble: self.meanMass]
                                                         identifier: @"meanMass"
                                                          formatter: formatter]];
        
        [self.parameters addObject: [GALParameter parameterWithName: @"Stars mass spread"
                                                              value: [NSNumber numberWithDouble: self.massSpread]
                                                         identifier: @"massSpread"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Galaxy bulge radius"
                                                              value: [NSNumber numberWithDouble: self.bulge]
                                                         identifier: @"bulge"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Galaxy extent"
                                                              value: [NSNumber numberWithDouble: self.extent]
                                                         identifier: @"extent"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Galaxies minimum relative distance"
                                                              value: [NSNumber numberWithDouble: self.d]
                                                         identifier: @"d"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Universe shell internal radius"
                                                              value: [NSNumber numberWithDouble: self.uniMinRadius]
                                                         identifier: @"uniMinRadius"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Universe shell external radius"
                                                              value: [NSNumber numberWithDouble: self.uniMaxRadius]
                                                         identifier: @"uniMaxRadius"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Galaxies minimum speed"
                                                              value: [NSNumber numberWithDouble: self.minSpeed]
                                                         identifier: @"minSpeed"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Galaxies maximum speed"
                                                              value: [NSNumber numberWithDouble: self.maxSpeed]
                                                         identifier: @"maxSpeed"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Stars safety distance"
                                                              value: [NSNumber numberWithDouble: self.lambda]
                                                         identifier: @"lambda"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Galaxy core radius"
                                                              value: [NSNumber numberWithDouble: self.coreRadius]
                                                         identifier: @"coreRadius"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Softening factor squared"
                                                              value: [NSNumber numberWithDouble: self.eps]
                                                         identifier: @"eps"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Tolerance factor"
                                                              value: [NSNumber numberWithDouble: self.theta]
                                                         identifier: @"theta"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Initial time"
                                                              value: [NSNumber numberWithDouble: self.t_i]
                                                         identifier: @"t_i"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Final time"
                                                              value: [NSNumber numberWithDouble: self.t_f]
                                                         identifier: @"t_f"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Integration step"
                                                              value: [NSNumber numberWithDouble: self.h]
                                                         identifier: @"h"
                                                          formatter: formatter]];
        [self.parameters addObject: [GALParameter parameterWithName: @"Sample interval"
                                                              value: [NSNumber numberWithDouble: self.sampleIntv]
                                                         identifier: @"sampleIntv"
                                                          formatter: formatter]];
    }
    
    return self;
}



- (MUSize)numBodies
{
    return self.numGalaxies * self.numBodiesGal;
}



- (void)setNilValueForKey:(NSString *)key
{
    [self setValue: [NSNumber numberWithDouble: 0.0]
            forKey: key];
}

@end
