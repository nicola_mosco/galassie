//
//  GALShell.mm
//  Galassie
//
//  Created by Nicola Mosco on 18/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import <Physics/PHYShell.h>

#import "NSNumber+GALNumberExtension.h"
#import "GALShell.h"


@implementation GALShell

- (id)init
{
    self = [super init];
    
    if (self)
    {
        auto dict = @{@"Name":        @"Shell",
                      @"Body name":   @"Star",
                      @"# of bodies": @10000};
        
        self.parameters = [NSMutableDictionary dictionaryWithDictionary:dict];
        
        type = @"body_cluster";
    }
    
    return self;
}



- (phy::Shell::PtrType)generate
{
    auto shell = new phy::Shell{[self frame],
                                [self.parameters[@"Name"] UTF8String]};
    
    shell->bodyName()  = [self.parameters[@"Body name"] UTF8String];
    shell->numBodies() = [(NSNumber*)self.parameters[@"Number of bodies"] sizeValue];
    
    return phy::Shell::PtrType(shell);
}

@end
