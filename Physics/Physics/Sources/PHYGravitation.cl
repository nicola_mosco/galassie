//
//  PHYGravitation.cl
//  Physics
//
//  Created by Nicola Mosco on 12/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#ifndef __OPENCL__
#   define __OPENCL__
#endif // __OPENCL__

#include "PHYTypes.h"


RVec3 bodybody(const Real G,
               const Real eps,
               const RVec3 pos_a,
               const RVec3 pos_b)
{
    Real p_dist; // Strictly positive distance.
    RVec3 diff;
    
    diff = pos_b - pos_a;
    
    p_dist = dot(diff, diff) + eps * eps;
    
    return G * rsqrt(p_dist * p_dist * p_dist);
    //return G * powr(b_dist, (Real)-1.5);
}



void leapfrog(const Size i,
              const Size j,
              const Real G,
              const Real eps,
              const Real h,
              constant Real* masses,
              constant RVec3* pos,
              constant RVec3* vel,
              global RVec3* newPos,
              global RVec3* newVel,
              global RVec3* accel)
{
    RVec3 acc_pt; // Partial acceleration.
    
    if (j == 0)
    {
        newPos[i] = pos[i] + vel[i] * h; // x(i+1) = x(i) + v(i-1/2) * h
    }
    
    barrier(CLK_GLOBAL_MEM_FENCE);
    
    if (i != j)
    {
        acc_pt = bodybody(G, eps, newPos[i], newPos[j]); // }
                                                         // |
                                                         // } a(i+1) = F( x(i+1) )
        accel[i] += masses[j] * acc_pt;                  // |
        accel[j] -= masses[i] * acc_pt;                  // }
    }
    
    barrier(CLK_GLOBAL_MEM_FENCE);
    
    if (j == 0)
    {
        newVel[i] = vel[i] + accel[i] * h; // v(i+1/2) = v(i-1/2) + a(i) * h
    }
}



kernel void gravitation(const Size numBodies,
                        const Real G,
                        const Real eps,
                        const Real h,
                        constant Real* masses,
                        constant RVec3* pos,
                        constant RVec3* vel,
                        global RVec3* newPos,
                        global RVec3* newVel,
                        global RVec3* accel)
{
    const Size i = get_global_id(0);
    const Size j = get_global_id(1);
    
    if (i >= numBodies || j > i)
    {
        return;
    }
    
    leapfrog(i, j, G, eps, h, masses, pos, vel, newPos, newVel, accel);
}
