//
//  GALSimulationBuilder.mm
//  Galassie
//
//  Created by Nicola Mosco on 31/07/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import "GALSimulationBuilder.h"
#import "GALSystemBuilder.h"
#import "GALGalaxyBuilder.h"
#import "GALSimulationParameters.h"
#import <MatUtils/MUTypes.h>
#import <MatUtils/MUSystem.h>
#import <MatUtils/MUSimulator.h>
#import <MatUtils/MUNGravitation.h>
#import <iostream>
#import <iomanip>


using namespace std;
using namespace mu;



@implementation GALSimulationBuilder
{
    GALSimulationParameters*    simParams_;
    System*                     system_;
    NGravitation*               interaction_;
    
    GALSystemBuilder*           sysBuilder_;
}



- (id)initWithSimulationParameters:(GALSimulationParameters *)someParams
{
    self = [super init];
    
    if (self)
    {
        simParams_   = someParams;
        system_      = NULL;
        interaction_ = NULL;
        sysBuilder_  = [[GALSystemBuilder alloc] initWithSimulationParameters: someParams];
    }
    
    return self;
}



- (void)prepareSimulation:(id)object
{
    [sysBuilder_ generateSystemData];
    
    Interaction::Params params;
    
    params["G"]   = simParams_.G;
    params["eps"] = simParams_.eps;
    
    system_       = [sysBuilder_ buildSystem];
    interaction_  = new NGravitation(params);
    
    //cout << "\nEnergy: " << scientific << setprecision(7) << interaction_->energy(system_) << endl;
    
    return;
}



- (MUSimulator *)buildSimulator
{
    Integrator*     integrator  = new Integrator(system_,
                                                 interaction_,
                                                 simParams_.theta,
                                                 simParams_.t_i,
                                                 simParams_.t_f,
                                                 simParams_.h);
    MUSimulator*    simulator   = [[MUSimulator alloc] initWithSystem: system_
                                                          interaction: interaction_
                                                           integrator: integrator
                                                       sampleInterval: simParams_.sampleIntv];
    
    try
    {
        /*if (simParams_.usingGPU)
        {
            [simulator setup: CL_DEVICE_TYPE_GPU];
        }
        else
        {
            [simulator setup: CL_DEVICE_TYPE_CPU];
        }*/
    }
    catch (const cl::Error& error)
    {
        cout << error.what() << " (" << error.err() << ")" << endl;
        
        exit(EXIT_FAILURE);
    }
    
    return simulator;
}



- (void)setup:(cl_device_type)dev_t
{
    
}

@end
