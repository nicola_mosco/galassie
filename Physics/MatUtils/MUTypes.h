/*
 *  MUTypes.h
 *  MatUtils
 *
 *  Created by Nicola Mosco on 27/12/10.
 *  Copyright 2010 Nicola Mosco. All rights reserved.
 *
 */

#ifndef _MU_TYPES_H_
#define _MU_TYPES_H_

#include <stdbool.h>
#include <OpenCL/cl.h>

#define MU_FLOAT    32
#define MU_DOUBLE   64
#define MU_L_DOUBLE 80
#define MU_BYTE      8
#define MU_S_INT    16
#define MU_INT      32
#define MU_LL_INT   64


#define MU_REAL_D   MU_FLOAT    // 32, 64, 80 bits.
#define MU_INT_D    MU_INT      //  8, 16, 32, 64 bits.

#if   MU_REAL_D == MU_FLOAT
#   define MU_PF_F  "%.*f"  /* Formato per printf e scanf.           */
#   define MU_PF_FC "%*.*f"
#   define MU_PF_E  "%.*e"
#   define MU_PF_EC "%*.*e"
#   define MU_SC_F  "%f"
#   define MU_SC_E  "%e"
#   define MU_PF_PR 7      /* Precisione con cui stampare i numeri. */
#elif MU_REAL_D == MU_DOUBLE
#   define MU_PF_F  "%.*lf"
#   define MU_PF_FC "%*.*lf"
#   define MU_PF_E  "%.*le"
#   define MU_PF_EC "%*.*le"
#   define MU_SC_F  "%lf"
#   define MU_SC_E  "%le"
#   define MU_PF_PR 15
#elif MU_REAL_D == MU_L_DOUBLE
#   define MU_PF_F  "%.*Lf"
#   define MU_PF_FC "%*.*Lf"
#   define MU_PF_E  "%.*Le"
#   define MU_PF_EC "%*.*Le"
#   define MU_SC_F  "%Lf"
#   define MU_SC_E  "%Le"
#   define MU_PF_PR 19
#else
#   error "*** Data type not supported! ***"
#endif

#if   MU_INT_D == MU_BYTE || MU_INT_D == MU_INT
#   define MU_PF_I  "%d"
#   define MU_PF_U  "%u"
#   define MU_PF_IC "%*d"
#   define MU_PF_UC "%*u"
#   define MU_SC_I  MU_PF_I
#   define MU_SC_U  MU_PF_U
#elif MU_INT_D == MU_S_INT
#   define MU_PF_I  "%hd"
#   define MU_PF_U  "%hu"
#   define MU_PF_IC "%*hd"
#   define MU_PF_UC "%*hu"
#   define MU_SC_I  MU_PF_I
#   define MU_SC_U  MU_PF_U
#elif MU_INT_D == MU_LL_INT
#   define MU_PF_I  "%lld"
#   define MU_PF_U  "%llu"
#   define MU_PF_IC "%*lld"
#   define MU_PF_UC "%*llu"
#   define MU_SC_I  MU_PF_I
#   define MU_SC_U  MU_PF_U
#else
#   error "*** Data type not supported! ***"
#endif


#define MU_PREC  6
#define MU_FIELD ( MU_PREC + 7 )

#define MU_PREC1  3
#define MU_FIELD1 ( MU_PREC1 + 7 )


#define MU_REAL( x )    ((MUReal)(x))
#define MU_INTEGER( n ) ((MUInt)(n))
#define MU_INT_US( n )  ((MUUInt)(n))


#define MU_SIZE_PTR    sizeof ( void* )
#define MU_SIZE_REAL   sizeof ( MUReal )
#define MU_SIZE_REAL3  sizeof ( MUReal3 )
#define MU_SIZE_REAL4  sizeof ( MUReal4 )
#define MU_SIZE_POINT3 sizeof ( MUPoint3 )

#if   MU_REAL_D == MU_FLOAT
typedef cl_float    MUReal;
typedef cl_float3   MUReal3;
typedef cl_float4   MUReal4;
#elif MU_REAL_D == MU_DOUBLE
typedef cl_double   MUReal;
typedef cl_double3  MUReal3;
typedef cl_double4  MUReal4;
#elif MU_REAL_D == MU_L_DOUBLE
typedef long double MUReal;
#endif


#define MU_SIZE_INT  sizeof ( MUInt )
#define MU_SIZE_UINT sizeof ( MUUInt )

#if   MU_INT_D == MU_BYTE
typedef int8_t   MUInt;
typedef uint8_t  MUUInt;
#elif MU_INT_D == MU_S_INT
typedef int16_t  MUInt;
typedef uint16_t MUUInt;
#elif MU_INT_D == MU_INT
typedef int32_t  MUInt;
typedef uint32_t MUUInt;
#elif MU_INT_D == MU_LL_INT
typedef int64_t  MUInt;
typedef uint64_t MUUInt;
#endif


typedef cl_uchar        MUByte;
typedef cl_uint         MURef;
typedef cl_int          MUCell;
typedef cl_uint         MUSize;
typedef bool            MUBool;
typedef MUByte*         MUBuffer;
typedef void*           MUPtr;


struct _MUPoint3
{
    MUReal  x;
    MUReal  y;
    MUReal  z;
};

typedef struct _MUPoint3 MUPoint3;


struct _MURange
{
    MUSize  location;
    MUSize  length;
};

typedef struct _MURange MURange;

#endif //_MU_TYPES_H_
