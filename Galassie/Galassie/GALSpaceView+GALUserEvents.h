//
//  GALSpaceView+GALUserEvents.h
//  Galassie
//
//  Created by Nicola Mosco on 04/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import "GALSpaceView.h"

@interface GALSpaceView (GALUserEvents)

- (void)mouseDragged:(NSEvent *)theEvent;
- (void)scrollWheel:(NSEvent *)theEvent;

@end
