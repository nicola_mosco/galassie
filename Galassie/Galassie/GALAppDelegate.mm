//
//  GALAppDelegate.mm
//  Galassie
//
//  Created by Nicola Mosco on 18/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import "GALAppDelegate.h"
#import "GALDocument.h"
#import "GALParameter.h"
#import "GALSimulationParameters.h"
#import "GALWelcomeController.h"


@implementation GALAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)notification
{
    self.welcomeController = [[NSWindowController alloc] initWithWindowNibName:@"WelcomeWindow"];
    
    [self.welcomeController showWindow: self];
}



- (BOOL)applicationShouldOpenUntitledFile:(NSApplication*)sender
{
    return NO;
}



+ (BOOL)canConcurrentlyReadDocumentsOfType:(NSString *)typeName
{
    return YES;
}



- (BOOL)applicationShouldHandleReopen:(NSApplication *)sender
                    hasVisibleWindows:(BOOL)flag
{
    if (!flag)
    {
        [self.welcomeController showWindow: self];
    }
    
    return NO;
}



- (IBAction)toggleWelcomeWindow:(id)sender
{
    [self.welcomeController showWindow: self];
}



- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    NSInteger count = 0;
    
    if (self.simParams.parameters)
    {
        count = self.simParams.parameters.count;
    }
    
    return count;
}



-           (id)tableView:(NSTableView *)tableView
objectValueForTableColumn:(NSTableColumn *)tableColumn
                      row:(NSInteger)row
{
    id value = nil;
    
    GALParameter* param = [self.simParams.parameters objectAtIndex: row];
    
    if ([tableColumn.identifier isEqualToString: @"Parameter"])
    {
        value = param.name;
    }
    else if ([tableColumn.identifier isEqualToString: @"Value"])
    {
        value = param;
    }
    
    return value;
}



- (void)tableView:(NSTableView *)tableView
   setObjectValue:(id)object
   forTableColumn:(NSTableColumn *)tableColumn
              row:(NSInteger)row
{
    if ([tableColumn.identifier isEqualToString: @"Value"])
    {
        GALParameter* param = [self.simParams.parameters objectAtIndex: row];
        
        param.value = [param.formatter numberFromString: object];
        
        [self.simParams setValue: param.value
                          forKey: param.identifier];
    }
}

@end
