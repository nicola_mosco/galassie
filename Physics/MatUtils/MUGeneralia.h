//
//  MUGeneralia.h
//  MatUtils
//
//  Created by Nicola Mosco on 17/05/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#import <vector>
#import <stdarg.h>
#import "MUTypes.h"
#import "MUConstants.h"
#import "MUMathFunc.h"


#ifndef __INLINE__
#   define __INLINE__ extern inline
#endif // __INLINE__


namespace mu
{
    typedef std::vector<MUReal> RealVec;
    typedef std::vector<MUReal3> Real3Vec;
}



/***************************** General Utilities *****************************/

inline MUReal muDegToRad( const MUReal alpha )
{
    return MU_REAL(alpha) * MU_REAL(M_PI) / MU_REAL(180);
}



inline MUReal muRadToDeg( const MUReal rho )
{
    return MU_REAL(rho) * MU_REAL(180) / MU_REAL(M_PI);
}



inline MUReal muSignValue( const MUReal x )
{
    return ( ( x >= MU_REAL(0) ) ? +1 : -1 );
}



inline MUReal3 muReal3( const MUReal x, const MUReal y, const MUReal z )
{
    MUReal3 real3;
    
    real3.x = x;
    real3.y = y;
    real3.z = z;
    
    return real3;
}



inline MUPoint3 muPoint3( const MUReal x, const MUReal y, const MUReal z )
{
    MUPoint3 point;
    
    point.x = x;
    point.y = y;
    point.z = z;
    
    return point;
}



inline MUBool muEqual( const MUReal3 v1, const MUReal3 v2 )
{
    MUBool     ug;
    
    ug = ( v1.x == v2.x ) && ( v1.y == v2.y ) && ( v1.z == v2.z );
    
    return ug;
}



inline MUBool muEqualSign( const cl_int3 s1, const cl_int3 s2 )
{
    MUBool     ug;
    
    ug = ( s1.x == s2.x ) && ( s1.y == s2.y ) &&
    ( s1.z == s2.z );
    
    return ug;
}



inline cl_int3 muSign( const MUReal3 v )
{
    cl_int3       s;
    
    s.x = (cl_int)muSignValue( v.x );
    s.y = (cl_int)muSignValue( v.y );
    s.z = (cl_int)muSignValue( v.z );
    
    return s;
}



inline MUReal3 muAssign( const MUReal lambda )
{
    MUReal3       w;
    
    w.x = lambda;
    w.y = lambda;
    w.z = lambda;
    
    return w;
}



inline MUReal3 muSum( const MUReal3 v1, const MUReal3 v2 )
{
    MUReal3       w;
    
    w.x = v1.x + v2.x;
    w.y = v1.y + v2.y;
    w.z = v1.z + v2.z;
    
    return w;
}



inline MUReal3 muSum( const MUSize n, ... )
{
    MUReal3 s = { 0.0 };
    MUReal3 v;
    va_list vl;
    
    va_start( vl, n );
    
    for ( MUSize i = 0; i < n; ++i )
    {
        v = va_arg( vl, MUReal3 );
        
        s = muSum( s, v );
    }
    
    va_end( vl );
    
    return s;
}



inline MUReal3 muDiff( const MUReal3 v1, const MUReal3 v2 )
{
    MUReal3       w;
    
    w.x = v1.x - v2.x;
    w.y = v1.y - v2.y;
    w.z = v1.z - v2.z;
    
    return w;
}



inline MUReal3 muScale( const MUReal3 v, const MUReal alpha )
{
    MUReal3       w;
    
    w.x = v.x * alpha;
    w.y = v.y * alpha;
    w.z = v.z * alpha;
    
    return w;
}



inline MUReal3 muScaleInt( const cl_int3 v, const MUReal alpha )
{
    MUReal3       w;
    
    w.x = v.x * alpha;
    w.y = v.y * alpha;
    w.z = v.z * alpha;
    
    return w;
}



inline MUReal3 muInvScale( const MUReal3 v, const MUReal alpha )
{
    MUReal3       w;
    
    w.x = v.x / alpha;
    w.y = v.y / alpha;
    w.z = v.z / alpha;
    
    return w;
}



inline MUReal muScalProd( const MUReal3 v1, const MUReal3 v2 )
{
    MUReal       w;
    
    w = v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
    
    return w;
}



inline MUReal3 muVectProd( const MUReal3 v1, const MUReal3 v2 )
{
    MUReal3       w;
    
    w.x = v1.y * v2.z - v1.z * v2.y;
    w.y = v1.z * v2.x - v1.x * v2.z;
    w.z = v1.x * v2.y - v1.y * v2.x;
    
    return w;
}



inline MUReal muNorm2( const MUReal3 v )
{
    return Sqrt( muScalProd( v, v ) );
}



inline MUReal muDist2( const MUReal3 v, const MUReal3 w )
{
    return muNorm2( muDiff( v, w ) );
}

/*****************************************************************************/
