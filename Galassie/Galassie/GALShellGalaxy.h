//
//  GALShellGalaxy.h
//  Galassie
//
//  Created by Nicola Mosco on 18/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import "GALGalaxy.h"


namespace phy
{
    class ShellGalaxy;
}


@interface GALShellGalaxy : GALGalaxy

- (phy::ShellGalaxy::PtrType)generate;

@end
