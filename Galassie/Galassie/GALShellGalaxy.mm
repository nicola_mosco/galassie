//
//  GALShellGalaxy.mm
//  Galassie
//
//  Created by Nicola Mosco on 18/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import <Physics/PHYShellGalaxy.h>

#import "NSNumber+GALNumberExtension.h"
#import "GALShellGalaxy.h"
#import "GALCore.h"
#import "GALShell.h"


@implementation GALShellGalaxy

- (id)init
{
    self = [super init];
    
    if (self)
    {
        auto bulge = [[GALShell alloc] init];
        
        bulge.parameters[@"Name"] = @"Bulge";
        
        self.parameters[@"Name"] = @"Shell Galaxy";
        
        [self addEntity:[[GALCore alloc] init]];
        [self addEntity:bulge];
        [self addEntity:[[GALShell alloc] init]];
        
        type = @"cluster";
    }
    
    return self;
}



- (phy::ShellGalaxy::PtrType)generate
{
    auto shellGalaxy = new phy::ShellGalaxy{[self frame],
                                            [self.parameters[@"Name"] UTF8String]};
    
    shellGalaxy->setCore([self.parameters[@"Core"] generate]);
    shellGalaxy->setBulge([self.parameters[@"Bulge"] generate]);
    shellGalaxy->setShell([self.parameters[@"Bulge"] generate]);
    
    return phy::ShellGalaxy::PtrType(shellGalaxy);
}

@end
