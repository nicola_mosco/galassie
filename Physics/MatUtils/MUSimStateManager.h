//
//  MUSimStateManager.h
//  MatUtils
//
//  Created by Nicola Mosco on 26/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import <string>
#import <vector>
#import <memory>
#import <dispatch/dispatch.h>
#import "MUSimState.h"
#import "MUSystem.h"


namespace mu
{
    class SimStateManager
    {
    public:
        SimStateManager(const std::string& dataFileName);
        ~SimStateManager();
        
        void                    operator()(const MUReal t,
                                           System* system);
        
        void                    wait();
        
        
        
    private:
        std::string             dataFileName_;
        
    private:
        typedef std::shared_ptr<SimState>  SimStatePtr;
        typedef std::vector<SimStatePtr>        CacheVec;
        
    private:
        void                    writeToFile(SimStatePtr statePtr);
        
        dispatch_queue_t        writeQueue_;
        dispatch_group_t        writeGroup_;
    };
}
