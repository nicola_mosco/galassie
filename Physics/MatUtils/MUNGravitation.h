//
//  MUNGravitation.h
//  MatUtils
//
//  Created by Nicola Mosco on 12/05/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#import <string>
#import <vector>
#import "MUInteraction.h"
#import "MUTypes.h"


namespace mu
{
    class NGravitation : public Interaction
    {
    public:
        NGravitation( const Params& params ); // Expected params: G, eps.
        ~NGravitation() {}
        
        const
        std::vector<std::string>&   oclKernelSources() const { return oclKernelSources_; }
        
        void                        addBuffers( System* system, oct::Octree* octree, OCLManager* openCL )
        {
            openCL->addBuffer( system->numBodies() * MU_SIZE_REAL3, 1, CL_MEM_READ_WRITE );
            openCL->addBuffer( octree->bufferSize(), 2, CL_MEM_READ_WRITE );
            openCL->addBuffer( system->numBodies() * MU_SIZE_REAL3, 3, CL_MEM_READ_WRITE );
        }
        
        void                        writeBuffers( const MUReal3* pos, oct::Octree* octree, OCLManager* openCL )
        {
            openCL->writeBuffer( 1, octree->numBodies() * MU_SIZE_REAL3, (MUBuffer)pos );
            openCL->writeBuffer( 2, octree->bufferSize(), octree->buffer() );
        }
        
        void                        readBuffers( MUReal3* accel, oct::Octree* octree, OCLManager* openCL )
        {
            openCL->readBuffer( 3, octree->numBodies() * MU_SIZE_REAL3, (MUBuffer)accel );
        }
        
        MUReal                      energy( System* system )
        {
            MUReal systemEnergy = energy( system->numBodies(), system->pos(), system->vel(), system->masses() );
            
            system->energy() = systemEnergy;
            
            return systemEnergy;
        }
        
        MUReal                      energy( const MUSize numBodies,
                                            const MUReal3* pos,
                                            const MUReal3* vel,
                                            const MUReal* masses );
        
        
        
    private:
        std::vector<std::string>    oclKernelSources_;
    };
}
