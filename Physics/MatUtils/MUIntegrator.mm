//
//  MUIntegrator.mm
//  MatUtils
//
//  Created by Nicola Mosco on 08/05/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#import "MUIntegrator.h"
#import "MURungeKutta4.h"
#import <iostream>
#import <iomanip>
#import <Foundation/Foundation.h>


using namespace std;
using namespace mu;
using namespace oct;

typedef RungeKutta4::RK4Ptr RK4Ptr;



Integrator::Integrator(System* system,
                       Interaction* interaction,
                       const MUReal theta,
                       const MUReal iniTime,
                       const MUReal finTime,
                       const MUReal intStep) :
    system_(system), interaction_(interaction),
    theta_(theta), t_i_(iniTime), t_(iniTime), t_f_(finTime)
{
    openCL_ = new OCLManager(interaction_->oclKernelSources(),
                             interaction_->oclKernelName(),
                             system_->numBodies());
    rk4_ = RK4Ptr(new RungeKutta4(system_->numBodies(), this, intStep));
    
    octree_ = new Octree(system_->numBodies(),
                         system_->pos(),
                         system_->masses(),
                         NULL,
                         system_->cmPos(),
                         interaction_->paramForName( "G" ),
                         interaction_->paramForName( "eps" ),
                         theta_);
}



Integrator::~Integrator()
{
    delete openCL_;
    delete octree_;
}



void Integrator::setup(const cl_device_type devType)
{
    openCL_->setup(devType);
    
    interaction_->addBuffers(system_, octree_, openCL_);
}



void Integrator::operator()(const MUReal sampleIntv,
                            const string& dataFileName)
{
    MUReal          samp = t_i_;
    SimStateManager storeSimState(dataFileName);
    
    t_ = t_i_;
    
    while (t_ < t_f_)
    {
        if (t_ + rk4_->intStep() > t_f_)
        {
            rk4_->intStep() = t_f_ - t_;
        }
        
        (*rk4_)(t_, system_->pos(), system_->vel());
        
        t_ += rk4_->intStep();
        
        if (t_ - samp >= sampleIntv || t_ >= t_f_)
        {
            samp = t_;
            
            postTimeUpdateNotification();
            
            storeSimState(t_, system_);
        }
    }
    
    storeSimState.wait();
}



void Integrator::interaction(const MUReal t,
                             const MUReal3 *pos,
                             MUReal3 *accel)
{
    octree_->update(pos, NULL, accel);
    
    interaction_->writeBuffers(pos, octree_, openCL_);
    
    (*openCL_)();
    
    interaction_->readBuffers(accel, octree_, openCL_);
    
    //(*octree_)(pos, accel);
}



inline
void Integrator::postTimeUpdateNotification() const
{
    @autoreleasepool
    {
        NSDictionary*       info            = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [NSNumber numberWithFloat: t_], @"Time", nil];
        NSNotification*     timeUpdateNotif = [NSNotification notificationWithName: @"MUTimeUpdateNotification"
                                                                            object: @"mu::Integrator"
                                                                          userInfo: info];
        
        [[NSNotificationCenter defaultCenter] performSelectorOnMainThread: @selector( postNotification: )
                                                               withObject: timeUpdateNotif
                                                            waitUntilDone: NO];
    }
}
