//
//  GALSpaceView+GALUserEvents.mm
//  Galassie
//
//  Created by Nicola Mosco on 04/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import "GALSpaceView+GALUserEvents.h"
#import "GALBodyRendering.h"


@implementation GALSpaceView (GALUserEvents)

- (void) mouseDragged: (NSEvent *)theEvent
{
    if ( [theEvent modifierFlags] & NSAlternateKeyMask )
    {
        deltaX += translationSens * [theEvent deltaX];
        deltaY -= translationSens * [theEvent deltaY];
    }
    else
    {
        angleX += rotationSens * [theEvent deltaX];
        angleY += rotationSens * [theEvent deltaY];
    }
    
    [self setNeedsDisplay: YES];
    
    return;
}



- (void)scrollWheel:(NSEvent *)theEvent
{
    if ( [theEvent modifierFlags] & NSCommandKeyMask )
    {
        [self zoom: scaleSens * [theEvent deltaY] * bodyScale
               min: bodyScaleMin
               max: bodyScaleMax
          property: &bodyScale];
        
        body->radius() = bodyScale;
        
        body->generateDisplayList();
    }
    else
    {
        GLfloat epsilon = zoomSens * [theEvent deltaY];
        
        if ( fabs( epsilon ) > 1.0 )
        {
            epsilon = 1.0;
        }
        
        GLfloat delta = 1.0 + epsilon;
        
        zoom *= delta;
        
        NSPoint location = [self convertPoint: [theEvent locationInWindow]
                                     fromView: nil];
        NSSize  size     = self.bounds.size;
        NSPoint newPoint = NSMakePoint( location.x / size.width * 2.0 - 1.0,
                                        location.y / size.height * 2.0 - 1.0 );
        zoomPoint = NSMakePoint( newPoint.x - zoomPoint.x, newPoint.y - zoomPoint.y );
        
        //visAngle /= zoom;
        
        /*if ( visAngle < 0.0 )
        {
            visAngle = 0.0;
        }
        else if ( visAngle > 180.0 )
        {
            visAngle = 180.0;
        }*/
        
        visWidth  /= delta * 1.001;
        visHeight /= delta * 1.001;
        
        /*if ( visWidth < 0.0 )
        {
            visWidth = 0.0;
        }
        
        if ( visHeight < 0.0 )
        {
            visHeight = 0.0;
        }*/
        
        visCooL    = -visWidth  * 0.5;
        visCooR    =  visWidth  * 0.5;
        visCooB    = -visHeight * 0.5;
        visCooT    =  visHeight * 0.5;
        
        
        /*visCooL = -visWidth  * 0.5 + ( zoomPoint.x - visCooL ) * epsilon;
        visCooR =  visWidth  * 0.5 + ( zoomPoint.x - visCooR ) * epsilon;
        visCooB = -visHeight * 0.5 + ( zoomPoint.y - visCooB ) * epsilon;
        visCooT =  visHeight * 0.5 + ( zoomPoint.y - visCooT ) * epsilon;*/
        
        self.extension = 2.0 / ( zoom * scale );
    }
    
    [self setNeedsDisplay: YES];
    
    return;
}



/*- (void)mouseDown:(NSEvent *)theEvent
{
    NSPoint location = [self convertPoint: [theEvent locationInWindow]
                                 fromView: nil];
    NSSize  size     = self.bounds.size;
    
    mouse = NSMakePoint( location.x / size.width * 2.0 - 1.0, location.y / size.height * 2.0 - 1.0 );
    
    NSLog(@"m.x = %f; m.y = %f", mouse.x, mouse.y);
    
    //mouseEvent = YES;
    
    self.mouseX = mouse.x;
    self.mouseY = mouse.y;
    
    [self setNeedsDisplay: YES];
}*/

@end
