//
//  MUDistributions.cpp
//  MatUtils
//
//  Created by Nicola Mosco on 02/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import "MUDistributions.h"
#import <gsl/gsl_randist.h>
#import <stdexcept>


using namespace std;
using namespace mu;
using namespace dist;



const string mu::dist::uniform      = "Uniform";
const string mu::dist::gaussianTail = "Gaussian Tail";
const string mu::dist::rayleighTail = "Rayleigh Tail";



/*****************************************************************************/



Distribution* Distribution::newByName( const std::string &name )
{
    Distribution* dist = NULL;
    
    if ( name.compare( "Uniform" ) == 0 )
    {
        dist = new Uniform();
    }
    else if ( name.compare( "Gaussian Tail" ) == 0 )
    {
        dist = new GaussianTail();
    }
    else if ( name.compare( "Rayleigh Tail" ) == 0 )
    {
        dist = new RayleighTail();
    }
    else
    {
        throw invalid_argument( "Invalid distribution name" );
    }
    
    return dist;
}



/*****************************************************************************/



MUReal Uniform::get( void )
{
    return (MUReal)gsl_ran_flat( generator_, a_, b_ );
}



void Uniform::setParameters( const map<string, MUReal>& params )
{
    a_ = params.find( "a" )->second;
    b_ = params.find( "b" )->second;
    
    return;
}



/*****************************************************************************/



MUReal GaussianTail::get( void )
{
    return mu_ + (MUReal)gsl_ran_gaussian_tail( generator_, a_, sigma_ );
}



void GaussianTail::setParameters( const map<string, MUReal>& params )
{
    mu_    = params.find( "mu" )->second;
    sigma_ = params.find( "sigma" )->second;
    a_     = params.find( "a" )->second;
    
    if ( sigma_ <= 0.0 )
    {
        throw domain_error( "Negative sigma for Gaussian Tail" );
    }
    
    return;
}



/*****************************************************************************/



MUReal RayleighTail::get( void )
{
    return (MUReal)gsl_ran_rayleigh_tail( generator_, a_, sigma_ );
}



void RayleighTail::setParameters( const map<string, MUReal>& params )
{
    sigma_ = params.find( "sigma" )->second;
    a_     = params.find( "a" )->second;
    
    if ( a_ < 0.0 || sigma_ <= 0.0 )
    {
        throw domain_error( "Negative parameters for Rayleigh Tail" );
    }
    
    return;
}



/*****************************************************************************/
