//
//  MURotation.h
//  MatUtils
//
//  Created by Nicola Mosco on 10/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import "MUAffineTransformation.h"
#import "MUTypes.h"
//#import <AffineOp/Matrix3x3.h>


@interface MURotation : MUAffineTransformation
{
    MUReal3      axis;
    MUReal       angle;
}

@property (assign) MUReal3 axis;
@property (assign) MUReal  angle;


+ (MURotation*)rotationWithAxis:(MUReal3)anAxis
                          angle:(MUReal)anAngle;


- (id)initWithAxis:(MUReal3)anAxis
             angle:(MUReal)anAngle;

//- (cryph::Matrix3x3)rotationMatrix;

- (void)applyToData:(NSMutableData *)pointsData
          withRange:(NSRange)range;

@end
