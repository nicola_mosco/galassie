//
//  GALGalaxyBuilder.h
//  Galassie
//
//  Created by Nicola Mosco on 10/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MatUtils/MUTypes.h>


@class GALSimulationParameters;


@interface GALGalaxyBuilder : NSObject

- (id)initWithSimulationParameters:(GALSimulationParameters*)someParams;

- (void)generateStateData:(NSMutableData*)stateData;
- (void)generateMassData:(NSMutableData*)massData;

@end
