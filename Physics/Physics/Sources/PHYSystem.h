//
//  PHYSystem.h
//  Physics
//
//  Created by Nicola Mosco on 06/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYSystem__
#define __Physics__PHYSystem__

#include "PHYTypes.h"
#include "PHYUniverse.h"


namespace phy
{
    class System
    {
    public:
        typedef std::shared_ptr<System> SPtrType;
        
    public: // Constructors and destructors.
        System(Universe::PtrType const& universe);
        virtual ~System() = default;
        
    public:
        Real numBodies() const;
        Real mass() const; // Total mass of the system.
        Real energy() const; // Initial energy of the system.
        
    public:
        virtual void build();
        
        
        
    protected:
        virtual void extractBodiesData();
        
    protected:
        void computeMass(); // Total mass of the system.
        void computeEnergy(); // Initial energy of the system.
    
    protected: // Istantaneous simulation data.
        RVec3Array  pos_;
        RVec3Array  vel_;
        RealArray   masses_;
        
    protected:
        Body::ListType bodies_; // Initial conditions data.
        
        
        
    private: // General system parameters.
        Size numBodies_;
        Real mass_;
        Real energy_;
    };
}

#endif /* defined(__Physics__PHYSystem__) */
