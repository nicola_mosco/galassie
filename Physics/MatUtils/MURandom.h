//
//  MURandom.h
//  MatUtils
//
//  Created by Nicola Mosco on 29/07/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import <gsl/gsl_rng.h>
#import "MUTypes.h"


namespace mu
{
    class Random
    {
    public:
        static
        void setup() { srand( (unsigned)time( NULL ) ); }
        
    public:
        Random( const gsl_rng_type* type = gsl_rng_taus );
        Random( const Random& random )
        {
            if ( this != &random )
            {
                type_ = random.type();
                
                generator_ = gsl_rng_alloc( type_ );
                
                gsl_rng_set( generator_, time( NULL ) );
            }
        }
        ~Random( void );
        
        
    public:
        MUReal          getRnd( void ) { return (MUReal) gsl_rng_uniform( generator_ ); }
        MUReal          getRndPos( void ) { return (MUReal) gsl_rng_uniform_pos( generator_ ); }
        MUReal          getRndInRange( MUReal a, MUReal b ) { return a + ( b - a ) * getRnd(); }
        
        const
        gsl_rng_type*   type( void ) const { return type_; }
        
        
        
    protected:
        gsl_rng*        generator_;
        
        
        
    private:
        const
        gsl_rng_type*   type_;
    };
}
