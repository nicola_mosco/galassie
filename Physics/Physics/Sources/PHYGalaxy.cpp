//
//  PHYGalaxy.cpp
//  Physics
//
//  Created by Nicola Mosco on 07/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#include "PHYGalaxy.h"


using namespace phy;



Galaxy::Galaxy(const ReferenceFrame& aRefFrame,
               const std::string& name)
    : Cluster{aRefFrame, name}
{}



void
Galaxy::addBodyCluster(BodyCluster::PtrType&& bodyCluster)
{
    bodyClusters_.push_back(std::forward<BodyCluster::PtrType>(bodyCluster));
}



void
Galaxy::build(Body::ListType& bodies)
{
    for (auto& bCluster : bodyClusters_)
    {
        bCluster->build(bodies);
    }
}
