#define USE_OCTREE


#ifndef NULL
#   define NULL ((MUPtr)0)
#endif // NULL


#ifndef MU_SIZE_PTR
#   define MU_SIZE_PTR  sizeof( MUPtr )
#endif // MU_SIZE_PTR



#define CL_SUCCESS  0

#define CL_TRUE     1
#define CL_FALSE    0



#define OCT_SIZE_PTR        MU_SIZE_PTR
#define OCT_SIZE_BYTE       sizeof( MUByte )
#define OCT_SIZE_BUFFER     sizeof( MUBuffer )
#define OCT_SIZE_HEADER     sizeof( struct _oct_header )
#define OCT_SIZE_NODE       sizeof( struct _oct_node )


#define OCT_NUM_CELLS       8
#define OCT_NUM_BRANCHES    OCT_NUM_CELLS


#define OCTSIZE( n ) ((MUSize)(n))
#define OCTREF( i )  ((MURef)(i))



typedef uchar           cl_uchar;
typedef uint            MUBool;
typedef uint            cl_uint;
typedef int             cl_int;
typedef ulong           cl_ulong;
typedef int3            cl_int3;
typedef uint3           cl_uint3;
typedef float           cl_float;
typedef float3          cl_float3;
typedef cl_uchar        MUByte;
typedef cl_uint         MURef;
typedef cl_int          MUCell;
typedef cl_uint         MUSize;

typedef __global
    MUByte     *   MUBuffer;
typedef __global
    void        *   MUPtr;

typedef cl_float        MUReal;
typedef cl_float3       MUReal3;



struct _oct_node
{
    MUCell                 c;          // Cell number relative to parent.
    MUBool                 isLeaf;
    MUReal                 M;
    MUReal3                cm;
    MUReal                 L;
    MUReal3                center;
    MURef                  branches[ OCT_NUM_BRANCHES ];
    MURef                  parent;
} __attribute__(( aligned(16) ));

typedef __global struct
    _oct_node       *   OctNode;
typedef OctNode         *   OctPNode;
typedef OctPNode            OctBranch;


struct _oct_header
{
    MUReal                   L;
    MUReal3                  center;
    MUSize                   n_bodies;
    MUReal3                  cm;
    MUReal                   M;
    MUReal                   theta;
    MUReal                   eps;
    MUReal                   G;
    MURef                  root;
} __attribute__(( aligned(16) ));

typedef __global struct
    _oct_header     *   OctHeader;



MUReal muSign( const MUReal x )
{
    return ( (signbit( x ) == 0) ? +1 : -1 );
}

void octComputeAccelForBody( MUReal3 *const accPtr,
                            const MUReal3 pos,
                            MUBuffer octree );
MUReal3 octComputeAccelFor2Bodies( const MUReal3 pos1,
                                  const MUReal3 pos2,
                                  const MUReal M2,
                                  const MUReal G,
                                  const MUReal eps );

MUBool octNodeIsLeaf( const OctNode node );
MUBool octNodeIsEmpty( const MURef nodeOffset );
MUBool octBranchIsEmpty( const OctNode node,
                        const MUCell cell );
MUBool octNodeBHTest( const OctNode node,
                     const MUReal d,
                     const MUReal theta );

OctHeader octGetHeader( MUBuffer octree );
OctNode octGetNode( MUBuffer octree,
                   const MURef nodeOffset );
OctNode octGetBranch( const OctNode node,
                     const MUCell c,
                     MUBuffer octree );
OctNode octGetParent( const OctNode node,
                     MUBuffer octree );
OctNode octGetRoot( MUBuffer octree );

MUCell octNodeCell( const OctNode node );

MUBool octFindNotEmptyBranch( OctPNode curNodePtr,
                             MUCell* cPtr,
                             MUBuffer octree );

MUBool octVisitNode( OctPNode curNodePtr,
                    MUReal3 *const accPtr,
                    const MUReal3 pos,
                    MUBuffer octree );
MUBool octDescendNode( OctPNode curNodePtr,
                      MUCell* cPtr,
                      MUBuffer octree );
void octAscendNode( OctPNode curNodePtr,
                   MUCell* cPtr,
                   MUBuffer octree );



__kernel void ngravkernel( MUSize numBodies,
                          __global MUReal3* pos,
                          __global MUByte* octree,
                          __global MUReal3* accel )
{
    MUSize          i      = get_global_id( 0 );
    MUReal3         acc_v  = (MUReal3)( 0.0, 0.0, 0.0 );
    
    if ( i >= numBodies )
    {
        return;
    }
    
    octComputeAccelForBody( &acc_v, pos[ i ], octree );
    
    accel[ i ] = acc_v;
    
    return;
}



inline MUReal3 octComputeAccelFor2Bodies( const MUReal3 pos1,
                                         const MUReal3 pos2,
                                         const MUReal M2,
                                         const MUReal G,
                                         const MUReal eps )
{
    MUReal        a    = 0.0;
    MUReal        d    = 0.0;
    MUReal3       dpos = (MUReal3)( 0.0, 0.0, 0.0 );
    
    dpos.x  = pos2.x - pos1.x;
    dpos.y  = pos2.y - pos1.y;
    dpos.z  = pos2.z - pos1.z;
    
    d       = sqrt( dpos.x * dpos.x +
                   dpos.y * dpos.y +
                   dpos.z * dpos.z +
                   eps );
    
    a       = G * M2 / ( d * d * d );
    
    dpos.x *= a;
    dpos.y *= a;
    dpos.z *= a;
    
    return dpos;
}



void octComputeAccelForBody( MUReal3 *const accPtr,
                            const MUReal3 pos,
                            MUBuffer octree )
{
    MUBool         processed = CL_FALSE;
    MUBool         descended = CL_FALSE;
    MUBool         visited   = CL_FALSE;
    MUCell         c         = 0;
    OctNode         curNode   = octGetRoot( octree );
    
    while ( curNode != NULL )
    {
        if ( !visited )
        {
            processed = octVisitNode( &curNode,
                                     accPtr,
                                     pos,
                                     octree );
        }
        
        if ( !processed )
        {
            descended = octDescendNode( &curNode, &c, octree );
            
            if ( descended )
            {
                visited = CL_FALSE;
            }
        }
        
        if ( !descended || processed )
        {
            visited   = CL_TRUE;
            processed = CL_FALSE;
            
            octAscendNode( &curNode, &c, octree );
        }
    }
    
    return;
}



MUBool octVisitNode( OctPNode curNodePtr,
                    MUReal3 *const accPtr,
                    const MUReal3 pos,
                    MUBuffer octree )
{
    MUBool         processed = CL_FALSE;
    MUReal3          cm        = (*curNodePtr)->cm;
    MUReal3          acc_v     = (MUReal3)( 0.0, 0.0, 0.0 );
    OctHeader       header;
    
    if ( cm.x == pos.x && cm.y == pos.y && cm.z == pos.z )
    {
        return CL_FALSE;
    }
    
    
    header = octGetHeader( octree );
    
    if ( octNodeIsLeaf( *curNodePtr ) )
    {
        processed = CL_TRUE;
    }
    else
    {
        MUReal d;
        
        d = sqrt( ( pos.x - cm.x ) * ( pos.x - cm.x ) +
                 ( pos.y - cm.y ) * ( pos.y - cm.y ) +
                 ( pos.z - cm.z ) * ( pos.z - cm.z ) );
        
        processed = octNodeBHTest( *curNodePtr, d, header->theta );
    }
    
    if ( processed == CL_TRUE )
    {
        acc_v = octComputeAccelFor2Bodies( pos,
                                          cm,
                                          (*curNodePtr)->M,
                                          header->G,
                                          header->eps );
        
        *accPtr += acc_v; // Guai ad accedere alle componenti di accPtr, va tutto a...
    }
    
    return processed;
}



inline
MUBool octDescendNode( OctPNode curNodePtr,
                      MUCell* cPtr,
                      MUBuffer octree )
{
    MUBool     descended = CL_FALSE;
    OctNode     prevNode  = NULL;
    
    prevNode  = *curNodePtr;
    
    descended = octFindNotEmptyBranch( curNodePtr, cPtr, octree );
    
    return descended; 
} 



inline 
void octAscendNode( OctPNode curNodePtr, 
                   MUCell* cPtr, 
                   MUBuffer octree ) 
{ 
    *cPtr = octNodeCell( *curNodePtr ) + 1; 
    
    *curNodePtr = octGetParent( *curNodePtr, octree ); 
    
    return; 
} 



inline 
MUBool octFindNotEmptyBranch( OctPNode curNodePtr, 
                             MUCell* cPtr, 
                             MUBuffer octree ) 
{ 
    MUBool     found = CL_FALSE; 
    
    while ( *cPtr < OCT_NUM_BRANCHES && 
           octBranchIsEmpty( *curNodePtr, *cPtr ) ) 
    { 
        (*cPtr)++; 
    } 
    
    if ( *cPtr < OCT_NUM_BRANCHES ) 
    { 
        *curNodePtr = octGetBranch( *curNodePtr, *cPtr, octree ); 
        
        found       = CL_TRUE; 
        
        *cPtr       = 0; 
    } 
    
    return found; 
} 



inline MUBool octNodeIsLeaf( const OctNode node ) 
{ 
    return node->isLeaf; 
} 



inline MUBool octNodeIsEmpty( const MURef nodeOffset ) 
{ 
    return ( nodeOffset == OCTREF(0) ); 
} 



inline MUBool octBranchIsEmpty( const OctNode node, 
                               const MUCell cell ) 
{ 
    if ( cell >= OCT_NUM_BRANCHES ) 
    { 
        return CL_FALSE; 
    } 
    
    return ( node->branches[ cell ] == OCTREF(0) ); 
} 



inline MUBool octNodeBHTest( const OctNode node, 
                            const MUReal d, 
                            const MUReal theta ) 
{ 
    return ( node->L / d < theta ); 
} 



inline OctHeader octGetHeader( MUBuffer octree ) 
{ 
    return (OctHeader)octree; 
} 



inline OctNode octGetNode( MUBuffer octree, 
                          const MURef nodeOffset ) 
{ 
    if ( nodeOffset == 0 ) 
    { 
        return NULL; 
    } 
    
    return (OctNode) ( octree + nodeOffset ); 
} 



inline OctNode octGetBranch( const OctNode node, 
                            const MUCell c, 
                            MUBuffer octree ) 
{ 
    MURef      branchOffset = node->branches[ c ]; 
    
    if ( branchOffset == 0 ) 
    { 
        return NULL; 
    } 
    
    return (OctNode) ( octree + branchOffset ); 
} 



inline OctNode octGetParent( const OctNode node, 
                            MUBuffer octree ) 
{ 
    if ( node->parent == 0 ) 
    { 
        return NULL; 
    } 
    
    return (OctNode) ( octree + node->parent ); 
} 



inline OctNode octGetRoot( MUBuffer octree ) 
{ 
    return octGetNode( octree, ((OctHeader)octree)->root ); 
} 



inline MUCell octNodeCell( const OctNode node ) 
{ 
    return node->c; 
}