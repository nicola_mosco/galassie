//
//  PHYDeviceData.h
//  Physics
//
//  Created by Nicola Mosco on 15/04/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYDeviceData__
#define __Physics__PHYDeviceData__

#include <memory>


namespace phy
{
    class DeviceDataImplementation;
    
    
    class DeviceData
    {
    public:
        DeviceData(std::unique_ptr<DeviceDataImplementation> implementation);
        DeviceData(DeviceData&& deviceData);
        
        
        
    private:
        std::unique_ptr<DeviceDataImplementation> implementation_;
        
    private:
        DeviceData() = delete;
        DeviceData(const DeviceData& deviceData) = delete;
        
        const DeviceData& operator=(const DeviceData& deviceData) = delete;
    };
}

#endif /* defined(__Physics__PHYDeviceData__) */
