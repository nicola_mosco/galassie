//
//  PHYCommunicationProtocol.h
//  Physics
//
//  Created by Nicola Mosco on 15/04/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYCommunicationProtocol__
#define __Physics__PHYCommunicationProtocol__

#include <memory>


namespace phy
{
    class Notification;
    class Request;
    
    
    class CommunicationProtocol
    {
    public:
        virtual ~CommunicationProtocol() = 0;
        
    public:
        virtual void notify(std::unique_ptr<Notification> notification) = 0;
        
        
        
    protected:
        CommunicationProtocol() = default;
        
        
        
    private:
        void sendNotification(std::unique_ptr<Notification> notification,
                              CommunicationProtocol& observer);
    };
}

#endif /* defined(__Physics__PHYCommunicationProtocol__) */
