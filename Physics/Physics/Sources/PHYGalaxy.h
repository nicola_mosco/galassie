//
//  PHYGalaxy.h
//  Physics
//
//  Created by Nicola Mosco on 07/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYGalaxy__
#define __Physics__PHYGalaxy__

#include "PHYCluster.h"
#include "PHYBodyCluster.h"


namespace phy
{
    class Galaxy : public Cluster
    {
    public:
        Galaxy(const ReferenceFrame& aRefFrame = ReferenceFrame{},
               const std::string& name = "Galaxy");
        virtual ~Galaxy() = default;
        
    public:
        void addBodyCluster(BodyCluster::PtrType&& bodyCluster);
        
    public:
        virtual void build(Body::ListType& bodies) final;
        
        
        
    protected:
        BodyCluster::ListType bodyClusters_;
    };
}

#endif /* defined(__Physics__PHYGalaxy__) */
