//
//  MUSimState.mm
//  MatUtils
//
//  Created by Nicola Mosco on 22/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import "MUSimState.h"
#import <iostream>
#import <iomanip>


using namespace std;
using namespace mu;



SimState::SimState( const MUSize numBodies )
{
    numBodies_ = numBodies;
    
    pos_ = new MUPoint3[ numBodies_ ];
    vel_ = new MUPoint3[ numBodies_ ];
}



SimState::SimState( const MUSize numBodies,
                    const MUReal t,
                    const MUReal3* pos,
                    const MUReal3* vel )
{
    numBodies_ = numBodies;
    
    pos_ = new MUPoint3[ numBodies_ ];
    vel_ = new MUPoint3[ numBodies_ ];
    
    update( t, pos, vel );
}



SimState::~SimState()
{
    delete[] pos_;
    delete[] vel_;
}



void SimState::update( const MUReal t,
                       const MUReal3 *pos,
                       const MUReal3 *vel )
{
    t_ = t;
    
    for ( size_t i = 0; i < numBodies_; ++i )
    {
        pos_[ i ] = muPoint3( pos[ i ].x,
                              pos[ i ].y,
                              pos[ i ].z );
        
        vel_[ i ] = muPoint3( vel[ i ].x,
                              vel[ i ].y,
                              vel[ i ].z );
    }
}



void SimState::readFromFile( FILE *dataFile )
{
    fread( &t_, MU_SIZE_REAL, 1, dataFile );
    
    fread( pos_, MU_SIZE_POINT3, numBodies_, dataFile );
    fread( vel_, MU_SIZE_POINT3, numBodies_, dataFile );
}



void SimState::writeToFile( FILE *dataFile )
{
    fwrite( &t_, MU_SIZE_REAL, 1, dataFile );
    
    fwrite( pos_, MU_SIZE_POINT3, numBodies_, dataFile );
    fwrite( vel_, MU_SIZE_POINT3, numBodies_, dataFile );
}



void SimState::getPos( MUReal3 *pos ) const
{
    for ( size_t i = 0; i < numBodies_; ++i )
    {
        pos[ i ] = muReal3( pos_[ i ].x, pos_[ i ].y, pos_[ i ].z );
    }
}



void SimState::getVel( MUReal3 *vel ) const
{
    for ( size_t i = 0; i < numBodies_; ++i )
    {
        vel[ i ] = muReal3( vel_[ i ].x, vel_[ i ].y, vel_[ i ].z );
    }
}



void SimState::print( const MUSize i, const int precision ) const
{
    cout << scientific << setprecision( precision ) << t_ << ":\npos = ( " << pos_[ i ].x << ", " << pos_[ i ].y << ", " << pos_[ i ].z << " ) \n";
    cout << scientific << setprecision( precision )       <<    "vel = ( " << vel_[ i ].x << ", " << vel_[ i ].y << ", " << vel_[ i ].z << " )" << endl;
}
