//
//  GALGalaxy.mm
//  Galassie
//
//  Created by Nicola Mosco on 17/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import <Physics/PHYGalaxy.h>

#import "GALGalaxy.h"
#import "GALVector3.h"


@implementation GALGalaxy

- (id)initWithName:(NSString *)name
{
    self = [super initWithName:name];
    
    if (self)
    {
        self.parameters[@"Position"] = [[GALVector3 alloc] initWithX: @0.0
                                                                   y: @0.0
                                                                   z: @0.0];
        self.parameters[@"Velocity"] = [[GALVector3 alloc] initWithX: @0.0
                                                                   y: @0.0
                                                                   z: @0.0];
        self.parameters[@"Axis"]     = [[GALVector3 alloc] initWithX: @0.0
                                                                   y: @0.0
                                                                   z: @0.0];
        self.parameters[@"Angle"]    = @0.0;
        
        entities = [NSMutableArray array];
        
        type = @"cluster";
    }
    
    return self;
}



- (phy::Galaxy::PtrType)generate
{
    auto galaxy = new phy::Galaxy{[self frame],
                                  [self.parameters[@"Name"] UTF8String]};
    
    return phy::Galaxy::PtrType(galaxy);
}

@end
