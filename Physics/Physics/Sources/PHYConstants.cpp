//
//  PHYConstants.cpp
//  Physics
//
//  Created by Nicola Mosco on 07/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#include <cmath>

#include "PHYConstants.h"


namespace phy
{
    const Real m_y  = Real(1e-24);
    const Real m_z  = Real(1e-21);
    const Real m_a  = Real(1e-18);
    const Real m_f  = Real(1e-15);
    const Real m_p  = Real(1e-12);
    const Real m_n  = Real(1e-9);
    const Real m_mu = Real(1e-6);
    const Real m_m  = Real(1e-3);
    const Real m_c  = Real(1e-2);
    const Real m_d  = Real(1e-1);
    const Real m_da = Real(1e1);
    const Real m_h  = Real(1e2);
    const Real m_k  = Real(1e3);
    const Real m_M  = Real(1e6);
    const Real m_G  = Real(1e9);
    const Real m_T  = Real(1e12);
    const Real m_P  = Real(1e15);
    const Real m_E  = Real(1e18);
    const Real m_Z  = Real(1e21);
    const Real m_Y  = Real(1e24);
    
    
    
    const Real uSI_s   = Real(1.0);
    const Real uSI_min = Real(60.0);
    const Real uSI_h   = Real(3600.0);
    const Real uSI_day = Real(24.0) * uSI_h;
    const Real uSI_y   = Real(3.15581500224e7);
    const Real uSI_c   = Real(75.0) * uSI_y + Real(25.0) * (uSI_y + 1);
    const Real uSI_gy  = Real(2.25e2) * m_M * uSI_y;
    
    const Real uSI_m  = Real(1.0);
    const Real uSI_au = Real(1.49597870e11);
    const Real uSI_ly = Real(9.46073047e15);
    const Real uSI_pc = Real(3.08567758066631e16);
    
    const Real uSI_kg = Real(1.0);
    
    
    const Real kSI_G          = Real(6.67259e-11);
    const Real kSI_Sol_M      = Real(1.9891e30);
    const Real kSI_Gal_Radius = Real(4.89e4) * uSI_ly;
    
    
    
    const Real k_G = Real(1.0);
    
    
    const Real u_s   = std::sqrt(k_G / kSI_G * (u_m / u_kg) * u_m * u_m);
    const Real u_min = uSI_min * u_s;
    const Real u_h   = uSI_h * u_s;
    const Real u_day = uSI_day * u_s;
    const Real u_y   = uSI_y * u_s;
    const Real u_c   = uSI_c * u_s;
    const Real u_gy  = Real(2.25e2) * m_M * u_y;
    
    const Real u_m  = Real(1.0) / (kSI_Gal_Radius);
    const Real u_au = uSI_au * u_m;
    const Real u_ly = uSI_ly * u_m;
    const Real u_pc = uSI_pc * u_m;
    
    const Real u_kg = Real(1.0) / (kSI_Sol_M);
    
    
    const Real k_Sol_M      = kSI_Sol_M * u_kg;
    const Real k_Gal_Radius = Real(4.89e4) * u_ly;
}
