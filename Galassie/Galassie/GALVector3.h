//
//  GALVector3.h
//  Galassie
//
//  Created by Nicola Mosco on 17/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Physics/PHYAlgebra.h>


@interface GALVector3 : NSObject

@property (strong) NSNumber* x;
@property (strong) NSNumber* y;
@property (strong) NSNumber* z;

@property (readonly) phy::RVec3 realVector3;
@property (readonly) phy::Vector3 vector3;


- (id)initWithX:(NSNumber*)x
              y:(NSNumber*)y
              z:(NSNumber*)z;

@end
