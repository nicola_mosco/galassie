//
//  PHYUniverse.h
//  Physics
//
//  Created by Nicola Mosco on 06/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYUniverse__
#define __Physics__PHYUniverse__

#include "PHYGeometry.h"
#include "PHYBody.h"
#include "PHYCluster.h"


namespace phy
{
    class Universe : public Geometry
    {
    public:
        typedef std::unique_ptr<Universe> PtrType;
        
    public: // Constructors and destructors.
        Universe(const std::string& name = "Universe");
        virtual ~Universe() = default;
        
    public: // Building physical system structure.
        void addCluster(Cluster::PtrType&& cluster);
        
        virtual void build(Body::ListType& bodies) final;
        
        
        
    private:
        Cluster::ListType clusters_;
    };
}

#endif /* defined(__Physics__PHYUniverse__) */
