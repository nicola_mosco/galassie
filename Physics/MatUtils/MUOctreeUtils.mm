//
//  MUOctreeUtils.mm
//  MatUtils
//
//  Created by Nicola Mosco on 07/08/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#import "MUOctreeUtils.h"
#import "MUGeneralia.h"
#import <assert.h>


/*****************************************************************************\
|*                    Public functions implementation...                     *|
\*****************************************************************************/

MUReal3 octPairsCenterOfMass( const cl_uint n, ... )
{
    cl_uint         i;
    MUReal3          cm  = { 0.0 };
    MUReal3          pos = { 0.0 };
    MUReal           m   = 0.0;
    MUReal           M   = 0.0;
    va_list         vl;
    
    va_start( vl, n );
    
    
    for ( i = 0; i < n; i++ )
    {
        double tmp_m;
        
        pos   = va_arg( vl, MUReal3 );
        tmp_m = va_arg( vl, double );
        m     = (MUReal)tmp_m;
        
        cm    = muSum( cm, muScale( pos, m ) );
        
        M    += m;
    }
    
    cm = muInvScale( cm, M );
    
    
    va_end( vl );
    
    return cm;
}



void octOctreeExtension( const MUReal3* pos,
                         const cl_ulong n_bodies,
                         const MUReal3 center,
                         MUReal *const LPtr )
{
    cl_uint         i;
    MUReal        max;
    MUReal        d_q;
    MUReal3       diff;
    
    diff = muDiff( pos[ 0 ], center );
    
    max = muScalProd( diff, diff );
    
    for ( i = 1; i < n_bodies; i++ )
    {
        diff = muDiff( pos[ i ], center );
        
        d_q = muScalProd( diff, diff );
        
        if ( d_q > max )
        {
            max = d_q;
        }
    }
    
    *LPtr = (MUReal)sqrt( max );
    
    return;
}



void octComputeAccelForBody(MUReal3 *const accPtr,
                            const MUReal3 pos,
                            const OctNode node,
                            const MUBuffer octree)
{
    MUCell          c;
    MUReal          d;
    MUReal3         cm      = node->cm;
    MUReal3         acc_v   = { 0.0 };
    OctHeader       header  = NULL;
    
    
    header = (OctHeader)octree;
    
    if ( muEqual( node->cm, pos ) )
    {
        return;
    }
    
    d = sqrtf((pos.x - cm.x) * (pos.x - cm.x) +
              (pos.y - cm.y) * (pos.y - cm.y) +
              (pos.z - cm.z) * (pos.z - cm.z));
    
    if (octNodeIsLeaf(node) ||
        octNodeBHTest(node, d, header->theta))
    {
        acc_v = octComputeAccelFor2Bodies(pos,
                                          cm,
                                          node->M,
                                          header->G,
                                          header->eps);
        
        (*accPtr).x += acc_v.x;
        (*accPtr).y += acc_v.y;
        (*accPtr).z += acc_v.z;
    }
    else
    {
        for (c = 0; c < OCT_NUM_BRANCHES; c++)
        {
            if (octBranchIsEmpty(node, c))
            {
                continue;
            }
            
            
            octComputeAccelForBody(accPtr,
                                   pos,
                                   octGetBranch(node, c, octree),
                                   octree);
        }
    }
    
    return;
}



cl_int3 octCellSign( cl_uint c )
{
    cl_int3      sign = { 0 };
    
    if ( c == 0 )
    {
        sign.x = +1;
        sign.y = +1;
        sign.z = +1;
    }
    else if ( c == 1 )
    {
        sign.x = -1;
        sign.y = +1;
        sign.z = +1;
    }
    else if ( c == 2 )
    {
        sign.x = -1;
        sign.y = -1;
        sign.z = +1;
    }
    else if ( c == 3 )
    {
        sign.x = +1;
        sign.y = -1;
        sign.z = +1;
    }
    else if ( c == 4 )
    {
        sign.x = +1;
        sign.y = -1;
        sign.z = -1;
    }
    else if ( c == 5 )
    {
        sign.x = +1;
        sign.y = +1;
        sign.z = -1;
    }
    else if ( c == 6 )
    {
        sign.x = -1;
        sign.y = +1;
        sign.z = -1;
    }
    else if ( c == 7 )
    {
        sign.x = -1;
        sign.y = -1;
        sign.z = -1;
    }
    
    return sign;
}

/*****************************************************************************/
