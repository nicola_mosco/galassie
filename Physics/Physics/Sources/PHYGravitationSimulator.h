//
//  PHYGravitationSimulator.h
//  Physics
//
//  Created by Nicola Mosco on 14/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYGravitationSimulator__
#define __Physics__PHYGravitationSimulator__

#include "PHYSimulator.h"


namespace phy
{
    class GravitationSimulator : public Simulator
    {
    public: // c-tors and d-tors.
        GravitationSimulator(const cl_device_type clDeviceType = CL_DEVICE_TYPE_GPU);
        virtual ~GravitationSimulator() = default; // Default could be fine.
        
        
        
    protected: // This device is not intended to be subclassed.
        virtual void task() final;
        
        
        
    private:
        GravitationSimulator(const GravitationSimulator& gravSim) = delete;
        const GravitationSimulator& operator=(const GravitationSimulator& gravSim) = delete;
    };
}

#endif /* defined(__Physics__PHYGravitationSimulator__) */
