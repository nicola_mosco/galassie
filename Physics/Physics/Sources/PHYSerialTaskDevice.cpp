//
//  PHYSerialTaskDevice.cpp
//  Physics
//
//  Created by Nicola Mosco on 24/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#include "PHYSerialTaskDevice.h"


using namespace phy;



SerialTaskDevice::SerialTaskDevice(const std::string& description)
    : Device{description}
{}



SerialTaskDevice::~SerialTaskDevice() = default;



void
SerialTaskDevice::performTask(dispatch_block_t beginAction,
                              dispatch_block_t endAction)
{
    Device::performTask(beginAction,
                        ^{
                            endAction();
                            
                            setState(State::IDLE);
                            
                            writeMessage("device is IDLE...");
                        });
}
