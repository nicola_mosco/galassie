//
//  GALDocumentController.h
//  Galassie
//
//  Created by Nicola Mosco on 04/01/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface GALDocumentController : NSDocumentController

@end
