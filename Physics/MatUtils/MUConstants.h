/*
 *  MUConstants.h
 *  MatUtils
 *
 *  Created by Nicola Mosco on 22/12/10.
 *  Copyright 2010 Nicola Mosco. All rights reserved.
 *
 */

#ifndef _MU_CONSTANTS_
#define _MU_CONSTANTS_

//------------------------- Moltiplicatori ---------------------------
#define MM_Y   MU_REAL(1e24)
#define MM_Z   MU_REAL(1e21)
#define MM_E   MU_REAL(1e18)
#define MM_P   MU_REAL(1e15)
#define MM_T   MU_REAL(1e12)
#define MM_G   MU_REAL(1e9)
#define MM_M   MU_REAL(1e6)
#define MM_k   MU_REAL(1e3)
#define MM_h   MU_REAL(1e2)
#define MM_da  MU_REAL(1e1)
#define MM_d   MU_REAL(1e-1)
#define MM_c   MU_REAL(1e-2)
#define MM_m   MU_REAL(1e-3)
#define MM_mu  MU_REAL(1e-6)
#define MM_n   MU_REAL(1e-9)
#define MM_p   MU_REAL(1e-12)
#define MM_f   MU_REAL(1e-15)
#define MM_a   MU_REAL(1e-18)
#define MM_z   MU_REAL(1e-21)
#define MM_y   MU_REAL(1e-24)

#define MM_Y_S  "Y"
#define MM_Z_S  "Z"
#define MM_E_S  "E"
#define MM_T_S  "T"
#define MM_G_S  "G"
#define MM_M_S  "M"
#define MM_k_S  "k"
#define MM_h_S  "h"
#define MM_da_S "da"
#define MM_d_S  "d"
#define MM_c_S  "c"
#define MM_m_S  "m"
#define MM_mu_S "µ"
#define MM_n_S  "n"
#define MM_p_S  "p"
#define MM_f_S  "f"
#define MM_a_S  "a"
#define MM_z_S  "z"
#define MM_y_S  "y"
//--------------------------------------------------------------------

//------------------------- Costanti Fisiche -------------------------
#define CF_c    MU_REAL(2.99792458e8)      // Velocità della luce nel vuoto; SI.
#define CF_G    MU_REAL(6.67259e-11)       // Costante di gravitazione universale; SI.
#define CF_e    MU_REAL(1.60217653e-19)    // Carica del positrone; {e} = C.
#define CF_me   MU_REAL(9.10938188e-31)    // Massa dell'elettrone; SI.
#define CF_eps0 MU_REAL(8.85418781762e-12) // Permittività elettrica in SI; {eps0} = F/m.
#define CF_mu0  MU_REAL(4.0e-7*M_PI)       // Permeabilità magnetica del vuoto; {mu0} = H/m.
#define CF_J    MU_REAL(4.186)             // L / Q = J.
#define CF_SOLM MU_REAL(1.9891e30)         // Massa del Sole; SI.
#define CF_GALM MU_REAL(6.82e11*CF_SOLM)   // Milky Way Mass.
//--------------------------------------------------------------------

//------------------------- Unità di Misura --------------------------
#define UM_m    MU_REAL(1.0)
#define UM_g    MU_REAL(1.0)
#define UM_s    MU_REAL(1.0)
#define UM_A    MU_REAL(1.0)
#define UM_K    MU_REAL(1.0)
#define UM_mol  MU_REAL(1.0)
#define UM_cd   MU_REAL(1.0)

#define UM_l_S   "m"         //Lunghezza.
#define UM_M_S   "kg"        //Massa.
#define UM_t_S   "s"         //Tempo.
#define UM_I_S   "A"         //Corrente Elettrica.
#define UM_T_S   "K"         //Temperatura Termodinamica.
#define UM_q_S   "mol"       //Quantità di Sostanza.
#define UM_Iv_S  "cd"        //Intensità Luminosa.

#define UM_f_S  "Hz"        //Frequenza.
#define UM_F_S   "N"         //Forza.
#define UM_E_S   "J"         //Energia.
#define UM_theta_S "rad"       //Angolo Piano.
#define UM_A_S   "m^2"       //Area.
#define UM_V_S   "m^3"       //Volume.
#define UM_v_S   "m s^-1"    //Velocità.
#define UM_omega_S   "rad s^-1" //Velocità Angolare.
#define UM_a_S    "m s^-2"  //Accelerazione.

#define UM_au  MU_REAL(1.49597870e11) * UM_m       //Unità Astronomica.
#define UM_ly  MU_REAL(9.46073047e15) * UM_m       //Anno Luce.
#define UM_pc  MU_REAL(3.08567758066631e16) * UM_m //Parsec.
#define UM_Ang MU_REAL(1e-10) * UM_m               //Ångstrom.

#define UM_au_S  "ua"
#define UM_ly_S  "al"
#define UM_pc_S  "pc"
#define UM_Ang_S "Å"
//--------------------------------------------------------------------

//------------------------- Valori Notevoli --------------------------
#define UM_DAY         MU_REAL(8.64e4) // Seconds per day.
#define UM_YEAR        MU_REAL(3.15581500224e7) // Seconds per year.
#define UM_CENTURY     MU_REAL(3.15581503e9) // Seconds per century.
#define UM_MER_YEAR    MU_REAL(7.60055184e6) // Seconds per Mercury-year.
//--------------------------------------------------------------------

//------------------------ Valori Particolari ------------------------
#define CF_G_SOLM_LY_CENTURY    MU_REAL(CF_G*(UM_CENTURY/UM_ly)*(UM_CENTURY/UM_ly)*(CF_SOLM/UM_ly)) // G in masse solari, anni luce, secoli.
#define CF_G_SOLM_LY_YEAR       MU_REAL(CF_G*(UM_YEAR/UM_ly)*(UM_YEAR/UM_ly)*(CF_SOLM/UM_ly))     // G in masse solari, anni luce, anni.
#define CF_G_SOLM_AU_YEAR       MU_REAL(CF_G*UM_YEAR*UM_YEAR*CF_SOLM/(UM_au*UM_au*UM_au))     // G in masse solari, unità astronomiche, anni.
#define CF_G_SOLM_LY_DAY        MU_REAL(CF_G*UM_DAY*UM_DAY*CF_SOLM/(UM_ly*UM_ly*UM_ly)) // G in masse solari, anni luce, giorni.
#define CF_G_GALM_LY_YEAR       MU_REAL(CF_G*(UM_YEAR/UM_ly)*(UM_YEAR/UM_ly)*(CF_GALM/UM_ly))
#define CF_G_GALM_LY_CENTURY    MU_REAL(CF_G*(UM_CENTURY/UM_ly)*(UM_CENTURY/UM_ly)*(CF_GALM/UM_ly))
//--------------------------------------------------------------------

#define umConvAU( x )         (MU_REAL(x)/UM_au)   // Coverte da metri a unità astronomiche.
#define umConvLY( x )         (MU_REAL(x)/UM_ly)   // Coverte da metri ad anni luce.
#define umConvPC( x )         (MU_REAL(x)/UM_pc)   // Coverte da metri a parsec.
#define umConvAng( x )        (MU_REAL(x)/UM_ly)   // Coverte da metri ad Ångstrom.
#define umConvDAY( x )        (MU_REAL(x)/UM_DAY)  // Coverte da secondi a giorni.
#define umConvYEAR( x )       (MU_REAL(x)/UM_YEAR)    // Coverte da secondi ad anni.
#define umConvCENTURY( x )    (MU_REAL(x)/UM_CENTURY)  // Coverte da secondi a secoli.
#define umConvSOLM( x )       (MU_REAL(x)/CF_SOLM) // Coverte da chilogrammi a masse solari.

#endif //_MU_CONSTANTS_
