//
//  GALGalaxyBuilder.mm
//  Galassie
//
//  Created by Nicola Mosco on 10/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import "GALGalaxyBuilder.h"
#import <MatUtils/MUOctree.h>
#import "GALGalaxyDefinition.h"
#import "GALSimulationParameters.h"
#import <MatUtils/MUGeneralia.h>


using namespace gal;
using namespace mu;
using namespace oct;



@implementation GALGalaxyBuilder
{
    gal::GalaxyDefinition*      galaxyDef;
}



- (id)initWithSimulationParameters:(GALSimulationParameters *)someParams
{
    self = [super init];
    
    if (self)
    {
        galaxyDef   = new GalaxyDefinition( someParams );
    }
    
    return self;
}



- (void)dealloc
{
    delete galaxyDef;
}



- (void)generateStateData:(NSMutableData*)stateData
{
    MUReal3*       pos     = (MUReal3*)stateData.mutableBytes;
    MUReal3*       vel     = (MUReal3*)(pos + galaxyDef->numBodies());
    Octree         octree  = Octree( galaxyDef->numBodies(), 2.0 * galaxyDef->extent() );
    
    
    pos[0] = muAssign( 0.0 );
    vel[0] = muAssign( 0.0 );
    
    
    for ( MUSize i = 1; i < galaxyDef->numBodies(); i++ )
    {
        //do
        {
            pos[ i ] = galaxyDef->getPosition();
        }
        //while ( !octree.insertBody( pos[ i ], MU_REAL(1), galaxyDef->lambda() ) );
        
        vel[ i ] = galaxyDef->getVelocity( pos[ i ] );
    }
    
    return;
}



- (void)generateMassData:(NSMutableData *)massData
{
    MUReal*        masses = (MUReal*)massData.mutableBytes;
    
    
    masses[0] = galaxyDef->coreMass();
    
    
    for ( MUSize i = 1; i < galaxyDef->numBodies(); i++ )
    {
        masses[ i ] = galaxyDef->getMass();
    }
    
    return;
}

@end
