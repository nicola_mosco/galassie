//
//  PHYBodyCluster.cpp
//  Physics
//
//  Created by Nicola Mosco on 07/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#include <stdexcept>

#include "PHYBodyCluster.h"


using namespace phy;



BodyCluster::BodyCluster(const ReferenceFrame& aRefFrame,
                         const std::string& name)
    : Cluster{aRefFrame, name}, bodyName_{"Body"}
{}



BodyCluster::~BodyCluster() = default;



const std::string&
BodyCluster::bodyName() const
{
    return bodyName_;
}



std::string&
BodyCluster::bodyName()
{
    return bodyName_;
}



void
BodyCluster::build(Body::ListType& bodies)
{
    this->buildStructure();
    
    if (bodies_.empty())
    {
        throw std::runtime_error("No bodies were created");
    }
    
    bodies.merge(bodies_);
}
