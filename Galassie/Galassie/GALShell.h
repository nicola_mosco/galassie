//
//  GALShell.h
//  Galassie
//
//  Created by Nicola Mosco on 18/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import "GALGeometry.h"


namespace phy
{
    class Shell;
}


@interface GALShell : GALGeometry

- (phy::Shell::PtrType)generate;

@end
