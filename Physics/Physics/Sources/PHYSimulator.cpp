//
//  PHYSimulator.cpp
//  Physics
//
//  Created by Nicola Mosco on 13/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#include "PHYSimulator.h"


using namespace phy;



Simulator::Simulator(const cl_device_type clDevType,
                     const std::string& description)
    : SingleTaskDevice{description},
      notificationInterval_{}, physDevType_{clDevType}
{}



Simulator::~Simulator() = default;



void
Simulator::acquireSystem(System::SPtrType system)
{
    system_ = system;
}



void
Simulator::setNotificationInterval(const Real notificationInterval)
{
    notificationInterval_ = notificationInterval;
}
