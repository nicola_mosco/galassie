//
//  PHYRequest.h
//  Physics
//
//  Created by Nicola Mosco on 15/04/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYRequest__
#define __Physics__PHYRequest__

namespace phy
{
    class Request
    {
    public:
        virtual ~Request() = 0;
        
    public:
        virtual void execute() = 0;
        
        
        
    protected:
        Request() = default;
    };
}

#endif /* defined(__Physics__PHYRequest__) */
