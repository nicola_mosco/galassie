//
//  GALViewState.h
//  Galassie
//
//  Created by Nicola Mosco on 03/04/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGL/OpenGL.h>
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>



extern const NSString* GALBodiesStyle;
extern const NSString* GALBodiesSpheres;
extern const NSString* GALBodiesVertices;



@interface GALViewState : NSObject
{
    NSMutableDictionary*    state;
    NSArray*                properties;
}

@property (strong) NSMutableDictionary* state;


@end
