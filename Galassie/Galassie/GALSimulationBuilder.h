//
//  GALSimulationBuilder.h
//  Galassie
//
//  Created by Nicola Mosco on 31/07/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MatUtils/MUTypes.h>


@class MUSimulator, GALSimulationParameters;


@interface GALSimulationBuilder : NSObject

- (id)initWithSimulationParameters:(GALSimulationParameters*)someParams;

- (void)prepareSimulation:(id)object;

- (MUSimulator*)buildSimulator;

- (void)setup:(cl_device_type)dev_t;

@end
