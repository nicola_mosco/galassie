//
//  MUSimState.h
//  MatUtils
//
//  Created by Nicola Mosco on 22/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <vector>
#import <fstream>
#import <cstdio>
#import <dispatch/dispatch.h>

#import "MUGeneralia.h"


namespace mu
{
    class SimState
    {
    public:
        SimState( const MUSize numBodies );
        SimState( const MUSize numBodies,
                  const MUReal t,
                  const MUReal3* pos,
                  const MUReal3* vel );
        ~SimState();
        
        MUReal      time() const { return t_; }
        
        void        getPos( MUReal3* pos ) const;
        void        getVel( MUReal3* vel ) const;
        
        MUReal3     pos( const MUSize i ) const
        {
            return muReal3( pos_[ i ].x,
                            pos_[ i ].y,
                            pos_[ i ].z );
        }
        
        MUReal3     vel( const MUSize i ) const
        {
            return muReal3( vel_[ i ].x,
                            vel_[ i ].y,
                            vel_[ i ].z );
        }
        
        void        update( const MUReal t,
                            const MUReal3* pos,
                            const MUReal3* vel );
        
        void        readFromFile( FILE* dataFile );
        void        writeToFile( FILE* dataFile );
        
        void        print( const MUSize i = 1, const int precision = 7 ) const;
        
        
        
    private:
        MUSize                  numBodies_;
        MUReal                  t_;
        MUPoint3*               pos_;
        MUPoint3*               vel_;
    };
}
