//
//  GALSplitViewBaseDelegate.h
//  Galassie
//
//  Created by Nicola Mosco on 03/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GALSplitViewBaseDelegate : NSObject <NSSplitViewDelegate>
{
    NSView*     spaceView;
    NSView*     sideBar;
}
@property(strong) IBOutlet NSView* spaceView;
@property(strong) IBOutlet NSView* sideBar;

@end
