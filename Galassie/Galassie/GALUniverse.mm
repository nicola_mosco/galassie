//
//  GALUniverse.mm
//  Galassie
//
//  Created by Nicola Mosco on 17/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import <Physics/PHYUniverse.h>

#import "GALUniverse.h"


@implementation GALUniverse

- (id)initWithName:(NSString *)name
{
    self = [super initWithName:name];
    
    if (self)
    {
        entities = [NSMutableArray array];
        
        type = @"universe";
    }
    
    return self;
}



- (phy::ReferenceFrame)frame
{
    return phy::ReferenceFrame();
}



- (phy::Universe::PtrType)generate
{
    auto universe = new phy::Universe{[self.parameters[@"Name"] UTF8String]};
    
    return phy::Universe::PtrType(universe);
}

@end
