//
//  MUOctree.h
//  MatUtils
//
//  Created by Nicola Mosco on 03/09/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#import <stdexcept>
#import <dispatch/dispatch.h>
#import "MUGeneralia.h"
#import "MUOctreeMemory.h"
#import "MUOctreeUtils.h"

namespace mu
{
    namespace oct
    {
        class Octree : public OctreeMemory
        {
        public:
            Octree( const MUSize numBodies,
                    const MUReal L );
            Octree( const MUSize numBodies,
                    const MUReal3* pos,
                    const MUReal* masses,
                    MUReal3* accel,
                    const MUReal3 center,
                    const MUReal G = 0.0,
                    const MUReal eps = 0.0,
                    const MUReal theta = 0.0 );
            ~Octree();
            
            
        public:
            void            setupHeader(const MUReal G,
                                        const MUReal eps,
                                        const MUReal theta);
            
            OctHeader       header();
            OctNode         rootNode();
            
            OctNode         nodeForIndex(const MURef nodeIndex);
            OctNode         nodeForOffset(const MURef nodeOffset);
            OctNode         branchForCell(OctNode node,
                                          const MUCell cell);
            
            MURef           nodeOffsetForIndex(const MURef nodeIndex);
            
            MUReal&         extension();
            
            const
            MUReal3*        pos() const;
            const
            MUReal*         masses() const;
            MUReal3*        accel( void );
            
            MUBool          insertBody(const MUReal3 pos,
                                       const MUReal mass,
                                       const MUReal lambda = MU_REAL(0));
            
            void            update(const MUReal3* pos,
                                   const MUReal* masses,
                                   MUReal3* accel );
            
            void            computeAccel();
            void            operator()();
            void            operator()(const MUReal3* pos,
                                       MUReal3* accel);
            
            
            
        private:
            OctHeader       getHeader();
            OctNode         getRoot();
            
            void            reset();
            void            setup();
            void            update();
            
            void            computeExtension();
            
            OctNode         allocNewNode(MURef *const nodeOffsetPtr);
            
            MUCell          findCell(const OctNode node,
                                     const MUReal3 pos,
                                     MUReal *const LPtr,
                                     MUReal3 *const centerPtr);
            MUBool          insertNode(MURef* nodeOffsetPtr,
                                       const MURef parentOffset,
                                       const MUReal L,
                                       const MUReal3 center,
                                       const MUReal3 pos,
                                       const MUReal mass,
                                       const MUReal lambda = MU_REAL(0));
            MUBool          insertNodeFindingCell(MURef* nodeOffsetPtr,
                                                  const MUReal3 pos,
                                                  const MUReal mass,
                                                  const MUReal lambda = MU_REAL(0));
            
            void            build();
            
            MUBool          nodeIsEmpty(const MURef nodeOffset);
            MUBool          nodeIsLeaf(const MURef nodeOffset);
            MUBool          nodeIsLeaf(const OctNode node);
            
            void            nodeCenterOfMass(OctNode node);
            
            void            setBranchCell(OctNode node,
                                          const MUCell cell);
            
        private:
            const
            MUReal3*            _pos;
            const
            MUReal*             _masses;
            MUReal3*            _accel;
            OctHeader           _header;
            OctNode             _root;
            dispatch_queue_t    queue_;
        };
    }
}
