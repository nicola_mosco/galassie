//
//  PHYDeviceDataImplementation.h
//  Physics
//
//  Created by Nicola Mosco on 18/04/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYDeviceDataImplementation__
#define __Physics__PHYDeviceDataImplementation__

namespace phy
{
    class Module;
    
    
    class DeviceDataImplementation
    {
    public:
        virtual ~DeviceDataImplementation() = 0;
        
        
        
    private:
        friend class Module;
        
    private:
        DeviceDataImplementation() = delete;
    };
}

#endif /* defined(__Physics__PHYDeviceDataImplementation__) */
