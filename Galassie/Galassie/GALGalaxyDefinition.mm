//
//  GALGalaxyDefinition.mm
//  Galassie
//
//  Created by Nicola Mosco on 03/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import <MatUtils/MUMathFunc.h>
#import <MatUtils/MUGeneralia.h>
#import "GALGalaxyDefinition.h"
#import <map>
#import <stdexcept>


using namespace std;
using namespace mu;
using namespace dist;
using namespace gal;



GalaxyDefinition::GalaxyDefinition( GALSimulationParameters* someParams,
                                    const string& massDistName,
                                    const string& spatialDistName ) : numBodies_(0), G_(0.0), coreMass_(0.0),
                                                                      meanMass_(0.0), massSpread_(0.0),
                                                                      extent_(0.0), bulge_(0.0), coreRadius_(0.0)
{
    Random::setup();
    
    massDist_    = Distribution::newByName( massDistName );
    spatialDist_ = Distribution::newByName( spatialDistName );
    
    phiDist_     = new Uniform( 0.0, 2.0 * M_PI );
    thetaDist_   = new Uniform( M_PI * 0.3, M_PI * 0.7 );
    
    setup( someParams );
}



GalaxyDefinition::~GalaxyDefinition( void )
{
    delete massDist_;
    delete spatialDist_;
}



void GalaxyDefinition::setup( GALSimulationParameters* someParams )
{
    this->numBodies()   = someParams.numBodiesGal;
    this->G()           = someParams.G;
    this->coreMass()    = someParams.coreMass;
    this->meanMass()    = someParams.meanMass;
    this->massSpread()  = someParams.massSpread;
    this->bulge()       = someParams.bulge;
    this->extent()      = someParams.extent;
    this->coreRadius()  = someParams.coreRadius;
    this->lambda()      = someParams.lambda;
    
    setupDistributions();
    
    return;
}



void GalaxyDefinition::setupDistributions( void )
{
    if ( coreMass_ <= 0.0 ||
         meanMass_ <= 0.0 || massSpread_ <= 0.0 ||
         extent_   <= 0.0 || coreRadius_ <= 0.0 )
    {
        throw domain_error( "Negative parameters" );
    }
    
    
    map<string, MUReal> massParams;
    map<string, MUReal> spatialParams;
    
    
    if ( massDist_->name().compare( uniform ) == 0 )
    {
        if ( meanMass_ - massSpread_ * MU_REAL(0.5) <= 0.0 )
        {
            throw domain_error( "Negative mass" );
        }
        
        massParams[ "a" ] = meanMass_ - massSpread_ * MU_REAL(0.5); // a.
        massParams[ "b" ] = meanMass_ + massSpread_ * MU_REAL(0.5); // b.
    }
    else if ( massDist_->name().compare( gaussianTail ) == 0 )
    {
        massParams[ "mu" ]    = meanMass_; // mu.
        massParams[ "sigma" ] = massSpread_; // sigma.
        massParams[ "a" ]     = 0.0; // a.
    }
    else
    {
        throw logic_error( "Unsupported distribution" );
    }
    
    
    if ( spatialDist_->name().compare( uniform ) == 0 )
    {
        spatialParams[ "a" ] = coreRadius_; // a.
        spatialParams[ "b" ] = extent_; // b.
    }
    else if ( spatialDist_->name().compare( rayleighTail ) == 0 )
    {
        spatialParams[ "sigma" ] = bulge_; // sigma.
        spatialParams[ "a" ]     = coreRadius_; // a.
    }
    else
    {
        throw logic_error( "Unsupported distribution" );
    }
    
    
    massDist_->setParameters( massParams );
    spatialDist_->setParameters( spatialParams );
    
    return;
}



MUReal GalaxyDefinition::getMass( void )
{
    return massDist_->get();
}



MUReal3 GalaxyDefinition::getPosition( void )
{
    MUReal3     pos = { 0.0 };
    MUReal      r;
    MUReal      phi;
    MUReal      theta;
    MUReal      rst;
    
    do
    {
        r = spatialDist_->get();
    }
    while ( r > extent_ );
    
    phi   = phiDist_->get();
    theta = thetaDist_->get();
    
    rst   = r * Sin( theta );
    
    pos   = muReal3( rst * Cos( phi ),
                     rst * Sin( phi ),
                     r * Cos( theta ) );
    
    return pos;
}



MUReal3 GalaxyDefinition::getVelocity( const MUReal3 pos )
{
    MUReal3      vel = { 0.0 };
    MUReal       r   = muNorm2( pos );
    MUReal       v;
    MUReal       f;
    
    v   =  Sqrt( G_ * coreMass_ / r );
    f   =  v / r;
    
    vel = muReal3( -pos.s[1] * f, pos.s[0] * f, 0.0 );
    
    return vel;
}
