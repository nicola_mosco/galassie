//
//  PHYShell.cpp
//  Physics
//
//  Created by Nicola Mosco on 06/01/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#include <dispatch/dispatch.h>
#include <random>
#include <cmath>

#include "PHYShell.h"


using namespace phy;



Shell::Shell(const ReferenceFrame& refFrame,
             const std::string& name)
    : BodyCluster{refFrame, name},
      numBodies_{},
      extRadius_{},
      intRadius_{},
      meanRadius_{},
      radialSpread_{},
      theta_{},
      meanMass_{},
      massSpread_{},
      minMass_{},
      gravFactor_{}
{
    bodyName() = "Star";
}



const Size
Shell::numBodies() const
{
    return numBodies_;
}



Size&
Shell::numBodies()
{
    return numBodies_;
}



const Real
Shell::externalRadius() const
{
    return extRadius_;
}



Real&
Shell::externalRadius()
{
    return extRadius_;
}



const Real
Shell::internalRadius() const
{
    return intRadius_;
}



Real&
Shell::internalRadius()
{
    return intRadius_;
}



const Real
Shell::meanRadius() const
{
    return meanRadius_;
}



Real&
Shell::meanRadius()
{
    return meanRadius_;
}



const Real
Shell::radialSpread() const
{
    return radialSpread_;
}



Real&
Shell::radialSpread()
{
    return radialSpread_;
}



const Real
Shell::theta() const
{
    return theta_;
}



Real&
Shell::theta()
{
    return theta_;
}



const Real
Shell::meanMass() const
{
    return meanMass_;
}



Real&
Shell::meanMass()
{
    return meanMass_;
}



const Real
Shell::massSpread() const
{
    return massSpread_;
}



Real&
Shell::massSpread()
{
    return massSpread_;
}



const Real
Shell::minMass() const
{
    return minMass_;
}



Real&
Shell::minMass()
{
    return minMass_;
}



const Real
Shell::gravFactor() const
{
    return gravFactor_;
}



Real&
Shell::gravFactor()
{
    return gravFactor_;
}



void
Shell::buildStructure()
{
    auto queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    auto nIter = numBodies_ / PHY_NUM_THREADS;
    
    dispatch_apply(PHY_NUM_THREADS, queue,
                   ^(Size g)
                   {
                       std::random_device gen;
                       std::normal_distribution<Real> radial_dist{meanRadius_, radialSpread_};
                       std::uniform_real_distribution<Real> theta_dist{theta_, std::acos(Real(-1)) - theta_};
                       std::uniform_real_distribution<Real> phi_dist{Real(0), Real(2) * std::acos(Real(-1))};
                       std::normal_distribution<Real> mass_dist{meanMass_, massSpread_};
                       std::uniform_real_distribution<Real> energy_dist{Real(1), Real(2)};
                       
                       Real r{};
                       Real theta{};
                       Real phi{};
                       Real mass{};
                       
                       auto ini = g * nIter;
                       auto fin = (g == PHY_NUM_THREADS - 1 ? numBodies_ : (g + 1) * nIter);
                       
                       for (auto i = ini; i < fin; ++i)
                       {
                           do
                           {
                               r = radial_dist(gen);
                           }
                           while (r < intRadius_ || r > extRadius_);
                           
                           do
                           {
                               mass = mass_dist(gen);
                           }
                           while (mass < minMass_);
                           
                           theta = theta_dist(gen);
                           phi   = phi_dist(gen);
                           
                           Vector3 pos = {r * std::sin(theta) * std::cos(phi),
                                          r * std::sin(theta) * std::sin(phi),
                                          r * std::cos(theta)};
                           Affine3 transf;
                           
                           Real  v   = std::sqrt(energy_dist(gen) * gravFactor_ / pos.norm());
                           RVec3 vel = {v * std::cos(phi + std::asin(Real(1))),
                                        v * std::sin(phi + std::asin(Real(1))),
                                        Real(0)};
                           
                           transf = Translation3{pos};
                           
                           Body::PtrType body{new Body{ReferenceFrame{transf, vel}, bodyName()}};
                           
                           body->mass() = mass;
                           
                           bodies_.push_back(std::move(body));
                       }
                   });
}
