//
//  GALAxises.m
//  Galassie
//
//  Created by Nicola Mosco on 04/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import "GALAxises.h"

@implementation GALAxises

@synthesize height, base, slices, stacks;



- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.height = 0.05;
        self.base   = 0.03;
        
        self.slices = 100;
        self.stacks = 100;
        
        arrow = gluNewQuadric();
        
        gluQuadricDrawStyle( arrow, GLU_FILL );
        gluQuadricNormals( arrow, GLU_SMOOTH );
        gluQuadricOrientation( arrow, GLU_OUTSIDE );
    }
    
    return self;
}



- (void)dealloc
{
    gluDeleteQuadric( arrow );
}



- (void)draw
{
    [super draw];
    
    glBegin( GL_LINES );
    {
        glVertex3f( -1.0, 0.0, 0.0 );
        glVertex3f( 1.0, 0.0, 0.0 );
        
        glVertex3f( 0.0, -1.0, 0.0 );
        glVertex3f( 0.0, 1.0, 0.0 );
        
        glVertex3f( 0.0, 0.0, -1.0 );
        glVertex3f( 0.0, 0.0, 1.0 );
    }
    glEnd();
    
    glPushMatrix();
    {
        glTranslatef( 1.0 - height, 0.0, 0.0 );
        glRotatef( 90.0, 0.0, 1.0, 0.0 );
        
        gluCylinder( arrow, base, 0.0, height, slices, stacks );
    }
    glPopMatrix();
    
    glPushMatrix();
    {
        glTranslatef( 0.0, 1.0 - height, 0.0 );
        glRotatef( -90.0, 1.0, 0.0, 0.0 );
        
        gluCylinder( arrow, base, 0.0, height, slices, stacks );
    }
    glPopMatrix();
    
    glPushMatrix();
    {
        glTranslatef( 0.0, 0.0, 1.0 - height );
        
        gluCylinder( arrow, base, 0.0, height, slices, stacks );
    }
    glPopMatrix();
}

@end
