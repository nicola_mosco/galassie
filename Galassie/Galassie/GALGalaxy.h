//
//  GALGalaxy.h
//  Galassie
//
//  Created by Nicola Mosco on 17/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import "GALGeometry.h"


namespace phy
{
    class Galaxy;
}


@interface GALGalaxy : GALGeometry

- (phy::Galaxy::PtrType)generate;

@end
