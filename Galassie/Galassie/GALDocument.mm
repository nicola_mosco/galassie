//
//  GALDocument.mm
//  Galassie
//
//  Created by Nicola Mosco on 03/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import "GALDocument.h"
#import "GALSpaceView.h"
#import "GALSimulationParameters.h"
#import "GALSimulationBuilder.h"
#import "GALDocumentRepresentation.h"
#import <MatUtils/MUSimulator.h>
#import <MatUtils/MUSystem.h>
#import <MatUtils/MUIntegrator.h>
#import <fstream>
#import <iostream>
#import <iomanip>
#import <MatUtils/MUSimState.h>


using namespace std;
using namespace mu;



@implementation GALDocument
{
    GALDocumentRepresentation*  docRep;
    
    GALSimulationBuilder*       simBuilder;
    
    NSInvocationOperation*      simBuildOp;
    NSOperationQueue*           simBuildQueue;
    
    NSInvocationOperation*      simRunOp;
    NSOperationQueue*           simRunQueue;
    
    NSMutableData*              visPosData;
    NSMutableData*              visVelData;
    MUReal3*                    visPos;
    MUReal3*                    visVel;
    
    NSMutableData*              enData;
    double*                     energies;
    double*                     times;
    NSUInteger                  lines;
    
    NSDate*                     simBegin;
    
    FILE*                       visDataFile;
    SimState*                   simState;
    
    NSTimer*                    timer;
}



- (id)init
{
    self = [super init];
    
    if (self)
    {
        inspDividerIndex = 0;
        defInspWidth     = 150.0f;
        maxInspWidth     = 300.0f;
        minInspWidth     = 60.0f;
        lastWidth        = defInspWidth;
        
        self.simTimeLeft = 0.0;
        self.simRunning  = NO;
        
        simBuilder       = nil;
        simulator        = nil;
        
        simBuildOp       = nil;
        simBuildQueue    = [[NSOperationQueue alloc] init];
        
        [simBuildQueue addObserver: self
                        forKeyPath: @"operationCount"
                           options: NSKeyValueObservingOptionNew
                           context: NULL];
        
        simRunOp         = nil;
        simRunQueue      = [[NSOperationQueue alloc] init];
        
        [simRunQueue addObserver: self
                      forKeyPath: @"operationCount"
                         options: NSKeyValueObservingOptionNew
                         context: NULL];
        
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(updateSimulationTime:)
                                                     name: @"MUTimeUpdateNotification"
                                                   object: nil];
        
        visPosData       = nil;
        visVelData       = nil;
        visPos           = NULL;
        visVel           = NULL;
        
        simBegin         = nil;
        timer            = nil;
        
        visDataFile      = NULL;
        simState         = NULL;
        
        docRep = [[GALDocumentRepresentation alloc] initWithDocument: self];
    }
    
    return self;
}



- (NSString *)windowNibName
{
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"GALDocument";
}



- (void)windowControllerDidLoadNib:(NSWindowController *)aController
{
    [super windowControllerDidLoadNib: aController];
    
    NSDateFormatter* dateForm = [self.timeLeftField formatter];
    
    dateForm.timeZone = [NSTimeZone timeZoneWithAbbreviation: @"UTC"];
}



#if 0
- (NSFileWrapper *)fileWrapperOfType:(NSString *)typeName
                               error:(NSError *__autoreleasing *)outError
{
    NSFileWrapper*  mainDir = nil;
    NSFileWrapper*  initialStateDir = nil;
    NSData*         posData = nil;
    NSData*         velData = nil;
    NSData*         massData = nil;
    
    mainDir = [[NSFileWrapper alloc] initDirectoryWithFileWrappers: nil];
    initialStateDir = [[NSFileWrapper alloc] initDirectoryWithFileWrappers: nil];
    
    [initialStateDir setPreferredFilename: @"Initial state data"];
    
    posData = [[NSData alloc] initWithBytesNoCopy: (MUPtr)simulator.system->pos()
                                           length: simulator.system->numBodies() * MU_SIZE_REAL3
                                     freeWhenDone: NO];
    velData = [[NSData alloc] initWithBytesNoCopy: (MUPtr)simulator.system->vel()
                                           length: simulator.system->numBodies() * MU_SIZE_REAL3
                                     freeWhenDone: NO];
    massData = [[NSData alloc] initWithBytesNoCopy: (MUPtr)simulator.system->masses()
                                            length: simulator.system->numBodies() * MU_SIZE_REAL
                                      freeWhenDone: NO];
    
    [initialStateDir addRegularFileWithContents: posData
                              preferredFilename: @"posData"];
    [initialStateDir addRegularFileWithContents: velData
                              preferredFilename: @"velData"];
    [initialStateDir addRegularFileWithContents: massData
                              preferredFilename: @"massData"];
    
    [mainDir addFileWrapper: initialStateDir];
    
    return mainDir;
}
#endif



-   (BOOL)writeToURL:(NSURL *)url
              ofType:(NSString *)typeName
    forSaveOperation:(NSSaveOperationType)saveOperation
 originalContentsURL:(NSURL *)absoluteOriginalContentsURL
               error:(NSError *__autoreleasing *)outError
{
    BOOL flag = YES;
    
    flag = [docRep buildStructureAtURL:url
                             withError:outError];
    
    return flag;
}



- (BOOL)readFromURL:(NSURL *)url
             ofType:(NSString *)typeName
              error:(NSError *__autoreleasing *)outError
{
    BOOL flag = YES;
    
    
    
    return flag;
}



+ (BOOL)autosavesInPlace
{
    return NO;
}



- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if ( object == simBuildQueue && [keyPath isEqualToString: @"operationCount"] )
    {
        if ( simBuildQueue.operationCount == 0 )
        {
            [self.indicator stopAnimation: self];
            [self.indicator setIndeterminate: NO];
            
            simulator = nil;
            simulator = [simBuilder buildSimulator];
            
            [_spaceView updateSceneWithParameters: _simParams
                                    positionsData: [simulator.system->posData() copy]
                                   velocitiesData: [simulator.system->velData() copy]];
            
            simBuildOp = nil;
        }
    }
    else if ( object == simRunQueue && [keyPath isEqualToString: @"operationCount"] )
    {
        if ( simRunQueue.operationCount == 0 )
        {
            [self.indicator setHidden: YES];
            self.simRunning = NO;
            
            [_spaceView updateSceneWithParameters: _simParams
                                    positionsData: simulator.system->posData()
                                   velocitiesData: simulator.system->velData()];
            
            //[self drawEnergy];
            
            simRunOp = nil;
        }
    }
    
    return;
}



- (void)updateSimulationTime:(NSNotification*)notif
{
    NSTimeInterval simDelta = [[NSDate date] timeIntervalSinceDate: simBegin];
    
    self.visTime = [[notif.userInfo objectForKey: @"Time"] doubleValue];
    
    MUReal simPerc = ( _visTime - _simParams.t_i ) / (_simParams.t_f - _simParams.t_i);
    
    self.indicator.doubleValue = simPerc;
    
    self.simTimeLeft = simDelta / simPerc - simDelta;
    
    MUReal timeLeft  = self.simTimeLeft / 3600.0;
    MUReal hours     = floor(timeLeft);
    MUReal minutes   = floor((timeLeft - hours) * 60.0);
    MUReal seconds   = (timeLeft - hours) * 3600.0 - minutes * 60.0;
    
    self.timeLeftRep = [NSString stringWithFormat:
                        @"%02d:%02d:%05.2f", int(hours), int(minutes), seconds];
}



- (IBAction)buildSimulation:(id)sender
{
    simulator     = nil;
    simBuilder    = [[GALSimulationBuilder alloc] initWithSimulationParameters: _simParams];
    
    simBuildOp    = [[NSInvocationOperation alloc] initWithTarget: simBuilder
                                                         selector: @selector( prepareSimulation: )
                                                           object: nil];
    
    [self.indicator setHidden: NO];
    [self.indicator setIndeterminate: YES];
    [self.indicator setNeedsDisplay: YES];
    [self.indicator startAnimation: self];
    
    [simBuildQueue addOperation: simBuildOp];
}



- (IBAction)runSimulation:(id)sender
{
    simRunOp = [[NSInvocationOperation alloc] initWithTarget: simulator
                                                    selector: @selector( simulate: )
                                                      object: self];
    
    [self.indicator setHidden: NO];
    [self.indicator setIndeterminate: NO];
    [self.indicator setNeedsDisplay: YES];
    [self.indicator setDoubleValue: 0.0];
    
    self.simRunning  = YES;
    self.simTimeLeft = 0.0;
    
    simBegin = [NSDate date];
    
    [simRunQueue addOperation: simRunOp];
}



- (IBAction)centerFrameOrigin:(id)sender
{
    [_spaceView centerFrameOrigin];
}



- (IBAction)startVisualization:(id)sender
{
    [self prepareVisualization];
    
    timer = [NSTimer scheduledTimerWithTimeInterval: 0.01
                                             target: self
                                           selector: @selector( updateScene: )
                                           userInfo: nil
                                            repeats: YES];
}



- (IBAction)stopVisualization:(id)sender
{
    [self resetVisualization];
}



- (void)prepareVisualization
{
    if (!visDataFile)
    {
        [self resetVisualization];
    }
    
    visDataFile = fopen( "dataFile.dat", "rb" );
    simState    = new SimState( _simParams.numBodies );
    
    visPosData  = [[NSMutableData alloc] initWithLength: _simParams.numBodies * MU_SIZE_REAL3];
    visVelData  = [[NSMutableData alloc] initWithLength: _simParams.numBodies * MU_SIZE_REAL3];
    
    visPos      = (MUReal3*)visPosData.mutableBytes;
    visVel      = (MUReal3*)visVelData.mutableBytes;
}



- (void)updateScene:(NSTimer *)theTimer
{
    simState->readFromFile( visDataFile );
    
    if ( feof( visDataFile ) )
    {
        [self resetVisualization];
        
        return;
    }
    
    self.visTime = simState->time();
    
    simState->getPos( visPos );
    simState->getVel( visVel );
    
    [_spaceView updateSceneWithParameters: _simParams
                            positionsData: visPosData
                           velocitiesData: visVelData];
    
    [_spaceView setNeedsDisplay: YES];
}



- (void)resetVisualization
{
    [timer invalidate];
    
    fclose( visDataFile );
    visDataFile = NULL;
    
    delete simState;
    simState = NULL;
    
    visPosData = nil;
    visVelData = nil;
}



- (NSUInteger)computeEnergy
{
    FILE* energyFile = fopen( "energyFile.dat", "wb" );
    FILE* dataFile   = fopen( "dataFile.dat", "rb" );
    NSUInteger count = 0;
    double time, energy, samp = 0.0;
    MUReal3* pos = new MUReal3[ _simParams.numBodies ];
    MUReal3* vel = new MUReal3[ _simParams.numBodies ];
    
    simState = new SimState( _simParams.numBodies );
    
    
    do
    {
        simState->readFromFile( dataFile );
        
        time = simState->time();
        
        if ( time - samp >= _simParams.sampleIntv )
        {
            samp = time;
            
            simState->getPos( pos );
            simState->getVel( vel );
            
            energy = simulator.interaction->energy( _simParams.numBodies, pos, vel, simulator.system->masses() );
            
            fwrite( &time, 1, sizeof(double), energyFile );
            fwrite( &energy, 1, sizeof(double), energyFile );
            
            ++count;
        }
    }
    while ( !feof( dataFile ) );
    
    
    delete[] pos;
    delete[] vel;
    delete simState;
    fclose( energyFile );
    fclose( dataFile );
    
    return --count;
}



- (void)drawEnergy
{
    lines = [self computeEnergy];
    
    enData = [[NSMutableData alloc] initWithContentsOfFile: @"energyFile.dat"];
    
    CPTXYGraph* graph = [[CPTXYGraph alloc] init];
    
    self.hostingView.hostedGraph = graph;
    
    CPTXYPlotSpace* plotSpace = (CPTXYPlotSpace*)graph.defaultPlotSpace;
    
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation: CPTDecimalFromDouble( -0.25 )
                                                    length: CPTDecimalFromDouble( _simParams.t_f )];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation: CPTDecimalFromDouble( -0.001 )
                                                    length: CPTDecimalFromDouble( 0.001 )];
    
    CPTScatterPlot* plot = [[CPTScatterPlot alloc] init];
    
    plot.dataSource = self;
    
    [graph addPlot: plot];
    
    graph.title = @"Energy";
    
    [graph applyTheme: [CPTTheme themeNamed: kCPTSlateTheme]];
}



- (NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    return lines;
}



- (double)doubleForPlot:(CPTPlot *)plot
                  field:(NSUInteger)fieldEnum
            recordIndex:(NSUInteger)index
{
    double num = 0.0;
    
    if ( fieldEnum == CPTScatterPlotFieldX )
    {
        num = *(((double*)enData.mutableBytes) + index);
    }
    else if ( fieldEnum == CPTScatterPlotFieldY )
    {
        num = *(((double*)enData.mutableBytes) + index + 1);
    }
    
    return num;
}

@end