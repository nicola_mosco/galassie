//
//  PHYReferenceFrame.cpp
//  Physics
//
//  Created by Nicola Mosco on 06/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//  

#include "PHYReferenceFrame.h"



using namespace phy;



ReferenceFrame::ReferenceFrame(const Affine3& transf,
                               const RVec3& vel)
    : transf_{transf}, vel_{vel.s[0], vel.s[1], vel.s[2]}
{}



const Affine3&
ReferenceFrame::transf() const
{
    return transf_;
}



RVec3
ReferenceFrame::vel() const
{
    return {vel_.x(), vel_.y(), vel_.z()};
}



void
ReferenceFrame::transformTo(const ReferenceFrame& aRefFrame)
{
    transf_ = aRefFrame.transf() * transf_;
    vel_    = aRefFrame.transf().rotation() * vel_;
}



RVec3
ReferenceFrame::operator()(const RVec3& point) const
{
    Vector3 p;
    
    p = transf_ * Vector3{point.s[0], point.s[1], point.s[2]};
    
    return {p.x(), p.y(), p.z()};
}
