//
//  PHYShellGalaxy.h
//  Physics
//
//  Created by Nicola Mosco on 06/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYShellGalaxy__
#define __Physics__PHYShellGalaxy__

#include "PHYGalaxy.h"
#include "PHYShell.h"
#include "PHYCore.h"


namespace phy
{
    class ShellGalaxy : public Galaxy
    {
    public:
        ShellGalaxy(const ReferenceFrame& aRefFrame = ReferenceFrame{},
                    const std::string& name = "OpenShellGalaxy");
        virtual ~ShellGalaxy() = default;
        
    public:
        void setCore(Core::PtrType core);
        void setBulge(Shell::PtrType bulge);
        void setShell(Shell::PtrType shell);
        
        
        
    private:
        BodyCluster::ListType::iterator core_it_;
        BodyCluster::ListType::iterator bulge_it_;
        BodyCluster::ListType::iterator shell_it_;
    };
}

#endif /* defined(__Physics__PHYShellGalaxy__) */
