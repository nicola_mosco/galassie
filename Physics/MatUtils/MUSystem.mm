//
//  MUSystem.mm
//  MatUtils
//
//  Created by Nicola Mosco on 08/05/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#import "MUSystem.h"
#import "MUGeneralia.h"


using namespace std;
using namespace mu;



System::System( const MUSize numBodies,
                NSMutableData* posData,
                NSMutableData* velData,
                NSMutableData* massesData ) : numBodies_( numBodies )
{
    posData_    = posData;
    velData_    = velData;
    massesData_ = massesData;
    
    pos_        = (MUReal3*)posData_.mutableBytes;
    vel_        = (MUReal3*)velData_.mutableBytes;
    masses_     = (MUReal*)massesData_.mutableBytes;
    
    computeTotalMass();
}



void System::computeTotalMass()
{
    mass_ = 0.0;
    
    for ( MUSize i = 0; i < numBodies_; i++ )
    {
        mass_ += masses_[ i ];
    }
}



MUReal3 System::cmPos() const
{
    MUReal3 cmp    = { 0.0 };
    
    for ( MUSize i = 0; i < numBodies_; i++ )
    {
        cmp = muSum( cmp, muScale( pos_[ i ], masses_[ i ] ) );
    }
    
    return muInvScale( cmp, mass_ );
}



MUReal3 System::cmVel() const
{
    MUReal3 cmv    = { 0.0 };
    
    for ( MUSize i = 0; i < numBodies_; i++ )
    {
        cmv = muSum( cmv, muScale( vel_[ i ], masses_[ i ] ) );
    }
    
    return muInvScale( cmv, mass_ );
}
