//
//  GALAppDelegate.h
//  Galassie
//
//  Created by Nicola Mosco on 18/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>


@class GALDocument, GALSimulationParameters, GALWelcomeController;


@interface GALAppDelegate : NSObject <NSApplicationDelegate, NSTableViewDataSource, NSTableViewDelegate>

@property (strong)  NSWindowController*                 welcomeController;
@property (weak)    IBOutlet NSTableView*               paramTable;
@property (weak)    IBOutlet GALSimulationParameters*   simParams;

@end
