//
//  MUDistributions.h
//  MatUtils
//
//  Created by Nicola Mosco on 02/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import <string>
#import <map>
#import "MURandom.h"


namespace mu
{
    namespace dist
    {
        extern const std::string uniform;
        extern const std::string gaussianTail;
        extern const std::string rayleighTail;
        
        
        /*********************************************************************/
        
        
        class Distribution : public Random
        {
        public:
            static
            Distribution*       newByName( const std::string& name );
            
        public:
            Distribution( const std::string& name,
                          const gsl_rng_type* type = gsl_rng_taus ) : Random( type ), name_( name ) {}
            virtual
            ~Distribution( void ) {}
            
            virtual
            MUReal              get( void ) = 0;
            
            virtual
            void                setParameters( const std::map<std::string, MUReal>& params ) = 0;
            
            const
            std::string&        name( void ) const { return name_; }
            
            
        private:
            std::string         name_;
        };
        
        
        /*********************************************************************/
        
        
        class Uniform : public Distribution
        {
        public:
            Uniform( const MUReal a = MU_REAL(0),
                     const MUReal b = MU_REAL(1) ) : Distribution( "Uniform" ),
                                                     a_( a ), b_( b )
            {}
            ~Uniform( void ) {}
            
            MUReal get( void );
            
            void setParameters( const std::map<std::string, MUReal>& params );
            
            MUReal& a( void ) { return a_; }
            MUReal& b( void ) { return b_; }
            
            
        private:
            MUReal           a_;
            MUReal           b_;
        };
        
        
        /*********************************************************************/
        
        
        class GaussianTail : public Distribution
        {
        public:
            GaussianTail( const MUReal mu = MU_REAL(0),
                          const MUReal sigma = MU_REAL(1),
                          const MUReal a = MU_REAL(0) ) : Distribution( "Gaussian Tail" ),
                                                          mu_( mu ), sigma_( sigma ),
                                                          a_( a )
            {}
            ~GaussianTail( void ) {}
            
            MUReal get( void );
            
            void setParameters( const std::map<std::string, MUReal>& params );
            
            MUReal& mu( void ) { return mu_; }
            MUReal& sigma( void ) { return sigma_; }
            MUReal& a( void ) { return a_; }
            
            
        private:
            MUReal           mu_;
            MUReal           sigma_;
            MUReal           a_;
        };
        
        
        /*********************************************************************/
        
        
        class RayleighTail : public Distribution
        {
        public:
            RayleighTail( const MUReal a = MU_REAL(0),
                          const MUReal sigma = MU_REAL(1) ) : Distribution( "Rayleigh Tail" ),
                                                              sigma_( sigma ), a_( a )
            {}
            ~RayleighTail( void ) {}
            
            MUReal get( void );
            
            void setParameters( const std::map<std::string, MUReal>& params );
            
            MUReal& a( void ) { return a_; }
            MUReal& sigma( void ) { return sigma_; }
            
            
        private:
            MUReal           a_;
            MUReal           sigma_;
        };
    }
}
