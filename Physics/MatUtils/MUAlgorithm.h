//
//  MUAlgorithm.h
//  MatUtils
//
//  Created by Nicola Mosco on 12/05/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#import <string>
#import <memory>

#import "MUTypes.h"


namespace mu
{
    class Integrator;
    
    class Algorithm
    {
    public:
        typedef std::shared_ptr<Algorithm> AlgorithmPtr;
        
    public:
        Algorithm( const std::string& name,
                   Integrator* integrator ) : name_( name ), integrator_( integrator ) {}
        virtual
        ~Algorithm() {}
        
        const
        std::string&        name() const { return name_; }
        
        virtual
        void                operator()( const MUReal t,
                                        MUReal3* pos,
                                        MUReal3* vel ) = 0;
        
        
        
    protected:
        Integrator*         integrator_;
        
        
        
    private:
        std::string         name_;
    };
}
