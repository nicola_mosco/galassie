//
//  GALGeometry.mm
//  Galassie
//
//  Created by Nicola Mosco on 17/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import <Physics/PHYAlgebra.h>
#import <Physics/PHYReferenceFrame.h>
#import <Physics/PHYGeometry.h>

#import "NSNumber+GALNumberExtension.h"
#import "GALGeometry.h"
#import "GALVector3.h"


@implementation GALGeometry

@synthesize type;



- (id)initWithName:(NSString *)name
{
    self = [super init];
    
    if (self)
    {
        self.parameters = [NSMutableDictionary dictionaryWithObject:name
                                                             forKey:@"Name"];
        
        type     = @"geometrical_entity";
        entities = nil;
    }
    
    return self;
}



- (phy::ReferenceFrame)frame
{
    phy::Translation3 trasl{[self.parameters[@"Position"] vector3]};
    phy::Rotation3 rot{[(NSNumber*)self.parameters[@"Angle"] value], [self.parameters[@"Axis"] vector3]};
    
    return phy::ReferenceFrame(trasl * rot, [self.parameters[@"Velocity"] realVector3]);
}



- (NSString *)description
{
    return self.parameters[@"Name"];
}



- (void)addEntity:(GALGeometry *)entity
{
    [entities addObject:entity];
}

@end
