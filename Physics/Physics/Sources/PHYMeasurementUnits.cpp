//
//  PHYMeasureUnits.cpp
//  Physics
//
//  Created by Nicola Mosco on 07/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#include "PHYConstants.h"


using namespace phy;



const Real m_y  = 1e-24;
const Real m_z  = 1e-21;
const Real m_a  = 1e-18;
const Real m_f  = 1e-15;
const Real m_p  = 1e-12;
const Real m_n  = 1e-9;
const Real m_mu = 1e-6;
const Real m_m  = 1e-3;
const Real m_c  = 1e-2;
const Real m_d  = 1e-1;
const Real m_da = 1e1;
const Real m_h  = 1e2;
const Real m_k  = 1e3;
const Real m_M  = 1e6;
const Real m_G  = 1e9;
const Real m_T  = 1e12;
const Real m_P  = 1e15;
const Real m_E  = 1e18;
const Real m_Z  = 1e21;
const Real m_Y  = 1e24;



const Real u_s  = 1.0;
const Real u_y  = 3.15581500224e7 * u_s;
const Real u_c  = 3.15581503e9 * u_s;
const Real u_gy = 2.25e2 * m_M * u_y * u_s;



const Real u_m  = 1.0;
const Real u_au = 1.49597870e11 * u_m;
const Real u_ly = 9.46073047e15 * u_m;
const Real u_pc = 3.08567758066631e16 * u_m;



const Real u_kg = 1.0;
