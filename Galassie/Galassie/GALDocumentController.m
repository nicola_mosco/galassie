//
//  GALDocumentController.m
//  Galassie
//
//  Created by Nicola Mosco on 04/01/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import "GALDocumentController.h"
#import "GALAppDelegate.h"


@implementation GALDocumentController

- (void)newDocument:(id)sender
{
    [[[NSApp delegate] welcomeController] close];
    
    [super newDocument: sender];
    
    [[self currentDocument] saveDocumentWithDelegate:self
                                     didSaveSelector:@selector(document:didSave:contextInfo:)
                                         contextInfo:NULL];
}



- (void)openDocument:(id)sender
{
    [[[NSApp delegate] welcomeController] close];
    
    [super openDocument:sender];
}



- (void)document:(NSDocument *)doc
         didSave:(BOOL)didSave
     contextInfo:(void  *)contextInfo
{
    if (!didSave)
    {
        [doc close];
    }
}

@end
