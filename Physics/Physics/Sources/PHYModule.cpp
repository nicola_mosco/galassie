//
//  PHYModule.cpp
//  Physics
//
//  Created by Nicola Mosco on 06/04/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#include <stdexcept>

#include "PHYModule.h"


using namespace phy;



void
Module::setParent(std::weak_ptr<Module> module)
{
    parent_ = module;
}



void
Module::addSubModule(std::shared_ptr<Module> module)
{
    throw std::runtime_error("*** EXCEPTION: trying to add module to leaf object! ***");
}



void
Module::removeSubModule(std::shared_ptr<Module> module)
{
    throw std::runtime_error("*** EXCEPTION: there are no modules to remove! ***");
}
