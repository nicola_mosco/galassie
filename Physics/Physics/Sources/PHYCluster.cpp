//
//  PHYCluster.cpp
//  Physics
//
//  Created by Nicola Mosco on 06/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#include "PHYCluster.h"


using namespace phy;



Cluster::Cluster(const ReferenceFrame& aRefFrame,
                 const std::string& name)
    : Geometry{aRefFrame, name}
{}
