//
//  GALParameter.h
//  Galassie
//
//  Created by Nicola Mosco on 05/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GALParameter : NSObject <NSCopying>

@property (strong) NSString*            name;
@property (strong) NSNumber*            value;
@property (strong) NSString*            identifier;
@property (strong) NSNumberFormatter*   formatter;


+ (GALParameter*)parameterWithName:(NSString*)name
                             value:(NSNumber*)value
                        identifier:(NSString*)identifier
                         formatter:(NSNumberFormatter*)formatter;

- (id)initWithName:(NSString*)name
             value:(NSNumber*)value
        identifier:(NSString*)identifier
         formatter:(NSNumberFormatter*)formatter;

@end
