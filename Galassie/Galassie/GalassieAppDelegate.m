//
//  GalassieAppDelegate.m
//  Galassie
//
//  Created by Nicola Mosco on 04/05/11.
//  Copyright 2011 Università degli Studi di Trieste. All rights reserved.
//

#import "GalassieAppDelegate.h"

@implementation GalassieAppDelegate

@synthesize window;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
}

@end
