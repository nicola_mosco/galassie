//
//  PHYMeasureUnits.h
//  Physics
//
//  Created by Nicola Mosco on 06/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#ifndef Physics_PHYMeasureUnits_h
#define Physics_PHYMeasureUnits_h

#include "PHYTypes.h"


namespace phy
{
    extern
    const Real m_y;
    extern
    const Real m_z;
    extern
    const Real m_a;
    extern
    const Real m_f;
    extern
    const Real m_p;
    extern
    const Real m_n;
    extern
    const Real m_mu;
    extern
    const Real m_m;
    extern
    const Real m_c;
    extern
    const Real m_d;
    extern
    const Real m_da;
    extern
    const Real m_h;
    extern
    const Real m_k;
    extern
    const Real m_M;
    extern
    const Real m_G;
    extern
    const Real m_T;
    extern
    const Real m_P;
    extern
    const Real m_E;
    extern
    const Real m_Z;
    extern
    const Real m_Y;
    
    
    extern
    const Real u_s; // Second.
    extern
    const Real u_y; // Year.
    extern
    const Real u_c; // Century.
    extern
    const Real u_gy; // Galactic year.
    
    
    extern
    const Real u_m; // Meter.
    extern
    const Real u_au; // Astronomic unit.
    extern
    const Real u_ly; // Light year.
    extern
    const Real u_pc; // Parsec.
    
    
    extern
    const Real u_kg; // Kilogram.
}

#endif
