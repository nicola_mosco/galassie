//
//  PHYGeometry.cpp
//  Physics
//
//  Created by Nicola Mosco on 06/01/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#include "PHYGeometry.h"


using namespace phy;



Geometry::Geometry(const ReferenceFrame& refFrame,
                   const std::string& name)
    : refFrame_{refFrame}, name_{name}
{}



Geometry::~Geometry() = default;



const std::string&
Geometry::name() const
{
    return name_;
}



const ReferenceFrame&
Geometry::referenceFrame() const
{
    return refFrame_;
}
