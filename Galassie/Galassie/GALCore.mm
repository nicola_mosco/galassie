//
//  GALCore.mm
//  Galassie
//
//  Created by Nicola Mosco on 18/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import <Physics/PHYCore.h>

#import "NSNumber+GALNumberExtension.h"
#import "GALCore.h"


@implementation GALCore

- (id)init
{
    self = [super init];
    
    if (self)
    {
        auto dict = @{@"Name":      @"Core",
                      @"Body name": @"Black Hole",
                      @"Mass":      @0.0};
        
        self.parameters = [NSMutableDictionary dictionaryWithDictionary:dict];
        
        type = @"body_cluster";
    }
    
    return self;
}



- (phy::Core::PtrType)generate
{
    auto core = new phy::Core{[self frame],
                              [self.parameters[@"Name"] UTF8String]};
    
    core->bodyName() = [self.parameters[@"Body Name"] UTF8String];
    core->mass()     = [(NSNumber*)self.parameters[@"Mass"] value];
    
    return phy::Core::PtrType(core);
}

@end
