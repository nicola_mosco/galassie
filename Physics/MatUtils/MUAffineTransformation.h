//
//  MUAffineTransformation.h
//  MatUtils
//
//  Created by Nicola Mosco on 10/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MUAffineTransformation : NSObject

- (void)applyToData:(NSMutableData*)pointsData
          withRange:(NSRange)range;

@end
