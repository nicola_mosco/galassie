//
//  GALDocumentRepresentation.h
//  Galassie
//
//  Created by Nicola Mosco on 05/01/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GALDocument;


@interface GALDocumentRepresentation : NSObject
{
    __weak
    GALDocument*    document;
}

@property (assign, readonly) BOOL hasSystemGeometry;
@property (assign, readonly) BOOL hasInitialState;
@property (assign, readonly) BOOL hasSimulationData;
@property (assign, readonly) BOOL hasEnergyData;


- (id)initWithDocument:(GALDocument*)doc;

- (BOOL)buildStructureAtURL:(NSURL*)fileURL
                  withError:(NSError *__autoreleasing *)error;


@end
