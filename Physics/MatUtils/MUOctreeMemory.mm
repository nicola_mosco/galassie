//
//  MUOctreeMemory.mm
//  MatUtils
//
//  Created by Nicola Mosco on 04/09/11.
//  Copyright 2011 Nicola Mosco. All rights reserved.
//

#import "MUOctreeMemory.h"
#import "MUOctreeUtils.h"

using namespace mu;
using namespace oct;


OctreeMemory::OctreeMemory( const MUSize numBodies ) : _numBodies( numBodies )
{
    _maxDepth  = depthForNumBodies( numBodies );
    _maxNodes  = numNodesForNumBodies( numBodies, _maxDepth );
    
    reset();
    
    allocBuffer( _maxNodes );
}



OctreeMemory::~OctreeMemory( void )
{
    freeBuffer();
}



void OctreeMemory::allocBuffer( const MUSize numNodes )
{
    _bufSize   = bufferSizeForNumNodes( _maxNodes );
    _freeBytes = _bufSize - OCT_SIZE_HEADER;
    
    _bufferData = [[NSMutableData alloc] initWithLength: _bufSize];
    _buffer     = (MUBuffer)_bufferData.mutableBytes;
    
    return;
}



void OctreeMemory::reallocBuffer( const MUSize numNodes )
{
    MUSize     oldBufSize = _bufSize;
    
    _bufSize    = bufferSizeForNumNodes( numNodes );
    _freeBytes += _bufSize - oldBufSize;
    
    _bufferData = [[NSMutableData alloc] initWithLength: _bufSize];
    _buffer     = (MUBuffer)_bufferData.mutableBytes;
    
    return;
}



void OctreeMemory::freeBuffer( void )
{
    _bufferData = nil;
    
    _buffer     = NULL;
    
    _bufSize    = OCTSIZE(0);
    _freeBytes  = OCTSIZE(0);
    _current    = OCTREF(0);
    _next       = OCTREF(0);
    
    return;
}



OctNode OctreeMemory::allocNewNode( MURef *const nodeOffsetPtr )
{
    if ( _freeBytes < OCT_SIZE_NODE )
    {
        addNodes( _numBodies );
    }
    
    _current        = _next;
    _next          += OCT_SIZE_NODE;
    _freeBytes     -= OCT_SIZE_NODE;
    
    *nodeOffsetPtr  = _current;
    
    _currentNode    = (OctNode) ( _buffer + _current );
    
    memset( (MUPtr)_currentNode, 0, OCT_SIZE_NODE );
    
    return _currentNode;
}



void OctreeMemory::reset( void )
{
    _freeBytes   = _bufSize - OCT_SIZE_HEADER;
    
    _current     = OCTREF(0);
    _next        = offsetForNodeIndex( OCTREF(0) );
    _currentNode = NULL;
    
    return;
}



void OctreeMemory::addNodes( const MUSize numAddedNodes )
{
    MUReal        logNumNodes;
    
    _maxNodes  += numAddedNodes;
    
    logNumNodes = logf( (MUReal)( 7 * _maxNodes + 1 ) ) / logf( 8 );
    _maxDepth   = (MUSize) ceilf( logNumNodes - 1.0f );
    
    reallocBuffer( _maxNodes );
    
    return;
}
