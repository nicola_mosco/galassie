//
//  PHYReferenceFrame.h
//  Physics
//
//  Created by Nicola Mosco on 06/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYReferenceFrame__
#define __Physics__PHYReferenceFrame__

#include <memory>

#include "PHYAlgebra.h"


namespace phy
{
    class ReferenceFrame
    {
    public:
        ReferenceFrame(const Affine3& transf = Affine3::Identity(),
                       const RVec3& vel = {0.0, 0.0, 0.0});
        
    public:
        const Affine3& transf() const;
        RVec3 vel() const;
        
    public:
        void transformTo(const ReferenceFrame& aRefFrame);
        RVec3 operator()(const RVec3& point) const;
        
        
        
    private:
        Affine3 transf_;
        Vector3 vel_;
    };
}

#endif /* defined(__Physics__PHYReferenceFrame__) */
