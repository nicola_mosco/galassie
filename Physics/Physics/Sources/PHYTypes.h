//
//  PHYTypes.h
//  Physics
//
//  Created by Nicola Mosco on 06/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#ifndef Physics_PHYTypes_h
#define Physics_PHYTypes_h


#ifndef __OPENCL__
#   ifdef __APPLE__
#       include <OpenCL/cl_platform.h>
#   else
#       include <CL/cl_platform.h>
#   endif // __APPLE__
#endif // __OPENCL__


#ifdef __cplusplus
#   include <vector>
#   include <memory>
#endif // __cplusplus


#ifndef PHY_NUM_THREADS
#   define PHY_NUM_THREADS  8
#endif


#define PHY_FLOAT   32
#define PHY_DOUBLE  64
#define PHY_INT_32  32
#define PHY_INT_64  64

#ifndef PHY_FLOATING
#   define PHY_FLOATING PHY_FLOAT
#endif
#ifndef PHY_INTEGER
#   define PHY_INTEGER PHY_INT_32
#endif


#define PHY_REAL_VECTOR_CL_3  3
#define PHY_REAL_VECTOR_CL_4  4

#ifndef PHY_REAL_VECTOR_3
#   define PHY_REAL_VECTOR_3    PHY_REAL_VECTOR_CL_4
#endif


#ifdef __cplusplus
#   define PHY_SIZE_PTR     sizeof(phy::Ptr)
#   define PHY_SIZE_REAL    sizeof(phy::Real)
#   define PHY_SIZE_REAL3   sizeof(phy::Real3)
#   define PHY_SIZE_REAL4   sizeof(phy::Real4)
#   define PHY_SIZE_RVEC3   sizeof(phy::RVec3)
#   define PHY_SIZE_RVEC4   sizeof(phy::RVec4)
#else
#   define PHY_SIZE_PTR     sizeof(Ptr)
#   define PHY_SIZE_REAL    sizeof(Real)
#   define PHY_SIZE_REAL3   sizeof(Real3)
#   define PHY_SIZE_REAL4   sizeof(Real4)
#   define PHY_SIZE_RVEC3   sizeof(RVec3)
#   define PHY_SIZE_RVEC4   sizeof(RVec4)
#endif // __cplusplus


#ifdef __OPENCL__
typedef float   cl_float;
typedef float3  cl_float3;
typedef float4  cl_float4;
//typedef double  cl_double;
//typedef double3 cl_double3;
//typedef double4 cl_double4;

typedef unsigned char cl_uchar;

typedef int     cl_int;
typedef uint    cl_uint;
typedef long    cl_long;
typedef ulong   cl_ulong;
#endif // __OPENCL__


#ifdef __cplusplus
namespace phy
{
#endif // __cplusplus
    
#if     PHY_FLOATING == PHY_FLOAT
    typedef cl_float    Real;
    typedef cl_float3   Real3;
    typedef cl_float4   Real4;
#elif   PHY_FLOATING == PHY_DOUBLE
    typedef cl_double   Real;
    typedef cl_double3  Real3;
    typedef cl_double4  Real4;
#else
#   error "*** Unrecognized floating type ***"
#endif
    
#if     PHY_REAL_VECTOR_3 == PHY_REAL_VECTOR_CL_3
    typedef Real3       RVec3;
#elif   PHY_REAL_VECTOR_3 == PHY_REAL_VECTOR_CL_4
    typedef Real4       RVec3;
#else
#   error "*** Unrecognized OpenCL vector type ***"
#endif
    
    typedef Real4       RVec4;
    
    typedef cl_uchar    Byte;
    
#if     PHY_INTEGER == PHY_INT_32
    typedef size_t      Ref;
    typedef cl_int      Cell;
    typedef size_t      Size;
    typedef cl_int      Int;
    typedef cl_uint     UInt;
#elif   PHY_INTEGER == PHY_INT_64
    typedef size_t      Ref;
    typedef cl_long     Cell;
    typedef size_t      Size;
    typedef cl_long     Int;
    typedef cl_ulong    UInt;
#else
#   error "*** Unrecognized integer type ***"
#endif
    
    typedef Byte*       Buffer;
    typedef void*       Ptr;
    
    
#ifdef __cplusplus
    typedef std::vector<Real> RealArray;
    typedef std::vector<RVec3> RVec3Array;
#endif // __cplusplus
    
#ifdef __cplusplus
}
#endif // __cplusplus

#endif
