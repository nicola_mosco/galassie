//
//  GALGalaxyDefinition.h
//  Galassie
//
//  Created by Nicola Mosco on 03/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import <MatUtils/MUDistributions.h>
#import "GALSimulationParameters.h"


namespace gal
{
    class GalaxyDefinition
    {
    public:
        GalaxyDefinition( GALSimulationParameters* someParams,
                          const std::string& massDistName    = mu::dist::gaussianTail,
                          const std::string& spatialDistName = mu::dist::rayleighTail );
        ~GalaxyDefinition( void );
        
        
    public:
        MUSize&                     numBodies( void ) { return numBodies_; }
        MUReal&                     G( void ) { return G_; }
        MUReal&                     coreMass( void ) { return coreMass_; }
        MUReal&                     meanMass( void ) { return meanMass_; }
        MUReal&                     massSpread( void ) { return massSpread_; }
        MUReal&                     bulge( void ) { return bulge_; }
        MUReal&                     extent( void ) { return extent_; }
        MUReal&                     coreRadius( void ) { return coreRadius_; }
        MUReal&                     lambda( void ) { return lambda_; }
        
        MUReal                      getMass( void );
        MUReal3                     getPosition( void );
        MUReal3                     getVelocity( const MUReal3 pos );
        
        
        
    private:
        void                        setup( GALSimulationParameters* someParams );
        void                        setupDistributions( void );
        
        
        
    private:
        mu::dist::Distribution*     massDist_;
        mu::dist::Distribution*     spatialDist_;
        mu::dist::Uniform*          phiDist_;
        mu::dist::Uniform*          thetaDist_;
        
        MUSize                      numBodies_;
        MUReal                      G_;
        MUReal                      coreMass_;
        MUReal                      meanMass_;
        MUReal                      massSpread_;
        MUReal                      bulge_;
        MUReal                      extent_;
        MUReal                      coreRadius_;
        MUReal                      lambda_;
    };
}
