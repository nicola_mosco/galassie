//
//  PHYUniverse.cpp
//  Physics
//
//  Created by Nicola Mosco on 06/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#include "PHYUniverse.h"



using namespace phy;



Universe::Universe(const std::string& name)
    : Geometry{ReferenceFrame{}, name}
{}



void
Universe::addCluster(Cluster::PtrType&& cluster)
{
    clusters_.push_back(std::forward<Cluster::PtrType>(cluster));
}



void
Universe::build(Body::ListType& bodies)
{
    for (auto& cluster : clusters_)
    {
        cluster->build(bodies);
    }
}
