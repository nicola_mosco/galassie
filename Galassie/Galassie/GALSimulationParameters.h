//
//  GALSimulationParameters.h
//  Galassie
//
//  Created by Nicola Mosco on 12/08/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MatUtils/MUTypes.h>


@class GALParameter;


@interface GALSimulationParameters : NSObject <NSTableViewDataSource, NSTableViewDelegate>

@property (assign)      MUSize  numGalaxies;    // Number of simulated galaxies.
@property (assign)      MUSize  numBodiesGal;   // Number of bodies per galaxy.
@property (readonly)    MUSize  numBodies;      // Total number of bodies.
@property (assign)      MUReal  G;              // Gravitational constant in suitable units.
@property (assign)      MUReal  coreMass;       // Mass of central black hole.
@property (assign)      MUReal  meanMass;       // Mean mass of stars in the galaxy.
@property (assign)      MUReal  massSpread;     // Mass spread for stars in the galaxy.
@property (assign)      MUReal  bulge;          // Bulge radius.
@property (assign)      MUReal  extent;         // Approximated radius of galaxies.
@property (assign)      MUReal  uniMinRadius;   // Internal radius of the Universe sferical shell.
@property (assign)      MUReal  uniMaxRadius;   // External radius of the Universe sferical shell.
@property (assign)      MUReal  d;              // Minimum relative distance between galaxies.
@property (assign)      MUReal  minSpeed;       // Galaxies minimum speed.
@property (assign)      MUReal  maxSpeed;       // Galaxies maximum speed.
@property (assign)      MUReal  lambda;         // Minimum relative distance between stars in a galaxy.
@property (assign)      MUReal  coreRadius;     // Internal galaxy shell radius.
@property (assign)      MUReal  eps;            // Softening factor in gravitational force calculation.
@property (assign)      MUReal  theta;          // Tolerance factor for the Octree traversal.
@property (assign)      MUReal  t_i;            // Simulated initial time.
@property (assign)      MUReal  t_f;            // Simulated final time.
@property (assign)      MUReal  h;              // Integration step.
@property (assign)      MUReal  sampleIntv;     // Sample interval.
@property (assign)      BOOL    usingGPU;       // What device type to use in OpenCL.

@property (strong)      NSMutableArray* parameters;


- (id)init;

@end
