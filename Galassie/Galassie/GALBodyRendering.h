//
//  GALBodyRendering.h
//  Galassie
//
//  Created by Nicola Mosco on 04/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGL/OpenGL.h>
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>
#import <MatUtils/MUTypes.h>
#import <MatUtils/MUMathFunc.h>


namespace gal
{
    class BodyRendering
    {
    public:
        BodyRendering( void );
        
        
    public:
        MUReal&             minSpeed() { return minSpeed_; }
        MUReal&             maxSpeed() { return maxSpeed_; }
        void                computeDeltaSpeed() { deltaSpeed_ = ( maxSpeed_ - minSpeed_ ) / ( 1.0 - colorMin_ ); }
        
        GLfloat&            radius( void ) { return radius_; }
        GLint&              slices( void ) { return slices_; }
        GLint&              stacks( void ) { return stacks_; }
        
        GLfloat&            shininess( void ) { return shininess_; }
        
        
        NSColor*            colorAmbient( void );
        NSColor*            colorDiffuse( void );
        NSColor*            colorSpecular( void );
        NSColor*            colorEmission( void );
        
        void                setColorAmbient( NSColor* colorAmbient );
        void                setColorDiffuse( NSColor* colorDiffuse );
        void                setColorSpecular( NSColor* colorSpecular );
        void                setColorEmission( NSColor* colorEmission );
        
        
        void                generateDisplayList( void );
        
        MUReal              normalizeVel( const MUReal v )
        {
            //return ( v >= 0.0 ? ( v - minSpeed_ ) / deltaSpeed_ + 0.5 : ( v + minSpeed_ ) / deltaSpeed_ + 0.5 );
            return ( Fabs(v) - minSpeed_ ) / deltaSpeed_ + colorMin_;
        }
        
        void                draw( const MUReal3 pos, const MUReal3 vel )
        {
            glPushMatrix();
            {
                glColor3f( normalizeVel( vel.s[0] ),
                           normalizeVel( vel.s[1] ),
                           normalizeVel( vel.s[2] ) );
                glTranslatef( pos.s[0], pos.s[1], pos.s[2] );
                glCallList( displayList_ );
            }
            glPopMatrix();
            
            return;
        }
        
        
        
    private:
        MUReal              minSpeed_;
        MUReal              maxSpeed_;
        MUReal              deltaSpeed_;
        
        GLfloat             radius_;
        GLint               slices_;
        GLint               stacks_;
        
        GLfloat             colorMin_;
        
        NSColor*            colorAmbient_;
        NSColor*            colorDiffuse_;
        NSColor*            colorSpecular_;
        NSColor*            colorEmission_;
        
        GLfloat             shininess_;
        GLfloat             colors_[4][4];
        
        //GLUquadricObj*      bodyObject_;
        
        GLuint              displayList_;
    };
}
