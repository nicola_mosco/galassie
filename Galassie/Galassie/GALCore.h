//
//  GALCore.h
//  Galassie
//
//  Created by Nicola Mosco on 18/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import "GALGeometry.h"


namespace phy
{
    class Core;
}


@interface GALCore : GALGeometry

- (phy::Core::PtrType)generate;

@end
