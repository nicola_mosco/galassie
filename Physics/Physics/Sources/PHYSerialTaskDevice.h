//
//  PHYSerialTaskDevice.h
//  Physics
//
//  Created by Nicola Mosco on 24/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#ifndef __Physics__PHYSerialTaskDevice__
#define __Physics__PHYSerialTaskDevice__

#include "PHYDevice.h"


namespace phy
{
    class SerialTaskDevice : public Device
    {
    public:
        virtual ~SerialTaskDevice() = 0;
        
    public:
        virtual void performTask(dispatch_block_t beginAction,
                                 dispatch_block_t endAction) final;
        
        
        
    protected:
        explicit SerialTaskDevice(const std::string& description);
        
    protected:
        virtual void task() = 0;
        
        
        
    private:
        SerialTaskDevice() = delete;
        SerialTaskDevice(const SerialTaskDevice& device) = delete;
        const SerialTaskDevice& operator=(const SerialTaskDevice& device) = delete;
    };
}

#endif /* defined(__Physics__PHYSerialTaskDevice__) */
