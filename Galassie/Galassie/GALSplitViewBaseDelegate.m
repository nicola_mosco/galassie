//
//  GALSplitViewBaseDelegate.m
//  Galassie
//
//  Created by Nicola Mosco on 03/11/11.
//  Copyright (c) 2011 Nicola Mosco. All rights reserved.
//

#import "GALSplitViewBaseDelegate.h"

@implementation GALSplitViewBaseDelegate

@synthesize spaceView, sideBar;



-      (BOOL)splitView:(NSSplitView *)splitView
    canCollapseSubview:(NSView *)subview
{
    if ( subview == sideBar )
    {
        return YES;
    }
    else
    {
        return NO;
    }
}



-                  (BOOL)splitView:(NSSplitView *)splitView
             shouldCollapseSubview:(NSView *)subview
    forDoubleClickOnDividerAtIndex:(NSInteger)dividerIndex
{
    if ( dividerIndex == 0 )
    {
        if ( [self splitView: splitView
          canCollapseSubview: subview] )
        {
            if ( [splitView isSubviewCollapsed: subview] )
            {
                return NO;
            }
            else
            {
                return YES;
            }
        }
    }
    
    return NO;
}



-       (CGFloat)splitView:(NSSplitView *)currentSplitView
    constrainMinCoordinate:(CGFloat)proposedMinimumPosition
               ofSubviewAt:(NSInteger)dividerIndex
{
    if ( dividerIndex == 0 )
    {
        CGFloat pos = NSWidth( [currentSplitView bounds] ) - 300.0f;
        
        if ( proposedMinimumPosition <= pos )
        {
            return pos;
        }
    }
    
    return proposedMinimumPosition;
}



-             (BOOL)splitView:(NSSplitView *)splitView
    shouldAdjustSizeOfSubview:(NSView *)view
{
    if ( view == sideBar )
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

@end
