/*
 *  MUMathFunc.h
 *  MAtUtils
 *
 *  Created by Nicola Mosco on 27/12/10.
 *  Copyright 2010 Nicola Mosco. All rights reserved.
 *
 */

#ifndef _MU_MATH_FUNC_H_
#define _MU_MATH_FUNC_H_

#include <math.h>

#include "MUTypes.h"

#if   MUREAL_D == FLOAT
#   define Sqrt( x ) (sqrtf( MU_REAL(x) ))
#   define Pow( x, e ) (powf( MU_REAL(x), MU_REAL(e) ))
#   define Sin( x ) (sinf( MU_REAL(x) ))
#   define Cos( x ) (cosf( MU_REAL(x) ))
#   define Tan( x ) (tanf( MU_REAL(x) ))
#   define Sinh( x ) (sinhf( MU_REAL(x) ))
#   define Cosh( x ) (coshf( MU_REAL(x) ))
#   define Tanh( x ) (tanhf( MU_REAL(x) ))
#   define Asin( x ) (asinf( MU_REAL(x) ))
#   define Acos( x ) (acosf( MU_REAL(x) ))
#   define Atan( x ) (atanf( MU_REAL(x) ))
#   define Asinh( x ) (asinhf( MU_REAL(x) ))
#   define Acosh( x ) (acoshf( MU_REAL(x) ))
#   define Atanh( x ) (atanhf( MU_REAL(x) ))
#   define Exp( x ) (expf( MU_REAL(x) ))
#   define Fabs( x ) (fabsf( MU_REAL(x) ))
#   define Ceil( x ) (ceilf( MU_REAL(x) ))
#   define Floor( x ) (floorf( MU_REAL(x) ))
#   define Round( x ) (roundf( MU_REAL(x) ))
#   define Fmod( x, y ) (fmodf( MU_REAL(x), MU_REAL(y) ))
#elif MUREAL_D == DOUBLE
#   define Sqrt( x ) (sqrt( MU_REAL(x) ))
#   define Pow( x, e ) (pow( MU_REAL(x), MU_REAL(e) ))
#   define Sin( x ) (sin( MU_REAL(x) ))
#   define Cos( x ) (cos( MU_REAL(x) ))
#   define Tan( x ) (tan( MU_REAL(x) ))
#   define Sinh( x ) (sinh( MU_REAL(x) ))
#   define Cosh( x ) (cosh( MU_REAL(x) ))
#   define Tanh( x ) (tanh( MU_REAL(x) ))
#   define Asin( x ) (asin( MU_REAL(x) ))
#   define Acos( x ) (acos( MU_REAL(x) ))
#   define Atan( x ) (atan( MU_REAL(x) ))
#   define Asinh( x ) (asinh( MU_REAL(x) ))
#   define Acosh( x ) (acosh( MU_REAL(x) ))
#   define Atanh( x ) (atanh( MU_REAL(x) ))
#   define Exp( x ) (exp( MU_REAL(x) ))
#   define Fabs( x ) (fabs( MU_REAL(x) ))
#   define Ceil( x ) (ceil( MU_REAL(x) ))
#   define Floor( x ) (floor( MU_REAL(x) ))
#   define Round( x ) (round( MU_REAL(x) ))
#   define Fmod( x, y ) (fmod( MU_REAL(x), MU_REAL(y) ))
#elif MUREAL_D == L_DOUBLE
#   define Sqrt( x ) (sqrtl( MU_REAL(x) ))
#   define Pow( x, e ) (powl( MU_REAL(x), MU_REAL(e) ))
#   define Sin( x ) (sinl( MU_REAL(x) ))
#   define Cos( x ) (cosl( MU_REAL(x) ))
#   define Tan( x ) (tanl( MU_REAL(x) ))
#   define Sinh( x ) (sinhl( MU_REAL(x) ))
#   define Cosh( x ) (coshl( MU_REAL(x) ))
#   define Tanh( x ) (tanhl( MU_REAL(x) ))
#   define Asin( x ) (asinl( MU_REAL(x) ))
#   define Acos( x ) (acosl( MU_REAL(x) ))
#   define Atan( x ) (atanl( MU_REAL(x) ))
#   define Asinh( x ) (asinhl( MU_REAL(x) ))
#   define Acosh( x ) (acoshl( MU_REAL(x) ))
#   define Atanh( x ) (atanhl( MU_REAL(x) ))
#   define Exp( x ) (expl( MU_REAL(x) ))
#   define Fabs( x ) (fabsl( MU_REAL(x) ))
#   define Ceil( x ) (ceill( MU_REAL(x) ))
#   define Floor( x ) (floorl( MU_REAL(x) ))
#   define Round( x ) (roundl( MU_REAL(x) ))
#   define Fmod( x, y ) (fmodl( MU_REAL(x), MU_REAL(y) ))
#endif

#endif //_MU_MATH_FUNC_H_
