//
//  PHYSystem.cpp
//  Physics
//
//  Created by Nicola Mosco on 06/09/12.
//  Copyright (c) 2012 Nicola Mosco. All rights reserved.
//

#include <array>
#include <dispatch/dispatch.h>

#include "PHYSystem.h"


using namespace phy;



System::System(Universe::PtrType const& universe)
{
    universe->build(bodies_);
    
    numBodies_ = bodies_.size();
    
    pos_.resize(numBodies_);
    vel_.resize(numBodies_);
    masses_.resize(numBodies_);
}



Real
System::numBodies() const
{
    return numBodies_;
}



Real
System::mass() const
{
    return mass_;
}



Real
System::energy() const
{
    return energy_;
}



void
System::computeMass()
{
    auto massesArray = masses_.data();
    
    mass_ = Real(0.0);
    
    auto queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    
    auto block(numBodies_ / PHY_NUM_THREADS);
    std::array<Real, PHY_NUM_THREADS> partials{};
    auto partPtr = partials.data();
    
    dispatch_apply(PHY_NUM_THREADS, queue,
                   ^(Size g)
                   {
                       auto b_in  = block * g;
                       auto b_fi  = (g == PHY_NUM_THREADS - 1 ? numBodies_ : block * (g + 1));
                       
                       for (auto i = b_in; i < b_fi; ++i)
                       {
                           partPtr[g] += massesArray[i];
                       }
                   });
    
    for(auto mass : partials)
    {
        mass_ += mass;
    }
}



void
System::computeEnergy()
{
    
}



void
System::extractBodiesData()
{
    auto queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    auto group = dispatch_group_create();
    
    dispatch_group_async(group, queue,
                         ^{
                             for (auto& body : bodies_)
                             {
                                 pos_.push_back(body->pos());
                             }
                         });
    
    dispatch_group_async(group, queue,
                         ^{
                             for (auto& body : bodies_)
                             {
                                 vel_.push_back(body->vel());
                             }
                         });
    
    dispatch_group_async(group, queue,
                         ^{
                             for (auto& body : bodies_)
                             {
                                 masses_.push_back(body->mass());
                             }
                         });
    
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    dispatch_release(group);
    
    /*for (auto body : bodies_)
    {
        pos_.push_back(body->pos());
        vel_.push_back(body->vel());
        masses_.push_back(body->mass());
    }*/
}



void
System::build()
{
    extractBodiesData();
    
    computeMass();
    computeEnergy();
}
