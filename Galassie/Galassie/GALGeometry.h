//
//  GALGeometry.h
//  Galassie
//
//  Created by Nicola Mosco on 17/03/13.
//  Copyright (c) 2013 Nicola Mosco. All rights reserved.
//

#import <Foundation/Foundation.h>


namespace phy
{
    class ReferenceFrame;
    class Geometry;
}


@interface GALGeometry : NSObject // This class is a pure interface.
{
    @protected
    NSString* type;
    NSMutableArray* entities;
}

@property (readonly) NSString* type;
@property (strong) NSMutableDictionary* parameters;


- (id)initWithName:(NSString*)name;


- (phy::ReferenceFrame)frame;


- (void)addEntity:(GALGeometry*)entity;

@end
